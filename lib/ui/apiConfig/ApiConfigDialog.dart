import 'dart:convert';

import 'package:auto_crysto/ui/apiresponseui/OnError.dart';
import 'package:auto_crysto/ui/commonui/CustomMaterialButton.dart';
import 'package:auto_crysto/ui/data/ApiHelper/ApiResponse.dart';
import 'package:auto_crysto/ui/data/bloc/ApiConfigBloc.dart';
import 'package:auto_crysto/ui/data/models/ApiConfigList.dart';
import 'package:auto_crysto/ui/data/models/ExchangeModel.dart';
import 'package:auto_crysto/ui/utils/AppColors.dart';
import 'package:auto_crysto/ui/utils/Images.dart';
import 'package:auto_crysto/ui/utils/Styles.dart';
import 'package:auto_crysto/ui/utils/Utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ApiConfigDialog extends StatefulWidget {
  // Used for checking which api is used for either add or update exchange
  final String type;
  final ApiConfigModel userExchange;

  const ApiConfigDialog({Key key, this.type, this.userExchange}) : super(key: key);@override
  _ApiConfigDialogState createState() => _ApiConfigDialogState(type,userExchange);
}

class _ApiConfigDialogState extends State<ApiConfigDialog> {
  ApiConfigBloc _apiConfigBloc;
  List<ExchangeModel> _exchangeList;
  String _selectedExchangeID;
  TextEditingController _exchangeController=TextEditingController();
  TextEditingController _apiKeyTextController=TextEditingController();
  TextEditingController _apiSecretTextController=TextEditingController();
  String _apiKeyError;
  String _apiSecretError;
  String _exchangeError;
  final ApiConfigModel userExchange;
  _ApiConfigDialogState(String type, this.userExchange,);
  @override
  void dispose() {
    _exchangeController.dispose();
    _apiKeyTextController.dispose();
    _apiSecretTextController.dispose();
    super.dispose();
  }
@override
  void initState() {
    // TODO: implement initState
    super.initState();
    _apiConfigBloc=ApiConfigBloc();
    fetchExchanges();
    _listenExchangeStream();
    _listenApiPostStream(context);
    if(this.userExchange!=null){
      _apiSecretTextController.text=userExchange.apiSecret;
      _apiKeyTextController.text=userExchange.apiKey;
    }

  }
  fetchExchanges()async{
  await _apiConfigBloc.fetchExchangeList();
  }
  @override
  Widget build(BuildContext context) {
    return buildDialog(context);
  }
  void showExchangePicker(BuildContext context) async {
    if (_exchangeList == null) {
      await _apiConfigBloc.fetchExchangeList();
      buildPicker(context);
    } else {
      _exchangeController.text=_exchangeList[0].title;
      _selectedExchangeID=_exchangeList[0].id.toString();
      buildPicker(context);
    }
  }
  Future buildPicker(BuildContext context) {
    return showCupertinoModalPopup(context: context, builder: (context){

      return Container(

        height: Utils().getScreenHeight(context)*.35,
        width: Utils().getScreenWidth(context),
        child: CupertinoPicker(
          itemExtent: 35.0,
          onSelectedItemChanged: (index) {
            setState(() {
              print(_exchangeList[index].title);
            });
          },
          children: _exchangeList != null
              ? List<Widget>.generate(_exchangeList.length, (index) {
            return Center(
              child: Text(_exchangeList[index].title),
            );
          })
              : List<Widget>.generate(0, (context) {}),
          backgroundColor: CupertinoColors.white,
          useMagnifier: true,
        ),
      );
    });
  }
  Widget buildDialog(BuildContext context) {
    return Center(
      child: SingleChildScrollView(
        child: Material(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
          child: Container(
            width: Utils().getScreenWidth(context)*.95,
            
            padding: EdgeInsets.fromLTRB(12.0, 20.0, 12.0, 20.0),
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(10.0)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Text(
                  "Settings",
                  style: Styles.boldHeadingOneBlack(),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Text(
                  "Connect to your exchange",
                  style: Styles.semiBoldHeadingThree(),
                ),
                SizedBox(
                  height: 20.0,
                ),
                Stack(
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        showExchangePicker(context);
                      },
                      child: AbsorbPointer(
                        child: TextFormField(
                          controller: _exchangeController,
                          showCursor: false,
                          readOnly: true,
                          validator: (value) {
                            if (value.isEmpty) {
                              return "Exchange must not be empty";
                            } else {
                              return null;
                            }
                          },
                          autofocus: false,
                          decoration: InputDecoration(
                            errorText: _exchangeError,
                            contentPadding: EdgeInsets.all(14.0),
                            labelText: "Select Exchange",
                            focusColor: AppColors.colorPrimary,
                            fillColor: AppColors.colorPrimary,
                            hoverColor: AppColors.colorPrimary,
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: AppColors.colorPrimary, width: 1.0)),
                          ),
                          keyboardType: TextInputType.text,
                          cursorColor: AppColors.colorPrimary,
                        ),
                      ),
                    ),
                    Align(
                        alignment: Alignment.centerRight,
                        child: Padding(
                          padding: const EdgeInsets.only(top: 10.0, right: 4.0),
                          child: Icon(Icons.keyboard_arrow_down,color: Colors.black26,),
                        ))
                  ],
                ),
                SizedBox(
                  height: 10.0,
                ),
                ApiKeyField(widget.key, _apiKeyTextController, _apiKeyError),
                SizedBox(
                  height: 10.0,
                ),
                ApiKeySecretField(widget.key, _apiSecretTextController, _apiSecretError),
                SizedBox(
                  height: 20.0,
                ),
                CustomMaterialButton(widget.key,(){
                  _sendApiKeysToServer();
                },widget.type),
                SizedBox(
                  height: 20.0,
                ),
                Text(
                  "-------- OR --------",
                  style: Styles.boldHeadingThree(),
                ),
                SizedBox(
                  height: 20.0,
                ),
                InkWell(
                  onTap: (){
                    _openBarCodeScanner();
                  },
                  child: Image.asset(
                    Images.qr,
                    height: 40.0,
                    width: 40.0,
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                FlatButton(
                  onPressed: _openBarCodeScanner,
                  child: Text("Scan QR Code", style: TextStyle(
                    decoration: TextDecoration.underline,
                      fontFamily: "Montserrat", fontWeight: FontWeight.w800, fontSize: 15.0
                  ),),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
  _openBarCodeScanner() async{
    String cameraScanResult = await BarcodeScanner.scan();
   try{
     var result=json.decode(cameraScanResult);
     print("Api key is"+result["apiKey"]);
     print("Secret key is"+result["secretKey"]);
     if(result["apiKey"]==null||result["secretKey"]==null){
       Fluttertoast.showToast(msg: "Error");
       showDialog(context: context,builder:(_)=>AlertDialog(title: Text("Error",style: Styles.semiBoldHeadingTwo(),),
         content: Text("Invalid QR code",style: Styles.regularBodyTwo(),),
         actions: <Widget>[
           MaterialButton(onPressed: (){
             Navigator.pop(context);
           })
         ],
       ));
       return;
     }
     setState(() {
       _apiKeyTextController.text=result["apiKey"];
       _apiSecretTextController.text=result["secretKey"];
       setApiError();
       setKeyError();
     });
   }catch(e){
     showDialog(context: context,builder:(_)=>AlertDialog(title: Text("Error",style: Styles.semiBoldHeadingTwo(),),
       content: Text("Invalid QR code",style: Styles.regularBodyTwo(),),
       shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
       actions: <Widget>[
         MaterialButton(onPressed: (){
      
           Navigator.pop(context);
         },
           shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
           color: AppColors.colorPrimary,textColor: Colors.white,child: Text("Ok",style: Styles.semiBoldHeadingFour(color: Colors.white),),)
       ],
     ));
    }

  }
  _listenExchangeStream(){
  _apiConfigBloc.exchangeStream.listen((value){
    switch(value.status){

      case Status.LOADING:
        print("Loading exchanges");
        break;
      case Status.COMPLETED:
        print("Loading exchanges completed");
        if(value.data.isNotEmpty){
          _exchangeController.text=value.data[0].title;
          _selectedExchangeID=value.data[0].id.toString();
        }
        setState(() {
          _exchangeList=value.data;
        });
        break;
      case Status.ERROR:
        Navigator.pop(context);
        OnError(onRetryPressed: _apiConfigBloc.fetchExchangeList(),
        errorMessage: value.message,
        );
        break;
    }
  });
  }
  _listenApiPostStream(BuildContext context) {
    _apiConfigBloc.apiPostStream.listen((data) {
      switch (data.status) {
        case Status.LOADING:
          print('laoding');
//        _onLoading(context,data.message);
          break;
        case Status.COMPLETED:
//          Scaffold.of(context).hideCurrentSnackBar();
//          showDialog(context: context,builder: (context)=>Container(
//            child: Center(
//              child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(AppColors.colorPrimary),
//                backgroundColor: Colors.white,
//              ),
//            ),
//          ));
//          Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>Home()));
        Fluttertoast.showToast(msg: data.data.message,backgroundColor: AppColors.colorPrimaryGradient,textColor: Colors.white,gravity: ToastGravity.BOTTOM);
          print("Completed");
          break;
        case Status.ERROR:
          showDialog(context: context,builder: (c)=>OnError(errorMessage: data.message,onRetryPressed: (){
            _sendApiKeysToServer();
          },));
          break;
      }
    });
  }
  _sendApiKeysToServer() async{

    if(widget.userExchange==null&&widget.type!="Save"){
      // implement logic
      Fluttertoast.showToast(msg: "Exchange id is null");
    }
    else if(_apiKeyTextController.text.isEmpty||_apiSecretTextController.text.isEmpty){
      setApiError();
      setKeyError();
    }
    else{
      Navigator.pop(context);
      Map<String,String> body={
        "api_key":_apiKeyTextController.text,
        "api_secret":_apiSecretTextController.text,
        "exchange":_selectedExchangeID.toString()
      };
      print("Sendning data");
      print(body);
       widget.type=="Save"?await _apiConfigBloc.postAPIKeys(body):await _apiConfigBloc.updateApiKeys(widget.userExchange.id.toString(),body);
    }

  }
  setApiError() {
    if(_apiKeyTextController.text.isEmpty){
      setState(() {
        _apiKeyError="Api Key must not be empty";
      });
    }
    else{
      setState(() {
        _apiKeyError=null;
      });
    }
  }
  setKeyError(){
    if(_apiSecretTextController.text.isEmpty){
      setState(() {
        _apiSecretError="Secret Key must not be empty";
      });
    }
    else{
      setState(() {
        _apiSecretError=null;
      });
    }
  }
}



class ApiKeyField extends StatelessWidget {
  final TextEditingController lNameController;
  final String lNameError;

  const ApiKeyField(Key key, this.lNameController, this.lNameError)
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: lNameController,
      autofocus: false,
//      onSaved: (value) => lName = value,
      validator: (value) {
        if (value.isEmpty) {
          return "Last name must not be empty";
        } else {
          return null;
        }
      },
      keyboardType: TextInputType.text,
      decoration: Styles.getOutlineBorder("Enter Api Key", lNameError),
      cursorColor: AppColors.colorPrimary,
    );
  }
}
class ApiKeySecretField extends StatelessWidget {
  final TextEditingController lNameController;
  final String lNameError;

  const ApiKeySecretField(Key key, this.lNameController, this.lNameError)
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: lNameController,
      autofocus: false,
//      onSaved: (value) => lName = value,
      validator: (value) {
        if (value.isEmpty) {
          return "Last name must not be empty";
        } else {
          return null;
        }
      },
      keyboardType: TextInputType.text,
      decoration: Styles.getOutlineBorder("Api Secret Key", lNameError),
      cursorColor: AppColors.colorPrimary,
    );
  }
}
