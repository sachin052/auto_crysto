import 'package:auto_crysto/routes/Router.dart';
import 'package:auto_crysto/ui/apiConfig/ApiConfigDialog.dart';
import 'package:auto_crysto/ui/apiresponseui/OnError.dart';
import 'package:auto_crysto/ui/data/ApiHelper/ApiResponse.dart';
import 'package:auto_crysto/ui/data/bloc/ApiConfigBloc.dart';
import 'package:auto_crysto/ui/data/models/ApiConfigList.dart';
import 'package:auto_crysto/ui/data/models/ExchangeModel.dart';
import 'package:auto_crysto/ui/home/Home.dart';
import 'package:auto_crysto/ui/utils/AppColors.dart';
import 'package:auto_crysto/ui/utils/Images.dart';
import 'package:auto_crysto/ui/utils/Styles.dart';
import 'package:auto_crysto/ui/utils/Utils.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

List<String> _firstPageList = [
  "Login to your binance account",
  "Navigate to your account setting page",
  "Go to Api Settings",
  'Click on "Enable API"',
  'It will ask you to name the API and hit submit (Note: You will need to enable 2FA before this)',
  'Make sure you only select "Read Info" and "Enable Trading" options and not "Enable Withdrawls',
  'Get the API Key and Secret and paste it in the settings.',
  'Make sure not to store in an un safe location once addedd to AutoCrysto.',
  'Make sure to save a copy because Binance may not show it you again. You will need to create a new API if you loose the previous one.',
  'Be Safe & Have fun Trading.'
];
List<String> _titles = [
  'Where to get the Api Keys?',
  'High Risk Instruction',
  'Medium Risk Instruction',
  'Low Risk Instruction'
];
List<String> _pageTitle = [
  'High Risk - High-Profit Option',
  'Medium Risk Option',
  'Low Risk Option'
];

List<String> _pageImages = [Images.graph1, Images.graph2, Images.graph3];
List<String> _pageContent = [
  'Lets you buy a digital asset when its price goes below the specified %, and sells when its goes above the '
      'sell % & the price starts to drop. If the price keeps goind up, it waits until the price changes direction, even if it is way above'
      'the set limit. It automatically sells if the sell target is not reachable within the specified duration',
  'Lets you buy a digital asset when its price goes below the specified %, and sells when its goes above the '
      'sell %. It automatically sells if the sell target is not reached within the specified duration.',
  'Lets you buy a digital asset when its price goes below the specified %, and sells when the price goes up and starts go down.'
];
int _selectedExchangeID = 0;
TextEditingController _exchangeController = TextEditingController();
TextEditingController _apiKeyTextController = TextEditingController();
TextEditingController _apiSecretTextController = TextEditingController();
var _snackKey = GlobalKey<ScaffoldState>();

class WalkThrough extends StatefulWidget {
  final fromMenu;

  const WalkThrough({Key key, this.fromMenu}) : super(key: key);

  @override
  _WalkThroughState createState() => _WalkThroughState(this.fromMenu);
}

class _WalkThroughState extends State<WalkThrough>
    with SingleTickerProviderStateMixin {
  final fromMenu;
  int _current = 0;
  int _titleIndex = 0;
  AnimationController _animationController;
  Animation<double> _animation;
  ApiConfigBloc _apiConfigBloc;
  List<ExchangeModel> _exchangeList;
  String _apiKeyError;
  String _apiSecretError;

  String _barCodeData;
  String _exchangeError;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  var qrText = "";
  bool showApiDialog = false;
  var apiStream;
  _WalkThroughState(this.fromMenu);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _animationController = new AnimationController(
        vsync: this, duration: new Duration(milliseconds: 500));
    _animation = new Tween(begin: 0.0, end: 1.0).animate(_animationController);
    _apiConfigBloc = ApiConfigBloc();
    _exchangeController = TextEditingController();

    _apiKeyTextController.addListener(setApiError);
    _apiSecretTextController.addListener(setKeyError);
    _exchangeController.addListener(checkExchangeError);
    if(!fromMenu){
      _apiConfigBloc.fetchExchangeList();
      _apiConfigBloc.getApiConfigs();
    }
    bindListeners(context);
    apiStream=StreamBuilder<ApiResponse<ApiConfigList>>(
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          switch (snapshot.data.status) {
            case Status.LOADING:
              return Container(
                color: AppColors.colorPrimary.withOpacity(.3),
                child: Center(
                  child: CircularProgressIndicator(
                    valueColor:
                    AlwaysStoppedAnimation<Color>(Colors.white),
                    backgroundColor: AppColors.colorPrimary,
                    semanticsLabel: "Loading",
                    semanticsValue: "value",
                  ),
                ),
              );
              break;
            case Status.COMPLETED:
              if (snapshot.data.data.exchangeList.isEmpty&&widget.fromMenu) {
                showApiDialog = true;
                Future.delayed(Duration(milliseconds: 200),(){
                  Utils.showAnimatedDialog(context, ApiConfigDialog(type: "Save",));
                });
              }
              return Container();
              break;
            case Status.ERROR:
              return OnError(
                errorMessage: snapshot.data.message,
                onRetryPressed: () {
                  _apiConfigBloc.getApiConfigs();
                },
              );
              break;
            default:
              return Container();
          }
        } else {
          return Container();
        }
      },
      stream: _apiConfigBloc.apiConfigStream,
    );
  }

  @override
  Widget build(BuildContext context) {
    _animationController.forward();
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: Styles.getTheme(),
      onGenerateRoute: Router.generateRoute,
      home: MediaQuery(
        data: MediaQuery.of(context).size.width>600?MediaQuery.of(context).copyWith(textScaleFactor: 1.2):MediaQuery.of(context),
        child: Scaffold(
          key: _snackKey,
          resizeToAvoidBottomPadding: false,
          body: Stack(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(top: 10.0),
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        colors: [
                          AppColors.colorPrimary,
                          AppColors.colorPrimaryGradient
                        ],
                        begin: Alignment.bottomCenter,
                        stops: [0.3, 0.8],
                        end: Alignment.topCenter)),
                child: Stack(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top:18.0),
                      child: Center(
                        child: CarouselSlider(
                          height: Utils().getScreenHeight(context) * .80,
                          aspectRatio: 16 / 9,
                          viewportFraction: 0.8,
                          initialPage: 0,
                          onPageChanged: (index) {
                            setState(() {
                              _current = index;
                              _titleIndex = index;
                            });
                          },
                          reverse: false,
                          enableInfiniteScroll: false,
                          autoPlayInterval: Duration(seconds: 3),
                          autoPlayAnimationDuration: Duration(milliseconds: 800),
                          autoPlayCurve: Curves.fastOutSlowIn,
                          enlargeCenterPage: true,
                          pauseAutoPlayOnTouch: Duration(seconds: 10),
                          items: [0, 1, 2, 3].map((i) {
                            return Builder(
                              builder: (BuildContext context) {
                                if (i == 0) {
                                  return PageChild(
                                      widget.key,
                                      "      Choose from 3 options- \nLow Risk, Medium Risk & High Profit",
                                      Images.tutorial_3);
                                } else {
                                  return PageWithImage(widget.key, i - 1);
                                }
                              },
                            );
                          }).toList(),
                        ),
                      ),
                    ),
                    Padding(
                      padding:  EdgeInsets.only(top: 35.0,left: 10.0,right: 10.0),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Visibility(
                          visible: widget.fromMenu,

                            child: Material(
                              color: Colors.transparent,
                              child: InkWell(
                                borderRadius: BorderRadius.circular(20.0),
                                onTap: () {
                                  Navigator.pop(context);
                                },
                                splashColor: AppColors.colorPrimary.withOpacity(.5),
                                child: Icon(
                                  Icons.arrow_back,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                          FadeTransition(
                            opacity: _animation,
                            child: Text(_titles[_titleIndex],
                                style: Styles.boldHeadingOne().copyWith(fontSize: MediaQuery.of(context).size.width/100*5.3)),
                          ),
                          Visibility(
                            visible: !fromMenu,
                            child: Material(
                              color: Colors.transparent,
                              child: InkWell(
                                borderRadius: BorderRadius.circular(20.0),
                                onTap: () {
                                  Navigator.pushReplacementNamed(context, Router.home);
                                },
                                splashColor: AppColors.colorPrimary.withOpacity(.5),
                                child: Icon(
                                  Icons.home,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Positioned(
                        top: Utils().getScreenHeight(context) * .90,
                        left: 0.0,
                        right: 0.0,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: List<Widget>.generate(4, (index) {
                            return Container(
                              width: 8.0,
                              height: 8.0,
                              margin: EdgeInsets.symmetric(
                                  vertical: 10.0, horizontal: 2.0),
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: _current == index
                                      ? Colors.white
                                      : Color.fromRGBO(0, 0, 0, 0.4)),
                            );
                          }),
                        )),
                    Visibility(
                      visible: !widget.fromMenu,
                      child: Positioned(
                          top: Utils().getScreenHeight(context) * .90,
                          right: 20.0,
                          child: GestureDetector(
                            onTap: () {
                              // Changed navigation to home screen on tapping skip button
                              Navigator.pushReplacement(context,
                                  MaterialPageRoute(builder: (_) => Home()));
//                            if (showApiDialog) {
//                              Utils.showAnimatedDialog(context, ApiConfigDialog(type: "Save",));
//                            } else {
//
//                            }
                            },
                            child: Text(_current!=4?"Skip":"Finish",
                                style: TextStyle(
                                    decoration: TextDecoration.underline,
                                    color: Colors.white)),
                          )),
                    ),
                  ],
                ),
              ),
              apiStream
            ],
          ),
        ),
      ),
    );
  }
  void showExchangePicker(BuildContext context) async {
    if (_exchangeList == null) {
      await _apiConfigBloc.fetchExchangeList();
      buildPicker(context);
    } else {
      buildPicker(context);
    }
  }

  Future buildPicker(BuildContext context) {
    final FixedExtentScrollController scrollController =
        FixedExtentScrollController(initialItem: _selectedExchangeID);
    return showCupertinoModalPopup(
        context: context,
        builder: (context) {
          return Container(
            height: Utils().getScreenHeight(context) * .35,
            width: Utils().getScreenWidth(context),
            child: CupertinoPicker(
              itemExtent: 35.0,
              onSelectedItemChanged: (index) {
                setState(() {
                  print(_exchangeList[index].title);
                });
              },
              children: _exchangeList != null
                  ? List<Widget>.generate(_exchangeList.length, (index) {
                      return Center(
                        child: Text(_exchangeList[index].title),
                      );
                    })
                  : List<Widget>.generate(0, (context) {}),
              backgroundColor: CupertinoColors.white,
              useMagnifier: true,
            ),
          );
        });
  }

  bindListeners(BuildContext context) {
    _listenExchangeStream(context);
    _listenApiPostStream(context);
  }

  void _listenExchangeStream(BuildContext context) {
    _apiConfigBloc.exchangeStream.listen((data) {
      switch (data.status) {
        case Status.LOADING:
          break;
        case Status.COMPLETED:
          _exchangeList = data.data;
          _exchangeController.text = _exchangeList[0].title;
          _selectedExchangeID = _exchangeList[0].id;
          break;
        case Status.ERROR:
          break;
      }
    });
  }

  void _listenApiPostStream(BuildContext context) {
    _apiConfigBloc.apiPostStream.listen((data) {
      switch (data.status) {
        case Status.LOADING:
//        _onLoading(context,data.message);
          break;
        case Status.COMPLETED:
//          Scaffold.of(context).hideCurrentSnackBar();
          Utils.showShowSnackBarWithKey(_snackKey, data.data.message, false);
          Navigator.pop(context);
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => Home()));
          print("Completed");
          break;
        case Status.ERROR:
          Utils.showShowSnackBarWithKey(_snackKey, data.message, true);
          print("Error");
          break;
      }
    });
  }

  void checkExchangeError() {
    if (_exchangeController.text.isEmpty) {
      _exchangeError = "Please select Exchange";
    }
    return null;
  }

  _openBarCodeScanner() async {
    String cameraScanResult = await BarcodeScanner.scan();
    setState(() {
      _apiKeyTextController.text = cameraScanResult;
    });
  }

  void setApiError() {
    if (_apiKeyTextController.text.isEmpty) {
      setState(() {
        _apiKeyError = "Api Key must not empty";
      });
    } else {
      setState(() {
        _apiKeyError = null;
      });
    }
  }

  void setKeyError() {
    if (_apiSecretTextController.text.isEmpty) {
      setState(() {
        _apiSecretError = "Secret Key must not empty";
      });
    } else {
      setState(() {
        _apiSecretError = null;
      });
    }
  }

  Builder buildSaveButton(BuildContext context) {
    return Builder(
      builder: (context) {
        return Material(
          elevation: 10.0,
          borderRadius: BorderRadius.circular(30.0),
          color: AppColors.colorPrimary,
          child: Container(
            height: 50,
            width: 300,
            decoration:
                BoxDecoration(borderRadius: BorderRadius.circular(30.0)),
            child: InkWell(
              onTap: () {
                _sendApiKeysToServer();
              },
              borderRadius: BorderRadius.circular(30.0),
              splashColor: Colors.white.withOpacity(.2),
              child: Container(
                color: Colors.transparent,
                child: Center(
                  child: Text(
                    "Save",
                    style: Styles.boldHeadingThreeWhite(),
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  _sendApiKeysToServer() async {
    if (_selectedExchangeID == 0) {
      // implement logic
    } else if (_apiKeyTextController.text.isEmpty ||
        _apiSecretTextController.text.isEmpty) {
      setApiError();
      setKeyError();
    } else {
      Navigator.pop(context);
      Map<String, String> body = {
        "api_key": _apiKeyTextController.text,
        "api_secret": _apiSecretTextController.text,
        "exchange": _selectedExchangeID.toString()
      };
      print("Sendning data");
      print(body);
      await _apiConfigBloc.postAPIKeys(body);
    }
  }
}

class PageChild extends StatelessWidget {
  final String pageText;
  final String pageImage;

  const PageChild(Key key, this.pageText, this.pageImage) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 5.0,vertical: 10.0),
      padding: EdgeInsets.all(8.0),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadiusDirectional.circular(10.0)),
      width: Utils().getScreenWidth(context),
      child: SingleChildScrollView(
        child: Column(
          children: List<Widget>.generate(_firstPageList.length, (index) {
            return Padding(
              padding: EdgeInsets.only(left: 15.0, top: 8.0),
              child: Row(
                textBaseline: TextBaseline.alphabetic,
                crossAxisAlignment: CrossAxisAlignment.baseline,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 8.0),
                    height: 5.0,
                    width: 5.0,
                    decoration: BoxDecoration(
                        color: Colors.black,
                        borderRadius: BorderRadius.circular(2.5)),
                  ),
                  SizedBox(
                    width: 20.0,
                  ),
                  Flexible(
                      child: Text(
                    _firstPageList[index],
                    style: Styles.semiBoldHeadingFive(),
                  )),
                  SizedBox(
                    width: 20.0,
                  ),
                ],
              ),
            );
          }),
        ),
      ),
    );
  }
}

class PageWithImage extends StatelessWidget {
  final int index;

  const PageWithImage(Key key, this.index) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(

      margin: EdgeInsets.symmetric(horizontal: 5.0,vertical: 5.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        color: Colors.white,
      ),
      child: SingleChildScrollView(
        child: Container(

          padding: EdgeInsets.only(top:10.0,right: 15.0),
          child: Column(
              children: <Widget>[
                Text(
                  _pageTitle[index],
                  style: Styles.boldHeadingThree(),
                ),
                SizedBox(
                  height: 20.0,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  textBaseline: TextBaseline.alphabetic,
                  children: <Widget>[
                    SizedBox(
                      width: 20.0,
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 4.0),
                      height: 5.0,
                      width: 5.0,
                      decoration:
                      BoxDecoration(shape: BoxShape.circle, color: Colors.green),
                    ),
                    SizedBox(
                      width: 10.0,
                    ),
                    Text(
                      "Buy When Price < Buy Limit",
                      style: Styles.semiBoldHeadingFive(),
                    )
                  ],
                ),
                SizedBox(
                  height: 20.0,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  textBaseline: TextBaseline.alphabetic,
                  children: <Widget>[
                    SizedBox(
                      width: 20.0,
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 4.0),
                      height: 5.0,
                      width: 5.0,
                      decoration:
                      BoxDecoration(shape: BoxShape.circle, color: Colors.red),
                    ),
                    SizedBox(
                      width: 10.0,
                    ),
                    Flexible(
                        child: Text(
                          "Sell, when price > Sell limit & price starts to go down",
                          style: Styles.semiBoldHeadingFive(),
                        ))
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Image.asset(
                    _pageImages[index],
                    fit: BoxFit.fill,
                    height: 100,
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  textBaseline: TextBaseline.alphabetic,
                  children: <Widget>[
                    SizedBox(
                      width: 20.0,
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 8.0),
                      height: 5.0,
                      width: 5.0,
                      decoration:
                      BoxDecoration(shape: BoxShape.circle, color: Colors.black),
                    ),
                    SizedBox(
                      width: 10.0,
                    ),
                    Flexible(
                        child: Text(
                          _pageContent[index],
                          style: Styles.semiBoldHeadingFive(),
                        ))
                  ],
                ),
              ],
          ),
        ),
      ),
    );
  }
}

class ApiKeyField extends StatelessWidget {
  final TextEditingController lNameController;
  final String lNameError;

  const ApiKeyField(Key key, this.lNameController, this.lNameError)
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: lNameController,
      autofocus: false,
//      onSaved: (value) => lName = value,
      validator: (value) {
        if (value.isEmpty) {
          return "Last name must not be empty";
        } else {
          return null;
        }
      },
      keyboardType: TextInputType.text,
      decoration: Styles.getOutlineBorder("Enter Api Key", lNameError),
      cursorColor: AppColors.colorPrimary,
    );
  }
}

class ApiKeySecretField extends StatelessWidget {
  final TextEditingController lNameController;
  final String lNameError;

  const ApiKeySecretField(Key key, this.lNameController, this.lNameError)
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: lNameController,
      autofocus: false,
//      onSaved: (value) => lName = value,
      validator: (value) {
        if (value.isEmpty) {
          return "Last name must not be empty";
        } else {
          return null;
        }
      },
      keyboardType: TextInputType.text,
      decoration: Styles.getOutlineBorder("Api Secret Key", lNameError),
      cursorColor: AppColors.colorPrimary,
    );
  }
}
