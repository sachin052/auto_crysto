import 'package:auto_crysto/routes/Router.dart';
import 'package:auto_crysto/ui/login_signup/LoginSignUp.dart';
import 'package:auto_crysto/ui/utils/AppColors.dart';
import 'package:auto_crysto/ui/utils/Images.dart';
import 'package:auto_crysto/ui/utils/Styles.dart';
import 'package:auto_crysto/ui/utils/Utils.dart';
import 'package:flutter/material.dart';
import 'package:page_indicator/page_indicator.dart';

class Tutorials extends StatefulWidget {
  @override
  _TutorialsState createState() => _TutorialsState();
}

class _TutorialsState extends State<Tutorials>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  Animation<double> _animation;
  int _currentPageIndex = 0;
  bool _btnVisibilty = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _animationController = AnimationController(
        vsync: this,
        duration: Duration(milliseconds: 600),
        lowerBound: 0.0,
        upperBound: 0.1);
    _animationController.addListener(() {
      setState(() {});
    });
  }

  @override
  void dispose() {
    super.dispose();
    _animationController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double scale = 1 - _animationController.value;
    return Material(
     color: AppColors.colorPrimary,
      child: MediaQuery(
        data: MediaQuery.of(context).size.width>600?MediaQuery.of(context).copyWith(textScaleFactor: 1.8):MediaQuery.of(context),
        child: Stack(
          children: <Widget>[
            Container(color: Colors.redAccent,),
            Material(
              child: PageIndicatorContainer(
                length: 3,
                shape: IndicatorShape.circle(size: 8.0),
                indicatorColor: AppColors.colorPrimaryGradient,
                padding: EdgeInsets.only(bottom: 50.0),
                indicatorSelectorColor: Colors.white,
                child: PageView.builder(
                  itemBuilder: (context, position) {
                    if (position == 0) {
                      return PageChild(
                          widget.key,
                          "         Automatically \ntrade Cryptos (Now) And \n      Stocks (Future)",
                          Images.tutorial_1);
                    } else if (position == 1) {
                      return PageChild(
                          widget.key,
                          "Trade Crypto Currencies on Binance, \nEven When You're Sleeping, for Free!",
                          Images.tutorial_2);
                    } else {
                      return PageChild(
                          widget.key,
                          "      Choose from 3 options- \nLow Risk, Medium Risk & High Profit",
                          Images.tutorial_3);
                    }
                  },
                  itemCount: 3,
                  onPageChanged: (index) {
                    setState(() {
                      _currentPageIndex = index;
                    });
                  },
                  pageSnapping: true,
                ),
              ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Padding(
                  padding: EdgeInsets.only(top: 40.0),
                  child: Image.asset(
                    Images.appLogoShadow,
                    height: 50,
                    width: 150,
                  )),
            ),
            Visibility(
              visible: _currentPageIndex == 2 ? false : true,
              child: Align(
                alignment: Alignment.topRight,
                child: Padding(
                    padding: EdgeInsets.only(top: 40.0, right: 10.0),
                    child: GestureDetector(
                        onTap: () {
                          Navigator.pushReplacementNamed(context, Router.loginSignUp);
                        },
                        child: Text(
                          "Skip",
                          style: TextStyle(
                              decoration: TextDecoration.underline,
                              color: Colors.white),
                        ))),
              ),
            ),
            Visibility(
              visible: _currentPageIndex == 2 ? true : false,
              child: Align(
                alignment: Alignment(.95,.95),
                child: Material(
                  elevation: 10.0,
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(20),
                  child: InkWell(
                    onTap: _onTapUp,
                    borderRadius: BorderRadius.circular(20),
                    splashColor: AppColors.colorPrimary.withOpacity(.3),
                    child: Container(
                      height: 30,
                      width: 110,
                      child: Center(
                        child: Text(
                          "Let's Start",
                          style: Styles.regularBodyThree(),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void _onTapUp() {
    Future.delayed(Duration(milliseconds: 200), () {
      Navigator.pushReplacementNamed(context, Router.loginSignUp);
    });
  }
}

class PageChild extends StatelessWidget {
  final String pageText;
  final String pageImage;

  const PageChild(Key key, this.pageText, this.pageImage) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      key: this.key,
      color: AppColors.colorPrimary,
      child: Container(
        height: Utils().getScreenHeight(context),
        width: Utils().getScreenWidth(context),
        color: AppColors.colorPrimary,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              this.pageImage,
              height: 250,
              width: 250,
            ),
            Align(
                alignment: Alignment.center,
                child: pageText.contains("Automatically")?RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                    children: [
                      TextSpan(text: 'Auto',style: Styles.semiBoldHeadingThree(color: Colors.white),),
                      TextSpan(text: 'matically\n',style: Styles.regularBodyTwo(color: Colors.white)),
                      TextSpan(text: 'trade ',style: Styles.regularBodyTwo(color: Colors.white)),
                      TextSpan(text: 'Cry',style: Styles.semiBoldHeadingThree(color: Colors.white)),
                      TextSpan(text: 'ptos (Now) And\n',style: Styles.regularBodyTwo(color: Colors.white)),
                      TextSpan(text: 'Sto',style: Styles.semiBoldHeadingThree(color: Colors.white),),
                      TextSpan(text: 'cks (Future) ',style: Styles.regularBodyTwo(color: Colors.white)),
                    ]
                  ),
                ):Text(
                  this.pageText,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                  ),
                ))
          ],
        ),
      ),
    );
  }
}
