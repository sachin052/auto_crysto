class Images{
  static final app_logo="assets/images/splash_logo.png";
  static final app_logo_white="assets/images/app_logo_white.png";
  static final tutorial_1="assets/images/tutorial_1.png";
  static final tutorial_2="assets/images/tutorial_2.png";
  static final tutorial_3="assets/images/tutorial_3.png";
  static final google_icon="assets/images/google_icon.png";
  static final fb="assets/images/fb_icon.png";
  static final profile="assets/images/profile.png";
  static final graph1="assets/images/graph1.png";
  static final graph2="assets/images/graph2.png";
  static final graph3="assets/images/graph3.png";
  static final qr="assets/images/qr.png";
  static final highRisk="assets/images/high_risk1.png";
  static final mediumRisk="assets/images/medium_risk2.png";
  static final lowRisk="assets/images/low_risk2.png";
  static final restore="assets/images/restore.png";
  static final subs="assets/images/subscription.png";
  static final diamond="assets/images/diamond.png";
  static final history="assets/images/history.png";
  static final curveBg="assets/images/curve_bg.png";
  static final appLogoShadow="assets/images/app_logo_shadow.png";
  static final invalidKey="assets/images/invalid_key.png";
  static final noInternet="assets/images/no_internet.png";
  static final error="assets/images/error.png";
  static List<String> menuImages=[
    "assets/images/trading.png",
    "assets/images/history.png",
    "assets/images/trading_ins.png",
    "assets/images/setting.png",
    "assets/images/change_pass.png",
    "assets/images/support.png",
    "assets/images/about.png",
    "assets/images/share.png",
    "assets/images/rate_us.png"
  ];
}