import 'package:flutter/material.dart';
class AppColors{
  static final colorPrimary=Color(0xFF159EA1);
  static final colorPrimaryGradient=Color(0xFF19C49B);
  static final gradient= LinearGradient(
  colors: [
  AppColors.colorPrimary,
  AppColors.colorPrimaryGradient
  ],
  begin: Alignment.bottomCenter,
  stops: [0.3, 0.8],
  end: Alignment.topCenter);
}