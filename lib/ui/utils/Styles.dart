import 'package:flutter/material.dart';

import 'AppColors.dart';

class Styles {
  static String familyName = "Montserrat";

  static InputDecoration getOutlineBorder(String text, String error) {
    return InputDecoration(
      contentPadding: EdgeInsets.all(14.0),
      labelText: text,
      hintStyle: Styles.semiBoldHeadingThree(color: AppColors.colorPrimary),
      errorText: error,
      border: OutlineInputBorder(
          borderSide: BorderSide(color: AppColors.colorPrimary, width: 1.0)),
    );
  }

  static customOutlinedBorder(String errorText,String labelText,){
    var border = OutlineInputBorder(
        borderSide: BorderSide(color: AppColors.colorPrimary, width: 1.0));
    var defStyles=Styles.semiBoldHeadingFour(color: Colors.black.withOpacity(.5));
    var style = Styles.semiBoldHeadingFourGrey();
    var errorStyle = Styles.semiBoldHeadingFive(color: Colors.red);
    return  InputDecoration(
    border: border,
    errorText: errorText,
    labelText: labelText,
    errorStyle: errorStyle,
    labelStyle: style,
    );
  }

  static InputDecoration getOutlineBorderWihSuffix(String text, String error) {
    return InputDecoration(
      contentPadding: EdgeInsets.all(14.0),
      labelText: text,
      hintStyle: Styles.semiBoldHeadingThree(color: AppColors.colorPrimary),
      errorText: error,
      suffix: Icon(Icons.arrow_drop_down),
      border: OutlineInputBorder(
          borderSide: BorderSide(color: AppColors.colorPrimary, width: 1.0)),
    );
  }

  static TextStyle semiBoldHeadingOne() {
    return TextStyle(
        fontFamily: familyName, fontWeight: FontWeight.w600, fontSize: 20.0);
  }

  static TextStyle semiBoldHeadingTwo({Color color = Colors.black}) {
    return TextStyle(
        fontFamily: familyName,
        fontWeight: FontWeight.w600,
        fontSize: 18.0,
        color: color);
  }

  static TextStyle semiBoldHeadingThree({Color color = Colors.black}) {
    return TextStyle(
        fontFamily: familyName,
        fontWeight: FontWeight.w600,
        fontSize: 16.0,
        color: color);
  }

  static TextStyle semiBoldHeadingFour({Color color = Colors.black}) {
    return TextStyle(
        fontFamily: familyName,
        fontWeight: FontWeight.w600,
        fontSize: 14.0,
        color: color);
  }

  static TextStyle semiBoldHeadingFive({Color color = Colors.black}) {
    return TextStyle(
        fontFamily: familyName,
        fontWeight: FontWeight.w600,
        fontSize: 12.0,
        color: color);
  }

  static TextStyle semiBoldHeadingFourGrey() {
    return TextStyle(
        fontFamily: familyName,
        fontWeight: FontWeight.w600,
        fontSize: 14.0,
        color: Colors.grey.withOpacity(.5));
  }

  static TextStyle boldHeadingOne() {
    return TextStyle(
        fontFamily: familyName,
        fontWeight: FontWeight.w800,
        fontSize: 20.0,
        color: Colors.white);
  }

  static TextStyle boldHeadingOneBlack() {
    return TextStyle(
        fontFamily: familyName, fontWeight: FontWeight.w800, fontSize: 20.0);
  }

  static TextStyle boldHeadingOneBWhite() {
    return TextStyle(
        fontFamily: familyName,
        fontWeight: FontWeight.w800,
        fontSize: 20.0,
        color: Colors.white);
  }

  static TextStyle boldHeadingTwo({Color colors = Colors.black}) {
    return TextStyle(
        fontFamily: familyName,
        fontWeight: FontWeight.w800,
        fontSize: 18.0,
        color: colors);
  }

  static TextStyle boldHeadingTwoWhite() {
    return TextStyle(
        fontFamily: familyName,
        fontWeight: FontWeight.w800,
        fontSize: 18.0,
        color: Colors.white);
  }

  static TextStyle boldHeadingThree() {
    return TextStyle(
        fontFamily: familyName, fontWeight: FontWeight.w800, fontSize: 15.0);
  }

  static TextStyle boldHeadingThreeWhite() {
    return TextStyle(
        fontFamily: familyName,
        fontWeight: FontWeight.w800,
        fontSize: 15.0,
        color: Colors.white);
  }

  static TextStyle boldHeadingFour() {
    return TextStyle(
        fontFamily: familyName, fontWeight: FontWeight.w800, fontSize: 13.0);
  }

  static TextStyle regularBodyOne({Color color=Colors.white}) {
    return TextStyle(fontFamily: familyName, fontSize: 18.0,color: color);
  }

  static TextStyle regularBodyTwo({Color color = Colors.black}) {
    return TextStyle(fontFamily: familyName, fontSize: 16.0, color: color);
  }

  static TextStyle regularBodyThree(
      {Color color = Colors.black, double fontSize = 13.0}) {
    return TextStyle(
        fontFamily: familyName, fontSize: fontSize, height: 1.3, color: color);
  }

  static TextStyle regularBodyThreeWhite() {
    return TextStyle(
        fontFamily: familyName,
        fontSize: 13.0,
        height: 1.3,
        color: Colors.white);
  }

  static TextStyle menuItems() {
    return TextStyle(
        fontFamily: familyName,
        fontSize: 14.0,
        height: 1.3,
        color: Colors.white);
  }

  static ThemeData getTheme() {
    return ThemeData(
      fontFamily: "Montserrat",
      cursorColor: AppColors.colorPrimary,
      textSelectionColor: AppColors.colorPrimary,
      primaryColor: AppColors.colorPrimary,
        textSelectionHandleColor: AppColors.colorPrimary
    );
  }
}
