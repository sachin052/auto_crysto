import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:flutter_luban/flutter_luban.dart';

import 'AppColors.dart';
// Contains various common methods
class Utils {
  Utils._privateConstructor();

  static final Utils _instance = Utils._privateConstructor();

  factory Utils() {
    return _instance;
  }

  double getScreenHeight(BuildContext context) {
    return MediaQuery.of(context).size.height;
  }
  double getScreenWidth(BuildContext context) {
    return MediaQuery.of(context).size.width;
  }
  static showShowSnackBar(BuildContext context,String text,bool isError){
    Scaffold.of(context).showSnackBar(SnackBar(content: Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Icon(isError?Icons.cancel:Icons.done,color: Colors.white,),
        SizedBox(width: 20.0,),
        Flexible(child: Text(text,style: TextStyle(color:Colors.white,fontSize: 16.0),)),
      ],
    ),backgroundColor: isError?Colors.red:AppColors.colorPrimary,));
  }
  static  onLoading(String text,BuildContext context,{bool longSnackBar=false}) {
     Scaffold.of(context).showSnackBar(SnackBar(
       duration: Duration(milliseconds: longSnackBar?60000:6000),
      content: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          CircularProgressIndicator(
              backgroundColor: Colors.white,
              valueColor:
              AlwaysStoppedAnimation<Color>(AppColors.colorPrimary)),
          SizedBox(
            width: 20.0,
          ),
          Text(
            text,
            style: TextStyle(color: Colors.white, fontSize: 16.0),
          ),
        ],
      ),
      backgroundColor: AppColors.colorPrimary,
    ));
  }
  static ScaffoldFeatureController onLoadingWithKey(GlobalKey<ScaffoldState> key,String text) {
    return key.currentState.showSnackBar(SnackBar(
      content: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          CircularProgressIndicator(
              backgroundColor: Colors.white,
              valueColor:
              AlwaysStoppedAnimation<Color>(AppColors.colorPrimary)),
          SizedBox(
            width: 20.0,
          ),
          Text(
            text,
            style: TextStyle(color: Colors.white, fontSize: 16.0),
          ),
        ],
      ),
      backgroundColor: AppColors.colorPrimary,
    ));
  }
  static showShowSnackBarWithKey(GlobalKey<ScaffoldState> key,String text,bool isError){
    key.currentState.showSnackBar(SnackBar(content: Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Icon(isError?Icons.cancel:Icons.done,color: Colors.white,),
        SizedBox(width: 20.0,),
        Flexible(child: Text(text,style: TextStyle(color:Colors.white,fontSize: 16.0),)),
      ],
    ),backgroundColor: isError?Colors.red:AppColors.colorPrimary,));
  }
  static void showAnimatedDialog(BuildContext context,Widget child) {
//  showDialog(context: context,builder: (_)=>ForgetDialog());
    showGeneralDialog(
        barrierColor: Colors.black.withOpacity(0.5),
        transitionBuilder: (context, a1, a2, widget) {
          final curvedValue = Curves.easeInOutBack.transform(a1.value) -   1.0;
          return Transform(
            transform: Matrix4.translationValues(0.0, curvedValue * 400, 0.0),
            child: Opacity(
              opacity: a1.value,
              child: child,
            ),
          );
        },
        transitionDuration: Duration(milliseconds: 400),
        barrierDismissible: true,
        barrierLabel: '',
        context: context,
        pageBuilder: (context, animation1, animation2) {});
  }
  // Used Image compression for faster uploading
  static Future<File> compressImage(File file, String targetPath) async {
    CompressObject compressObject = CompressObject(
      imageFile:file, //image
      path:targetPath, //compress to path
      quality: 85,//first compress quality, default 80
      step: 9,//compress quality step, The bigger the fast, Smaller is more accurate, default 6
      mode: CompressMode.LARGE2SMALL,//default AUTO
    );
     var path=await Luban.compressImage(compressObject);
//    var result = await FlutterImageCompress.compressAndGetFile(
//      file.absolute.path, targetPath,
//      quality: 88,
//      rotate: 180,
//    );
     return File(path);
  }
  static showProgressDialog(BuildContext context, String title) {
    try {
      showCupertinoDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              content: Flex(
                direction: Axis.horizontal,
                children: <Widget>[
                  CircularProgressIndicator(valueColor:  AlwaysStoppedAnimation<Color>(AppColors.colorPrimary)),
                  Padding(padding: EdgeInsets.only(left: 15),),
                  Flexible(
                      flex: 8,
                      child: Text(
                        title,
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold),
                      )),
                ],
              ),
            );
          });
    } catch (e) {
      print(e.toString());
    }
  }
}

