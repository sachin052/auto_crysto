
import 'package:auto_crysto/ui/commonui/CustomMaterialButton.dart';
import 'package:auto_crysto/ui/utils/AppColors.dart';
import 'package:auto_crysto/ui/utils/Styles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';


 Future<Widget> dialogWithOneBtn(BuildContext context,String title,String subTitle,VoidCallback onButtonPressed)async{
   return await showCupertinoDialog(context: context, builder: (context)=>CupertinoAlertDialog(

      title: Text(title,style: Styles.semiBoldHeadingFour(),),
      content: Text(subTitle,style: Styles.regularBodyThree(),),
      actions: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Container(
              width: 80,
              margin: EdgeInsets.all(4.0),
              child: MaterialButton(
                onPressed: onButtonPressed,
                child: Text("Ok",style: Styles.semiBoldHeadingFour(color: Colors.white),),
                color: AppColors.colorPrimary,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0)),
              ),
            ),
          ],
        )
      ],
    ));

  }

Future<Widget> dialogWithTwoBtn(BuildContext context,String title,String subTitle,VoidCallback onButtonPressed)async{
  return await showCupertinoDialog(context: context, builder: (context)=>CupertinoAlertDialog(
    title: Text(title,style: Styles.semiBoldHeadingFour(),),
    content: Text(subTitle,style: Styles.regularBodyThree(),),
    actions: <Widget>[
      CupertinoDialogAction(isDefaultAction: true, child: new Text("OK",style: Styles.semiBoldHeadingFour(color: CupertinoColors.destructiveRed),),onPressed: onButtonPressed,),
      CupertinoDialogAction(isDefaultAction: true, child: new Text("Cancel",style: Styles.semiBoldHeadingFour(color: CupertinoColors.destructiveRed),),onPressed: onButtonPressed,)
    ],
  ));
}


