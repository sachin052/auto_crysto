import 'package:auto_crysto/ui/utils/AppColors.dart';
import 'package:auto_crysto/ui/utils/Styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as prefix0;
class CustomInputForm extends StatefulWidget {
  final VoidCallback onTap;
  final String labelText;
  final VoidCallback onChanged;
  final TextEditingController text;
  final FocusNode node;
  final BuildContext parentContext;
  const CustomInputForm(Key key, this.onTap, this.labelText, this.onChanged, this.text, this.node, this.parentContext) : super(key: key);@override
  _CustomInputFormState createState() => _CustomInputFormState();
}

class _CustomInputFormState extends State<CustomInputForm> {
  FocusNode myFocusNode=FocusNode();
  var style = Styles.semiBoldHeadingFourGrey();
  var border = OutlineInputBorder(
      borderSide: BorderSide(color: AppColors.colorPrimary, width: 1.0));
  var keyboard = TextInputType.numberWithOptions(decimal: false);
  @override
  Widget build(BuildContext context) {
    return TextField(
      autofocus: false,
      controller: widget.text,
      onChanged: (value){
        setState(() {
//          print("value is ${value}");
          widget.text.text=value;
          print("value is ${widget.text.text}");
        });
      },
        onTap: (){

        },
        focusNode: widget.node,
        decoration: InputDecoration(
          errorText: widget.text!=null&&widget.text.text.length>0&&widget.text.text.isEmpty?"Field Can't be empty":null,
            labelText: widget.labelText,
            labelStyle: widget.node.hasFocus
                ? Styles.semiBoldHeadingFour()
                : style,
            border: border),
        cursorColor: AppColors.colorPrimary,
        keyboardType: keyboard);
  }
}
