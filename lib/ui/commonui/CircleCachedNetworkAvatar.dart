import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class CircleCachedNetworkAvatar extends StatelessWidget {
  final String url;
  final double size;

  CircleCachedNetworkAvatar({@required this.url, this.size = 50.0});

  @override
  Widget build(BuildContext context) {
    return Container(
        height: size,
        width: size,
        child: url != null
            ? ClipOval(
          child: CachedNetworkImage(
            fadeInDuration: const Duration(seconds: 0),
            fadeOutDuration: const Duration(seconds: 0),
            imageUrl: url,
            placeholder: (context,c)=>Container(color: Colors.greenAccent, child: Icon(Icons.person)),
            fit: BoxFit.cover,
          ),
        )
            : CircleAvatar(
            backgroundColor: Colors.greenAccent,
            child: Icon(Icons.person)));
  }
}