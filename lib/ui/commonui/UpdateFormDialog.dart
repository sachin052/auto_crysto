import 'package:auto_crysto/ui/utils/AppColors.dart';
import 'package:auto_crysto/ui/utils/Images.dart';
import 'package:auto_crysto/ui/utils/Styles.dart';
import 'package:flutter/material.dart';

class UpdateFormDialog extends StatefulWidget {
  final VoidCallback onTapUpdate;
  final String planName;
  final String changePlanName;

  const UpdateFormDialog(
      {Key key, this.onTapUpdate, this.planName, this.changePlanName})
      : super(key: key);

  @override
  _UpdateFormDialogState createState() => _UpdateFormDialogState();
}

class _UpdateFormDialogState extends State<UpdateFormDialog> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(borderRadius: BorderRadius.only(topLeft: Radius.circular(10.0),topRight: Radius.circular(10.0)),color: Colors.white,),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          SizedBox(
            height: 5,
          ),
          Container(
            height: 3,
            width: 80,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
                color: AppColors.colorPrimary.withOpacity(.5)),
          ),
          SizedBox(
            height: 30.0,
          ),
          Material(
            elevation: 5.0,
            borderRadius: BorderRadius.circular(40.0),
            child: CircleAvatar(
              radius: 40.0,
              backgroundColor: Colors.transparent,
              backgroundImage: AssetImage(Images.diamond),
            ),
          ),
          SizedBox(
            height: 10.0,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text.rich(
              TextSpan(style: Styles.regularBodyTwo(), children: [
                TextSpan(
                  text: "Curently ",
                ),
                TextSpan(
                    text: widget.planName,
                    style: Styles.semiBoldHeadingThree()),
                TextSpan(
                    text:
                    " is active, Kindly confirm if you want to activate "),
                TextSpan(
                    text: widget.changePlanName,
                    style: Styles.semiBoldHeadingThree())
              ]),
              textAlign: TextAlign.center,
            ),
          ),
          RaisedButton(
            padding: EdgeInsets.all(10.0),
            color: AppColors.colorPrimary,
            child: Container(
              width: MediaQuery.of(context).size.width - 100,
              child: Center(
                  child: Text(
                    "Update",
                    style: Styles.semiBoldHeadingThree(color: Colors.white),
                  )),
            ),
            onPressed: widget.onTapUpdate,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)),
          ),
          RaisedButton(
            padding: EdgeInsets.all(10.0),
            color: Colors.grey,
            child: Container(
              width: MediaQuery.of(context).size.width - 100,
              child: Center(
                  child: Text(
                    "Cancel",
                    style: Styles.semiBoldHeadingThree(color: Colors.white),
                  )),
            ),
            onPressed: () {
              Navigator.pop(context);
            },
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)),
          ),
          SizedBox(
            height: 10.0,
          ),
        ],
      ),
    );
  }
}
