
import 'dart:io';

import 'package:auto_crysto/ui/utils/AppColors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

typedef File MyCallback(File foo);
    androidPicker(context,MyCallback myCallback) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10), topRight: Radius.circular(10.0)),
        ),
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: new Wrap(
              children: <Widget>[
                ListTile(
                  title: Center(
                      child: new Text('Pick Photo',
                          style: TextStyle(
                              color: AppColors.colorPrimary, fontSize: 20.0))),
                ),
                ListTile(
                    leading: new Icon(
                      Icons.camera,
                      color: AppColors.colorPrimary,
                    ),
                    title: new Text('Camera',
                        style: TextStyle(color: AppColors.colorPrimary)),
                   onTap: ()async{
                      Navigator.pop(context);
                     var file= await getImageFromCamera();
                     myCallback(file);
                   },
                ),
                ListTile(
                  leading: new Icon(
                    Icons.videocam,
                    color: AppColors.colorPrimary,
                  ),
                  title: new Text(
                    'Gallery',
                    style: TextStyle(color: AppColors.colorPrimary),
                  ),
                  onTap: ()async{
                    Navigator.pop(context);
                    var file =await getImageFromGallery(context);
                    myCallback(file);
                  },
                ),
              ],
            ),
          );
        });
  }
  Future<File>  getImageFromGallery(BuildContext context) async {
    return await ImagePicker.pickImage(source: ImageSource.gallery);
//    setState(() {
////      _image = image;
//      Navigator.of(context).pop();
//    });
  }
  Future<File> getImageFromCamera() async {
    return await ImagePicker.pickImage(source: ImageSource.camera);

//    setState(() {
////      _image = image;
//      Navigator.of(context).pop();
//    });
  }
  actionSheetMethodIOS(BuildContext context,MyCallback myCallback) {
    showCupertinoModalPopup(
        context: context,
        builder: (context) {
          return CupertinoActionSheet(
            title: Text("Profile Photo"),
            message: Text("Pick your prefered photos source"),
            actions: <Widget>[
              CupertinoActionSheetAction(
                  onPressed: () async{
                    var file=await getImageFromCamera();
                    myCallback(file);
                    Navigator.of(context,rootNavigator: true).pop();
                  },
                  child: Material(
                      color: Colors.transparent,
                      child: Text(
                        "Camera - Capture new photo",
                        style: TextStyle(
                            fontSize: 18.0, color: AppColors.colorPrimary),
                      ))),
              CupertinoActionSheetAction(
                  onPressed: () async{
                    Navigator.of(context,rootNavigator: true).pop();
                    var file=await getImageFromGallery(context);
                    myCallback(file);
//                    Navigator.pop(context);
                  },
                  child: Material(
                      child: Text("Photo Albums",
                          style: TextStyle(
                              color: AppColors.colorPrimary, fontSize: 18.0)))),
            ],
            cancelButton: CupertinoActionSheetAction(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(
                  "Cancel",
                  style: TextStyle(color: Colors.red.withOpacity(.8)),
                )),
          );
        });
  }

