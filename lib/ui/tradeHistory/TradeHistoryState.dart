import 'package:auto_crysto/ui/apiresponseui/Loading.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';

import 'TradeHistory.dart';

class TradeHistoryState with ChangeNotifier {

  TradeStatus currentState=TradeStatus.onLoading;

  Widget getCurrentState() {
    if (currentState == TradeStatus.onLoading) {
      return onLoading();
    }
    else if (currentState == TradeStatus.onError) {
      return null;
    }
    else {
      return Container(child: Text("Complete state"),);
    }
  }

  void updateState(TradeStatus status) {
    currentState = status;
    notifyListeners();
  }
}
enum TradeStatus {
  onLoading,
  onError,
  onComplete
}