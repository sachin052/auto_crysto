import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:auto_crysto/ui/data/ApiHelper/ApiBaseHelper.dart';
import 'package:auto_crysto/ui/data/ApiHelper/ApiResponse.dart';
import 'package:auto_crysto/ui/data/models/ApiConfigList.dart';
import 'package:auto_crysto/ui/data/models/TradeModel.dart';
import 'package:convert/convert.dart';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:crypto/crypto.dart';

class TradeBloc
{
  StreamController<ApiResponse<List<TradeModel>>> controller;
  StreamSink<ApiResponse<List<TradeModel>>> get sink=>controller.sink;
  Stream<ApiResponse<List<TradeModel>>> get stream=>controller.stream;
  TradeBloc(){
    controller=StreamController<ApiResponse<List<TradeModel>>>();
  }
  getTradeHistory(ApiConfigModel model)async{
    if(model.apiKey.isEmpty){
      sink.add(ApiResponse.error("Api Key is empty"));
      return;
    }
    else if(model.apiSecret.isEmpty){
      sink.add(ApiResponse.error("Api Secret is empty"));
      return;
    }
    sink.add(ApiResponse.loading("Loading"));
    try{
    await getOrders(model);
    }catch(e){
      sink.add(ApiResponse.error(e.toString()));
    }
  }
  getOrders(ApiConfigModel model)async{

    String baseUrl = 'https://api.binance.com/api/v3/allOrders?';
    String symbol='BTCUSDT';
//    String symbol='LTCBTC';
    int timeStamp = DateTime.now().millisecondsSinceEpoch;
    String queryParams = 'recvWindow=5000' + '&limit=1000'+ '&timestamp='+timeStamp.toString()+'&symbol=$symbol';
    String secret =model.apiSecret;
//    String secret ="PVPTPpHXIDoLpHG6KrGCoSiizpiXS3tez21JV4hbKDaeSE61jYXZpDs7CExsCTHQ";

    List<int> messageBytes = utf8.encode(queryParams);
    List<int> key = utf8.encode(secret);
    Hmac hmac = new Hmac(sha256, key);
    Digest digest = hmac.convert(messageBytes);
    String signature = hex.encode(digest.bytes);
    String url = baseUrl + queryParams + "&signature=" + signature;
    var dio=Dio();
    dio.options.baseUrl=baseUrl;
    dio.options.connectTimeout=60000;
    dio.options.receiveTimeout=60000;
//    showDialog(context: context,builder: (c)=>onLoading(colors: AppColors.colorPrimary));
    try {
      var response=await dio.get(url,options: Options(
          headers: {
            "Accept": "application/json",
            "HTTP_ACCEPT_LANGUAGE": "en-US",
            "Accept-Language": "en-US",
            "X-MBX-APIKEY": model.apiKey
//            "X-MBX-APIKEY": "n5Mej2JOFydaXo52qEvUpQtynXry2z71KxliK2ycrxUbtsVDGZ7sWWkVKGocxqRp"
          }
      ),);
      
      sink.add(ApiResponse.completed(tradeHistoryFromJson(jsonEncode(response.data))));
      print(jsonEncode(response.data));
    } on DioError catch(e) {

      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.
      if(e.error is SocketException){
        sink.add(ApiResponse.error("Connection to API server failed due to internet connection"));
      }
      else if(e.response != null) {

        print(e.response.data);
        sink.add(ApiResponse.error(jsonDecode(jsonEncode(e.response.data))["msg"]));
//        Fluttertoast.showToast(msg: jsonDecode(jsonEncode(e.response.data))["msg"]);
        print(e.response.headers);
        print(e.response.request);
      }
      else{
        // Something happened in setting up or sending the request that triggered an Error
        var error=AppException(handleError(e), "");
        sink.add(ApiResponse.error(error.toString()));
        print(e.request);
        print(e.message);
//        Fluttertoast.showToast(msg: e.message);
      }
    }

  }
  void dispose(){
    controller.close();
  }

}