import 'package:auto_crysto/ui/data/ApiHelper/ApiResponse.dart';
import 'package:auto_crysto/ui/data/models/ApiConfigList.dart';
import 'package:auto_crysto/ui/data/models/TradeModel.dart';
import 'package:auto_crysto/ui/tradeHistory/TradeBloc.dart';
import 'package:auto_crysto/ui/utils/AppColors.dart';
import 'package:auto_crysto/ui/utils/Images.dart';
import 'package:auto_crysto/ui/utils/Styles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class TradeHistory extends StatefulWidget {
  final ApiConfigModel model;

  const TradeHistory({Key key, this.model}) : super(key: key);

  @override
  _TradeHistoryState createState() => _TradeHistoryState();
}

class _TradeHistoryState extends State<TradeHistory>
    with SingleTickerProviderStateMixin {
  TradeBloc tradeBloc;
  Animation<double> _animation;
  AnimationController controller;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    controller =
        AnimationController(vsync: this, duration: Duration(seconds: 1));
    _animation = Tween(begin: 0.0, end: 1.0).animate(controller);
    tradeBloc = TradeBloc();
    if(widget.model==null){
      tradeBloc.sink.add(ApiResponse.error("Couldn’t fetch the Trading History! Set API-Key first"));
    }
    else{
      tradeBloc.getTradeHistory(widget.model);
    }

  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: Styles.getTheme(),
      home: MediaQuery(
        data: MediaQuery.of(context).size.width>600?MediaQuery.of(context).copyWith(textScaleFactor: 1.2):MediaQuery.of(context),
        child: Scaffold(
          appBar:
          AppBar(
                backgroundColor: AppColors.colorPrimaryGradient,
                title: Material(
                  type: MaterialType.transparency,
                  child: Text(
                    "Trading History",
                    style: Styles.semiBoldHeadingThree(color: Colors.white,),
                  ),
                ),
                centerTitle: true,

                elevation: 0.0,

                leading: IconButton(
                    icon: Icon(Icons.arrow_back),
                    onPressed: () {
                      Navigator.pop(context);
                    }),
              ),
          body: Stack(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(gradient: AppColors.gradient),
              ),

              StreamBuilder<ApiResponse<List<TradeModel>>>(
                stream: tradeBloc.stream,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    var data = snapshot.data;
                    switch (data.status) {
                      case Status.LOADING:
                        return Container(
                          color: AppColors.colorPrimary.withOpacity(.2),
                          margin: EdgeInsets.only(bottom: 10.0),
                          child: Align(
                              alignment: Alignment.center,
                              child: CircularProgressIndicator(
                                valueColor:
                                    AlwaysStoppedAnimation<Color>(Colors.white),
                              )),
                        );
                        break;
                      case Status.COMPLETED:
                        if (data.data.isEmpty) {
                          return Container(
                            child: Center(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  RotatedBox(
                                    quarterTurns: 4,
                                    child: Icon(
                                      Icons.error_outline,
                                      color: Colors.white.withOpacity(.8),
                                      size: 180.0,
                                    ),
                                  ),
                                  Text(
                                    'Oops!',
                                    style: Styles.semiBoldHeadingTwo(
                                        color: Colors.white),
                                  ),
                                  SizedBox(height: 10.0),
                                  Text(
                                    "No Transaction History Found",
                                    style: Styles.regularBodyTwo(
                                        color: Colors.white),
                                  )
                                ],
                              ),
                            ),
                          );
                        }
                        return RefreshIndicator(
                          color: AppColors.colorPrimary,
                          onRefresh: ()async{
                            await tradeBloc.getTradeHistory(widget.model);
                          },
                          child: ListView.builder(
                            padding: EdgeInsets.only(top: 10.0),
                            itemBuilder: (context, index) {
                              return buildHistoryCard(data.data.reversed.toList()[index]);
                            },
                            itemCount: data.data.length,
                          ),
                        );
                        break;
                      case Status.ERROR:
                        return Container(
                          child: buildErrorView(data.message,data.message.contains("Couldn’t fetch the Trading History! Set API-Key first")?"OK":"RETRY"),
                        );
                        break;
                      default:
                        return Container();
                    }
                  } else {
                    return Container();
                  }
                },
              ),


            ],
          ),
        ),
      ),
    );
  }

  Center buildErrorView(String msg,String buttonText) {
    return Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            RotatedBox(
                              quarterTurns: 4,
                              child: Image.asset(
                                msg.contains("internet")?Images.noInternet:Images.invalidKey,
                                height: 200,width: 250,

                              ),
                            ),
                            SizedBox(height: 15.0),
                            Text(
                              'Oops!',
                              style: Styles.semiBoldHeadingTwo(
                                  color: Colors.white),
                            ),
                            SizedBox(height: 10.0),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                msg,
                                textAlign: TextAlign.center,
                                style:
                                    Styles.regularBodyTwo(color: Colors.white),
                              ),
                            ),
                            SizedBox(height: 10.0),
                            RaisedButton(
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0),side: BorderSide(color: Colors.white,width: 2.0)),
                              color: AppColors.colorPrimary,
                              child: Text(buttonText, style: Styles.semiBoldHeadingFive(color: Colors.white)),
                              onPressed:()async{
                                buttonText=="RETRY"?await  tradeBloc.getTradeHistory(widget.model):Navigator.pop(context,"setApiConfig");

                                },
                            )
                          ],
                        ),
                      );
  }

  Widget buildHistoryCard(TradeModel model) {
    controller.forward();
    Color currentColor = model.side == "SELL" ? Colors.redAccent : Colors.green;
    var time = DateTime.fromMicrosecondsSinceEpoch(model.updateTime * 1000,
        isUtc: false);
    var format = DateFormat("d MMM y hh:mm a");
    return FadeTransition(
      opacity: _animation,
      child: Card(
        margin: EdgeInsets.only(top: 5.5, bottom: 5.5, right: 8.0, left: 8.0),
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        elevation: 5.0,
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                      model.symbol.substring(0, 3) +
                          " / " +
                          model.symbol.substring(3, 6) +
                          " (${model.orderId.toString()})",
                      style: Styles.semiBoldHeadingFour()),
                  Container(
                    padding: EdgeInsets.only(
                        top: 4.0, left: 16.0, right: 16.0, bottom: 4.0),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        border: Border.all(color: currentColor, width: 1.0)),
                    child: Text(
                      model.side,
                      style: Styles.semiBoldHeadingFive(
                        color: currentColor,
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 5.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    format.format(time),
                    style: Styles.regularBodyThree(fontSize: 13.0),
                  ),
                  Text(
                    model.status,
                    style: Styles.semiBoldHeadingFour(),
                  )
                ],
              ),
              SizedBox(
                height: 5.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    model.price + " " + model.symbol.substring(0, 3),
                    style: Styles.semiBoldHeadingFour(
                        color: Colors.black.withOpacity(.7)),
                  ),
                  Text(model.executedQty,
                      style: Styles.semiBoldHeadingFour(
                          color: Colors.black.withOpacity(.7)))
                ],
              ),
              SizedBox(
                height: 5.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Price",
                    style: Styles.semiBoldHeadingFive(
                        color: Colors.black.withOpacity(.6)),
                  ),
                  Text(
                    "Amount",
                    style: Styles.semiBoldHeadingFive(
                        color: Colors.black.withOpacity(.6)),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
