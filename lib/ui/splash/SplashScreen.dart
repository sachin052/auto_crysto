import 'dart:async';
import 'dart:io';

import 'package:auto_crysto/routes/Router.dart';
import 'package:auto_crysto/ui/apiresponseui/Loading.dart';
import 'package:auto_crysto/ui/data/sharedPrefHelper/SharedPrefHelper.dart';
import 'package:auto_crysto/ui/utils/AppColors.dart';
import 'package:auto_crysto/ui/utils/Images.dart';
import 'package:auto_crysto/ui/utils/Utils.dart';
import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  Animation<double> _animation;
  String screenToLoad;

  @override
  void initState() {
    super.initState();
    _animationController = new AnimationController(
        vsync: this, duration: new Duration(milliseconds: 500));
    loadNextPage();
  }

  @override
  Widget build(BuildContext context) {
    var utils = Utils();
    return MediaQuery(
      data: MediaQuery.of(context).size.width>600?MediaQuery.of(context).copyWith(textScaleFactor: 1.8):MediaQuery.of(context),
      child: Scaffold(
        body: Container(
          height: utils.getScreenHeight(context),
          width: utils.getScreenWidth(context),
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [
                    AppColors.colorPrimary,
                    AppColors.colorPrimaryGradient
                  ],
                  begin: Alignment.bottomCenter,
                  stops: [0.3, 0.8],
                  end: Alignment.topCenter)),
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image.asset(
                  Images.app_logo,
                  height: 180,
                  width: 180,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "The Bull Market is here! AutoMagically Trade Cryptos using our Mobile Apps!",
                    style: TextStyle(
                      color: Colors.white,
                    ),textAlign: TextAlign.center,
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  loadNextPage() {
    _animation = new CurvedAnimation(
      parent: _animationController,
      curve: Curves.easeOut,
    );
    checkCounter();
  }

  checkCounter() async {
    PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
      String appName = packageInfo.appName;
      String packageName = packageInfo.packageName;
      String version = packageInfo.version;
      String buildNumber = packageInfo.buildNumber;
      print(version);
      print(buildNumber);
    });
    SharedPreferences pref = await SharedPreferences.getInstance();
    var data = await SharedPrefHelper.getUserData();
    var token = await SharedPrefHelper.getUserToken();
    if (data != null) {
      print("AuthToken:${token.accessToken}");
      screenToLoad = Router.home;
    } else if (pref.getBool("first_open") == null) {
      pref.setBool('first_open', true);
      screenToLoad = Router.tutorials;
    } else {
      screenToLoad = Router.loginSignUp;
    }
    Timer(Duration(milliseconds: 1000), () {
      Navigator.pushReplacementNamed(context, screenToLoad);
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _animationController.dispose();
  }
}
