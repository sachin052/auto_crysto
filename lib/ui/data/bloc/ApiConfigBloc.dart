

import 'dart:async';
import 'package:auto_crysto/ui/data/ApiHelper/ApiBaseHelper.dart';
import 'package:auto_crysto/ui/data/ApiHelper/ApiResponse.dart';
import 'package:auto_crysto/ui/data/ApiHelper/Repo/AppRepo.dart';
import 'package:auto_crysto/ui/data/models/ApiConfigList.dart';
import 'package:auto_crysto/ui/data/models/CommonApiResponse.dart';
import 'package:auto_crysto/ui/data/models/ExchangeModel.dart';


class ApiConfigBloc{
  AppRepo _appReo;
  StreamController streamController;
  StreamController postStreamController;
  StreamController apiConfigListController;

  StreamSink<ApiResponse<List<ExchangeModel>>> get exchangeSink=>streamController.sink;
  StreamSink<ApiResponse<CommonApiResponse>> get _apiSink=>postStreamController.sink;
  StreamSink<ApiResponse<ApiConfigList>> get _apiConfigSink=>apiConfigListController.sink;

  Stream<ApiResponse<List<ExchangeModel>>> get exchangeStream=>streamController.stream;
  Stream<ApiResponse<CommonApiResponse>> get apiPostStream=>postStreamController.stream;
  Stream<ApiResponse<ApiConfigList>> get apiConfigStream=>apiConfigListController.stream;

  void dispose(){
    streamController.close();
    postStreamController.close();
    apiConfigListController.close();
  }
  ApiConfigBloc(){
    _appReo=AppRepo();
    apiConfigListController=StreamController<ApiResponse<ApiConfigList>>.broadcast();
    postStreamController=StreamController<ApiResponse<CommonApiResponse>>.broadcast();
    streamController=StreamController<ApiResponse<List<ExchangeModel>>>();
  }

   fetchExchangeList()async{
    exchangeSink.add(ApiResponse.loading("Loading Exhanges"));
    try{
      var response= await _appReo.getExchangeList();
      exchangeSink.add(ApiResponse.completed(response));
      print(response);
    }catch(e){

      exchangeSink.add(ApiResponse.error(e.toString()));
    }
   }
   postAPIKeys(Map<String,String> body)async{
    _apiSink.add(ApiResponse.loading("Saving keys"));
    try{
      var response=await _appReo.postExchangeApiKeys(body);
      _apiSink.add(ApiResponse.completed(response));
    }catch(e){
      _apiSink.add(ApiResponse.error(e.toString()));
    }
   }
   updateApiKeys(String userExchangeID,Map<String,String> body)async{
     _apiSink.add(ApiResponse.loading("Updating keys"));
     try{
       var response=await _appReo.updateExchangeApiKeys(userExchangeID,body);
       _apiSink.add(ApiResponse.completed(response));
     }catch(e){
       _apiSink.add(ApiResponse.error(e.toString()));
     }
    }
   getApiConfigs()async{
    _apiConfigSink.add(ApiResponse.loading(""));
    try{
      var response= await _appReo.fetchApiConfigList();
      _apiConfigSink.add(ApiResponse.completed(response));
    }catch(e){
      _apiConfigSink.add(ApiResponse.error(e.toString()));
    }
   }

}