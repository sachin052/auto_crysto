import 'dart:async';
import 'dart:convert';

import 'package:auto_crysto/ui/data/models/LoginModel.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefHelper {
  static SharedPreferences prefs;

  static initSharedPref() async {
    prefs = await SharedPreferences.getInstance();
  }

  static Future<UserDetail> getUserData() async {
    await initSharedPref();
    if(prefs.containsKey("user")){
      return UserDetail.fromJson(jsonDecode(prefs.get("user")));
    }
    else{
      return null;
    }
  }
  static Future clearData()async{
    await initSharedPref();
        prefs.remove("user");
        prefs.remove("token");
}
  static Future<AccessToken> getUserToken() async {
    await initSharedPref();
    if(prefs.containsKey("token")){
      return AccessToken.fromJson(jsonDecode(prefs.get("token")));
    }
    else{
      return null;
    }
  }
  static saveUpdatedToken(AccessToken token)async{
    if(prefs.containsKey("token")){
      prefs.remove("token");
      await prefs.setString("token", jsonEncode(token));
    }
  }
  static bool isAlreadyLoggedIn(String email){
     return prefs.containsKey("app");
  }
  static ifFirstTimeUser(String email)async{
    await prefs.setBool("app", true);
  }
}
