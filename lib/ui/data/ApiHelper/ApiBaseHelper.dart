import 'dart:collection';
import 'dart:convert';

import 'package:auto_crysto/ui/data/models/CommonApiResponse.dart';
import 'package:auto_crysto/ui/data/models/LoginModel.dart';
import 'package:auto_crysto/ui/data/sharedPrefHelper/SharedPrefHelper.dart';
import 'package:dio/dio.dart';

class ApiBaseHelper {
  //Live
  static const String baseUrl = "https://autocrysto.trade/api";

  //Dev
//   static const String baseUrl = "https://autocrysto.cratos.co.in/api";
  var dio = Dio();
  var refreshDio = Dio();
  var interceptor;
  // In milliseconds
  int connectionTimeOut=20000;
  var currentPath;
  ApiBaseHelper() {
    dio.options.baseUrl = baseUrl;
    dio.options.connectTimeout=connectionTimeOut;
    dio.options.sendTimeout=connectionTimeOut;
    dio.options.receiveTimeout=connectionTimeOut;
    refreshDio.options.baseUrl = baseUrl;
    dio.options.headers = requestHeaders;
    refreshDio.options.headers = requestHeaders;
    interceptor = InterceptorsWrapper(onRequest: (request) {
      print("request is changed:${request.data}");
    }, onResponse: (response) async {
      print("response :${response.data}");
    }, onError: (DioError error) async {
      print(error.error);
      if (error.response?.statusCode == 401) {
        dio.lock();

        dio.interceptors.responseLock.lock();
        dio.interceptors.errorLock.lock();
        var token = await SharedPrefHelper.getUserToken();
        Map<String, String> body = {"refresh_token": token.refreshToken};

//          var response=await post("/auth/refresh_token/", body);
        try {
          var response = await refreshDio.post(
            "/auth/refresh_token/",
            data: FormData.from(body),
          );
          if (response.statusCode == 200) {
            // Save the updated token in local
            var updatedToken = AccessToken.fromJson(response.data["data"][0]);
            requestHeaders["Authorization"] =
                "Bearer ${updatedToken.accessToken}";
            await SharedPrefHelper.saveUpdatedToken(updatedToken);
            dio.unlock();
            dio.interceptors.responseLock.unlock();
            dio.interceptors.errorLock.unlock();
            //Resolve the request
            // repeat the request with a new options
            return dio.request(error.request.path,
                options: Options(headers: requestHeaders));
          } else {
            //Redirect to login Screen;
            return error.error;
          }
        } catch (e) {
          print(e);
//          return error;

        }
        return error.error;
      } else {
        return error.error;
      }
    });
    dio.interceptors.add(interceptor);
//    refreshDio.interceptors.add(inteceptor);
  }

  Map<String, String> requestHeaders = {
    'Accept': 'application/json',
  };

  Future<dynamic> get(String url) async {
    var responseJson;
    try {
      var userToken = await SharedPrefHelper.getUserToken();
      if (userToken != null) {
        requestHeaders["Authorization"] = "Bearer ${userToken.accessToken}";
//        requestHeaders["Authorization"]="Bearer QyDHiGVeXf6wIjBIKzpJv5ssXO19BP";
      }
      final response = await dio.get(baseUrl + url);
      responseJson = _returnResponse(response);
    } catch (e) {
      print("THROW${e.toString()}");
      throw AppException(handleError(e,currentPath), "");
    }
//    } on SocketException {
//      throw FetchDataException('No Internet connection');
//    }
    return responseJson;
  }

  Future<dynamic> post(String url, Map<String, dynamic> body) async {
    var responseJson;
    try {
      var userToken = await SharedPrefHelper.getUserToken();
      if (userToken != null) {
        requestHeaders["Authorization"] =
            "Bearer ${userToken.accessToken}".trim();
//        requestHeaders["Authorization"]="Bearer QyDHiGVeXf6wIjBIKzpJv5ssXO19BP";
//        requestHeaders["Authorization"]="Bearer ${userToken.accessToken}".trim();
      }
      print(requestHeaders);
      if(url=="/auth/signup"){
        currentPath="/auth/signup";
      }
      final response = await dio.post(baseUrl + url, data: FormData.from(body));
      print(response.statusCode);
      print(response.data);
      responseJson = _returnResponse(response);
    } catch (e) {
      throw AppException(handleError(e,currentPath), "");
    }
    return responseJson;
  }

  Future<dynamic> put(String url, Map<String, dynamic> body) async {
    var responseJson;
    try {
      var userToken = await SharedPrefHelper.getUserToken();
      if (userToken != null) {
        requestHeaders["Authorization"] =
            "Bearer ${userToken.accessToken}".trim();
      }
      print(requestHeaders);
      final response = await dio.put(baseUrl + url, data: FormData.from(body));
      print(response.extra);
      responseJson = _returnResponse(response);
    } catch (e) {
      throw AppException(handleError(e,currentPath), "");
    }
    return responseJson;
  }

  dynamic _returnResponse(Response response) {
    switch (response.statusCode) {
      case 200:
//        var responseJson = json.decode(response.data);
//        print(responseJson);
        return response.data;
      case 400:
        throw BadRequestException(response.data.toString());
      case 401:
        throw UnauthorizedException("Session Expired please login again");
      case 403:
        throw UnauthorizedException(response.data.toString());
      case 500:
        String messgae;
        var value = CommonApiResponse.fromJson(jsonDecode(response.data));
        try {
          if (value.error["email"][0] != null) {
            messgae = value.error["email"][0];
          }
        } catch (ex) {
          messgae = value.message;
        }
        throw BadRequestException(messgae);
      default:
        throw FetchDataException(
            'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
    }
  }
}

class AppException implements Exception {
  final _message;
  final _prefix;

  AppException([this._message, this._prefix]);

  String toString() {
    return "$_prefix$_message";
  }
}

class FetchDataException extends AppException {
  FetchDataException([String message])
      : super(message, "Error During Communication: ");
}

class BadRequestException extends AppException {
  BadRequestException([message]) : super(message, "Invalid Request: ");
}

class UnauthorizedException extends AppException {
  UnauthorizedException([message]) : super(message, "Unauthorized:\n \n ");
}

class InvalidInputException extends AppException {
  InvalidInputException([String message]) : super(message, "Invalid Input: ");
}

String handleError(DioError error,[String path]) {
  String errorDescription = "";
  if (error is DioError) {
    switch (error.type) {
      case DioErrorType.CANCEL:
        errorDescription = "Reerror.typequest to API server was cancelled";
        break;
      case DioErrorType.CONNECT_TIMEOUT:
        errorDescription = "Something went wrong";
        break;
      case DioErrorType.DEFAULT:
        errorDescription =
            "Connection to API server failed due to internet connection";
        break;
      case DioErrorType.RECEIVE_TIMEOUT:
        errorDescription = "Something went wrong";
        break;
      case DioErrorType.RESPONSE:
        if(error.response.statusCode==500){
         try{
           if(path!=null&&path=="/auth/signup"){
             return CommonApiResponse.fromJson(error.response.data).error["email"][0];
           }
           var response=CommonApiResponse.fromJson(error.response.data);
           errorDescription=response.message;
         }catch(e){
           errorDescription=e.toString();
         }
        }
       else{
          errorDescription =
          "Received invalid status code: ${error.response.statusCode}";
        }
        break;
      case DioErrorType.SEND_TIMEOUT:
        // TODO: Handle this case.
        errorDescription = "Something went wrong";
        break;
    }
  } else {
    errorDescription = "Unexpected error occured";
  }
  return errorDescription;
}
