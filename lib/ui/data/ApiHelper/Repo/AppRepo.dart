import 'dart:convert';
import 'dart:io';

import 'package:auto_crysto/ui/data/models/ApiConfigList.dart';
import 'package:auto_crysto/ui/data/models/Coin.dart';
import 'package:auto_crysto/ui/data/models/CommonApiResponse.dart';
import 'package:auto_crysto/ui/data/models/ExchangeModel.dart';
import 'package:auto_crysto/ui/data/models/LoginModel.dart';
import 'package:auto_crysto/ui/data/models/States.dart';
import 'package:auto_crysto/ui/data/models/UserDetails.dart';
import 'package:auto_crysto/ui/data/models/UserPlans.dart';
import 'package:auto_crysto/ui/data/sharedPrefHelper/SharedPrefHelper.dart';
import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../ApiBaseHelper.dart';

class AppRepo {
  var apiHelper = ApiBaseHelper();

  Future<List<States>> fetchCountryList() async {
    final response = await apiHelper.get("/location/countries/");
    return StateResponse.fromJson(response).results;
  }

  Future<List<States>> fetchStateList(String countryID) async {
    final response =
        await apiHelper.get("/location/states/?country_id=$countryID");
    return StateResponse.fromJson(response).results;
  }

  Future<List<States>> fetchCityList(String stateID) async {
    final response = await apiHelper.get("/location/cities/?state_id=$stateID");
    return StateResponse.fromJson(response).results;
  }

  Future<ApiConfigList> fetchApiConfigList() async {
    final response = await apiHelper.get("/exchange/user_exchanges/");
    return ApiConfigList.fromJson(response);
  }

  Future<Map<String, dynamic>> signUp(Map<String, String> body) async {
    final response = await apiHelper.post("/auth/signup", body);
    return response;
  }

  Future<LoginModel> login(Map<String, String> body) async {
    final response = await apiHelper.post("/auth/login/", body);
    var json = JsonCodec();
    // Saving user's data locally
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var model = LoginModel.fromJson(response);
    prefs.setString("user", jsonEncode(model.userDetails));
    prefs.setString("token", jsonEncode(model.accessToken));
    prefs.setString("purchase", jsonEncode(model.purchaseDetails));
    return model;
  }

  Future<LoginModel> loginWithFB(Map<String, dynamic> body) async {
    final response = await apiHelper.post("/auth/facebook/", body);
    var json = JsonCodec();
    // Saving user's data locally
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var model = LoginModel.fromJson(response);
    prefs.setString("user", jsonEncode(model.userDetails));
    prefs.setString("token", jsonEncode(model.accessToken));
    prefs.setString("purchase", jsonEncode(model.purchaseDetails));
    return model;
  }

  Future<LoginModel> loginWithGoogle(Map<String, dynamic> body) async {
    final response = await apiHelper.post("/auth/google/", body);
    var json = JsonCodec();
    // Saving user's data locally
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var model = LoginModel.fromJson(response);
    prefs.setString("user", jsonEncode(model.userDetails));
    prefs.setString("token", jsonEncode(model.accessToken));
    prefs.setString("purchase", jsonEncode(model.purchaseDetails));
    return model;
  }

  Future<List<ExchangeModel>> getExchangeList() async {
    var response = await apiHelper.get("/exchange/exchanges");
    return ExchangeModelResponse.fromJson(response).exchangeList;
  }

  Future<CommonApiResponse> postExchangeApiKeys(
      Map<String, String> body) async {
    var response = await apiHelper.post("/exchange/user_exchanges/", body);

    return CommonApiResponse.fromJson(response);
  }

  Future<CommonApiResponse> updateExchangeApiKeys(
      String userExchangeID, Map<String, String> body) async {
    var response =
        await apiHelper.put("/exchange/user_exchanges/$userExchangeID/", body);

    return CommonApiResponse.fromJson(response);
  }

  Future<List<Coins>> getCoinsList() async {
    var response = await apiHelper.get("/exchange/coins/");
    return CoinResponse.fromJson(response).coinList;
  }

  Future<dynamic> getUserProfile() async {
    var response = await apiHelper.get("/auth/profile/details/");
    return BaseResponse.fromJson(response);
  }

  Future<UserPlans> getUserTradePlans() async {
    var response = await apiHelper.get("/exchange/user_trade_plans/");
    return UserPlans.fromJson(response);
  }

  // Sending form data to server  of LOW/HIGH/MEDIUM Risk form
  Future<CommonApiResponse> postFormData(Map<String, String> body) async {
    var response = await apiHelper.post("/exchange/user_trade_plans/", body);
    return CommonApiResponse.fromJson(response);
  }

  Future<CommonApiResponse> updateFormData(
      String planID, Map<String, String> body) async {
    var response =
        await apiHelper.put("/exchange/user_trade_plans/$planID/", body);
    print("body is $body");
    return CommonApiResponse.fromJson(response);
  }

  Future<CommonApiResponse> activeIAPTrial() async {
    var response = await apiHelper.get("/iap/trial/");
    return CommonApiResponse.fromJson(response);
  }

  Future<CommonApiResponse> updateProfile(Map<String, String> body) async {
    var response = await apiHelper.put("/auth/profile/update/", body);
    return CommonApiResponse.fromJson(response);
  }

  Future<CommonApiResponse> updatePassword(
      String userID, Map<String, String> body) async {
    var response = await apiHelper.put("/auth/update_password/", body);
    return CommonApiResponse.fromJson(response);
  }

  //Logout user from app
  Future<CommonApiResponse> logOutUser() async {
    var response = await apiHelper.get("/auth/logout/");
    return CommonApiResponse.fromJson(response);
  }

  Future<dynamic> uploadImage(File localImage) async {
    var dio = Dio();
    var token = await SharedPrefHelper.getUserToken();
    dio.options.baseUrl = ApiBaseHelper.baseUrl;

    var image = UploadFileInfo(localImage, localImage.path);
    FormData formData = FormData.from({
      "image": image,
    });
    var response = await dio.post("/auth/profile/update_pic/", data: formData,
        onSendProgress: (received, total) {
      if (total != -1) {
        print((received / total * 100).toStringAsFixed(0) + "%");
      }
    },
//        options: Options(
//          headers: {
//            'Accept': 'application/json',
//            "Content-Type": "application/x-www-form-urlencoded"
//          },
//        )
        );
    return CommonApiResponse.fromJson(response.data).data[0]["name"];
  }

  Future<CommonApiResponse>forgetPassword(String emailID) async{
    var response = await apiHelper.post("/auth/forget_password/",({
      "email":emailID}));
    return CommonApiResponse.fromJson(response);
  }
}
