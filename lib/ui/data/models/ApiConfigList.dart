
import 'dart:convert';

ApiConfigList apiConfigFromJson(String str) => ApiConfigList.fromJson(json.decode(str));

String apiConfigToJson(ApiConfigList data) => json.encode(data.toJson());

class ApiConfigList {
  bool valid;
  int code;
  String message;
  List<ApiConfigModel> exchangeList;

  ApiConfigList({
    this.valid,
    this.code,
    this.message,
    this.exchangeList,
  });

  factory ApiConfigList.fromJson(Map<String, dynamic> json) => new ApiConfigList(
    valid: json["valid"],
    code: json["code"],
    message: json["message"],
    exchangeList: new List<ApiConfigModel>.from(json["data"].map((x) => ApiConfigModel.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "valid": valid,
    "code": code,
    "message": message,
    "data": new List<dynamic>.from(exchangeList.map((x) => x.toJson())),
  };
}

class ApiConfigModel {
  int id;
  String apiKey;
  String apiSecret;
  int user;
  int exchange;

  ApiConfigModel({
    this.id,
    this.apiKey,
    this.apiSecret,
    this.user,
    this.exchange,
  });

  factory ApiConfigModel.fromJson(Map<String, dynamic> json) => new ApiConfigModel(
    id: json["id"],
    apiKey: json["api_key"],
    apiSecret: json["api_secret"],
    user: json["user"],
    exchange: json["exchange"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "api_key": apiKey,
    "api_secret": apiSecret,
    "user": user,
    "exchange": exchange,
  };
}
