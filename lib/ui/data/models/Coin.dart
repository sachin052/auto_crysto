class Coins{
  String id;
  String name;
  String pair;
  String price;

  Coins.fromJson(Map<String,dynamic> json){
    id=json["id"].toString();
        name=json["name"];
        pair=json["pair"];
        price=json["price"].toString();
  }

}

class CoinResponse{
  List<Coins> coinList;
  CoinResponse.fromJson(Map<String,dynamic>json){
    if(json!=null&&json["data"]!=null){
      coinList=List<Coins>();
      json["data"].forEach((v){
        coinList.add(Coins.fromJson(v));
      });
    }
  }
}