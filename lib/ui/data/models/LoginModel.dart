
import 'package:codable/codable.dart';

class LoginModel{
	UserDetail userDetails;
	AccessToken accessToken;
	PurchaseDetails purchaseDetails;
	LoginModel.fromJson(Map<String ,dynamic> json){
		userDetails=UserDetail.fromJson(json["data"][0]["user_details"]);
		accessToken=AccessToken.fromJson(json["data"][0]["access_token"]);
		purchaseDetails=PurchaseDetails.fromJson(json['data'][0]["purchase_detalis"]);
	}
}

class UserDetail {
	String id;
	String firstName;
	String lastName;
	String email;
	String profilePic;
	UserDetail.fromJson(Map<String ,dynamic> json){
		id=json["id"].toString();
		firstName=json["first_name"];
		lastName=json["last_name"];
		email=json["email"];
		profilePic=json["profile_pic"];
	}
	Map<String, dynamic> toJson() => {
		'id' : id.toString(),
		'first_name' : firstName,
		'last_name' : lastName,
		'email' : email,
		'profile_pic' : profilePic,
	};

  @override
  void encode(KeyedArchive object) {
    object.encode("userDetails",this);
  }
}
class AccessToken{
	String accessToken;
	int expiresIn;
	String tokenType;
	String scope;
	String refreshToken;
	AccessToken.fromJson(Map<String,dynamic> json){
		accessToken=json["access_token"];
		expiresIn=json["expires_in"];
		tokenType=json["token_type"];
		scope=json["scope"];
		refreshToken=json["refresh_token"];
	}
	Map<String, dynamic> toJson() => {
		'access_token' : accessToken,
		'expires_in' : expiresIn,
		'token_type' : tokenType,
		'scope' : scope,
		'refresh_token' : refreshToken,
	};

}
class PurchaseDetails{
	String planType;
	String packageId;
	String expiryDate;
	String expireInmSec;
	String expiryWithoutTz;
	bool isExpired;
	bool autoRenewing;

	PurchaseDetails.fromJson(Map<String,dynamic> json){
		planType=json["plan_type"];
		packageId=json["package_id"];
		expiryDate=json["expire_date"];
		expireInmSec=json["expire_date_milis"].toString();
		isExpired=json["is_expired"];
		autoRenewing=json["auto_renewing"];
	}
	Map<String, dynamic> toJson() => {
		'plan_type' : planType,
		'package_id' : packageId,
		'expire_date' : expiryDate,
		'expire_date_milis' : expireInmSec,
		'is_expired' : isExpired,
		'auto_renewing':autoRenewing
	};

}
