class CommonApiResponse{
  bool valid;
  int code;
  String message;
  dynamic data;
  dynamic error;
  CommonApiResponse.fromJson(Map<String,dynamic> json){
    valid=json["valid"];
    code=json["code"];
    message=json["message"];
    data=json["data"];
    error=json["error"];
  }
}