class ExchangeModel{
  int id;
  String title;
  bool isActive;

  ExchangeModel.fromJson(Map<String,dynamic> json){
    id=json["id"];
    title=json["title"];
    isActive=json["is_active"];
  }
}
class ExchangeModelResponse{
  List<ExchangeModel> exchangeList;
  ExchangeModelResponse.fromJson(Map<String,dynamic> json){
    if(json!=null&&json["data"]!=null){
      exchangeList=List<ExchangeModel>();
      json["data"].forEach((value){
        exchangeList.add(ExchangeModel.fromJson(value));
      });
    }
  }
}