class FormData{
   String coin;
   String exchange;
   String plan_type;
   String trading_amount;
   String buy_time_scale;
   String amount_per_buy;
   String number_of_position;
   String buy_threshold;
   String sell_threshold;
   String is_default;
   Map<String, String> toMap() {
     return {
       'coin': coin,
       'exchange':exchange,
       'plan_type': plan_type,
       'trading_amount': trading_amount,
       'buy_time_scale': buy_time_scale,
       'amount_per_buy': amount_per_buy,
       'number_of_position': number_of_position,
       'buy_threshold': buy_threshold,
       'sell_threshold': sell_threshold,
       'is_default': is_default,
     };
   }
   Map<String, String> toLowMap() {
     return {
       'coin': coin,
       'exchange':exchange,
       'plan_type': plan_type,
       'trading_amount': trading_amount,
       'amount_per_buy': amount_per_buy,
       'number_of_position': number_of_position,
       'buy_threshold': buy_threshold,
       'is_default': is_default,
     };
   }
}