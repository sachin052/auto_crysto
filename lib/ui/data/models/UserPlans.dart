// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);



class UserPlans {
  bool valid;
  int code;
  String message;
  List<Plan> plansList;

  UserPlans({
    this.valid,
    this.code,
    this.message,
    this.plansList,
  });

  factory UserPlans.fromJson(Map<String, dynamic> json) => new UserPlans(
    valid: json["valid"],
    code: json["code"],
    message: json["message"],
    plansList: new List<Plan>.from(json["data"].map((x) => Plan.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "valid": valid,
    "code": code,
    "message": message,
    "data": new List<dynamic>.from(plansList.map((x) => x.toJson())),
  };
}

class Plan {
  int id;
  User user;
  Coin coin;
  int exchange;
  String planType;
  int tradingAmount;
  int buyTimeScale;
  String amountPerBuy;
  int numberOfPosition;
  String buyThreshold;
  String sellThreshold;
  bool isDefault=false;
  DateTime createdAt;

  Plan({
    this.id,
    this.user,
    this.coin,
    this.exchange,
    this.planType,
    this.tradingAmount,
    this.buyTimeScale,
    this.amountPerBuy,
    this.numberOfPosition,
    this.buyThreshold,
    this.sellThreshold,
    this.isDefault,
    this.createdAt,
  });

  factory Plan.fromJson(Map<String, dynamic> json) => new Plan(
    id: json["id"],
    user: User.fromJson(json["user"]),
    coin: Coin.fromJson(json["coin"]),
    exchange: json["exchange"],
    planType: json["plan_type"],
    tradingAmount: json["trading_amount"],
    buyTimeScale: json["buy_time_scale"],
    amountPerBuy: json["amount_per_buy"],
    numberOfPosition: json["number_of_position"],
    buyThreshold: json["buy_threshold"],
    sellThreshold: json["sell_threshold"],
    isDefault: json["is_default"],
    createdAt: DateTime.parse(json["created_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "user": user.toJson(),
    "coin": coin.toJson(),
    "exchange": exchange,
    "plan_type": planType,
    "trading_amount": tradingAmount,
    "buy_time_scale": buyTimeScale,
    "amount_per_buy": amountPerBuy,
    "number_of_position": numberOfPosition,
    "buy_threshold": buyThreshold,
    "sell_threshold": sellThreshold,
    "is_default": isDefault,
    "created_at": createdAt.toIso8601String(),
  };
}

class Coin {
  int id;
  String name;
  String pair;
  double price;

  Coin({
    this.id,
    this.name,
    this.pair,
    this.price,
  });

  factory Coin.fromJson(Map<String, dynamic> json) => new Coin(
    id: json["id"],
    name: json["name"],
    pair: json["pair"],
    price: json["price"].toDouble(),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "pair": pair,
    "price": price,
  };
}

class User {
  int id;
  String firstName;
  String lastName;
  String email;
  String profilePic;
  City city;
  bool isFacebook;
  bool isGoogle;
  bool isVerified;
  bool isDirect;
  String profilePicUrl;

  User({
    this.id,
    this.firstName,
    this.lastName,
    this.email,
    this.profilePic,
    this.city,
    this.isFacebook,
    this.isGoogle,
    this.isVerified,
    this.isDirect,
    this.profilePicUrl,
  });

  factory User.fromJson(Map<String, dynamic> json) => new User(
    id: json["id"],
    firstName: json["first_name"],
    lastName: json["last_name"],
    email: json["email"],
    profilePic: json["profile_pic"],
    city: City.fromJson(json["city"]),
    isFacebook: json["is_facebook"],
    isGoogle: json["is_google"],
    isVerified: json["is_verified"],
    isDirect: json["is_direct"],
    profilePicUrl: json["profile_pic_url"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "first_name": firstName,
    "last_name": lastName,
    "email": email,
    "profile_pic": profilePic,
    "city": city.toJson(),
    "is_facebook": isFacebook,
    "is_google": isGoogle,
    "is_verified": isVerified,
    "is_direct": isDirect,
    "profile_pic_url": profilePicUrl,
  };
}

class City {
  int id;
  String title;
  UserState state;

  City({
    this.id,
    this.title,
    this.state,
  });

  factory City.fromJson(Map<String, dynamic> json) => new City(
    id: json["id"],
    title: json["title"],
    state: UserState.fromJson(json["state"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "title": title,
    "state": state.toJson(),
  };
}

class UserState {
  int id;
  String title;
  Country country;

  UserState({
    this.id,
    this.title,
    this.country,
  });

  factory UserState.fromJson(Map<String, dynamic> json) => new UserState(
    id: json["id"],
    title: json["title"],
    country: Country.fromJson(json["country"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "title": title,
    "country": country.toJson(),
  };
}

class Country {
  int id;
  String title;

  Country({
    this.id,
    this.title,
  });

  factory Country.fromJson(Map<String, dynamic> json) => new Country(
    id: json["id"],
    title: json["title"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "title": title,
  };
}
