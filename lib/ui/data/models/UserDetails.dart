// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

BaseResponse welcomeFromJson(String str) => BaseResponse.fromJson(json.decode(str));

String welcomeToJson(BaseResponse data) => json.encode(data.toJson());

class BaseResponse {
  bool valid;
  int code;
  String message;
  List<UserDetails> data;

  BaseResponse({
    this.valid,
    this.code,
    this.message,
    this.data,
  });

  factory BaseResponse.fromJson(Map<String, dynamic> json) => new BaseResponse(
    valid: json["valid"],
    code: json["code"],
    message: json["message"],
    data: new List<UserDetails>.from(json["data"].map((x) => UserDetails.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "valid": valid,
    "code": code,
    "message": message,
    "data": new List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class UserDetails {
  int id;
  String firstName;
  String lastName;
  String email;
  String profilePic;
  City city;
  bool isFacebook;
  bool isGoogle;
  bool isVerified;
  bool isDirect;
  String profilePicUrl;

  UserDetails({
    this.id,
    this.firstName,
    this.lastName,
    this.email,
    this.profilePic,
    this.city,
    this.isFacebook,
    this.isGoogle,
    this.isVerified,
    this.isDirect,
    this.profilePicUrl,
  });

  factory UserDetails.fromJson(Map<String, dynamic> json) => new UserDetails(
    id: json["id"],
    firstName: json["first_name"],
    lastName: json["last_name"],
    email: json["email"],
    profilePic: json["profile_pic"],
    city: City.fromJson(json["city"]),
    isFacebook: json["is_facebook"],
    isGoogle: json["is_google"],
    isVerified: json["is_verified"],
    isDirect: json["is_direct"],
    profilePicUrl: json["profile_pic_url"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "first_name": firstName,
    "last_name": lastName,
    "email": email,
    "profile_pic": profilePic,
    "city": city.toJson(),
    "is_facebook": isFacebook,
    "is_google": isGoogle,
    "is_verified": isVerified,
    "is_direct": isDirect,
    "profile_pic_url": profilePicUrl,
  };
}

class City {
  int id;
  String title;
  State state;

  City({
    this.id,
    this.title,
    this.state,
  });

  factory City.fromJson(Map<String, dynamic> json) => new City(
    id: json["id"],
    title: json["title"],
    state: State.fromJson(json["state"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "title": title,
    "state": state.toJson(),
  };
}

class State {
  int id;
  String title;
  Country country;

  State({
    this.id,
    this.title,
    this.country,
  });

  factory State.fromJson(Map<String, dynamic> json) => new State(
    id: json["id"],
    title: json["title"],
    country: Country.fromJson(json["country"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "title": title,
    "country": country.toJson(),
  };
}

class Country {
  int id;
  String title;

  Country({
    this.id,
    this.title,
  });

  factory Country.fromJson(Map<String, dynamic> json) => new Country(
    id: json["id"],
    title: json["title"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "title": title,
  };
}
