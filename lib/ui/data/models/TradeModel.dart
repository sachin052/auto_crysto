// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

List<TradeModel> tradeHistoryFromJson(String str) => List<TradeModel>.from(json.decode(str).map((x) => TradeModel.fromJson(x)));

String tradeHistoryToJson(List<TradeModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class TradeModel {
  String symbol;
  int orderId;
  int orderListId;
  String clientOrderId;
  String price;
  String origQty;
  String executedQty;
  String cummulativeQuoteQty;
  String status;
  String timeInForce;
  String type;
  String side;
  String stopPrice;
  String icebergQty;
  int time;
  int updateTime;
  bool isWorking;

  TradeModel({
    this.symbol,
    this.orderId,
    this.orderListId,
    this.clientOrderId,
    this.price,
    this.origQty,
    this.executedQty,
    this.cummulativeQuoteQty,
    this.status,
    this.timeInForce,
    this.type,
    this.side,
    this.stopPrice,
    this.icebergQty,
    this.time,
    this.updateTime,
    this.isWorking,
  });

  factory TradeModel.fromJson(Map<String, dynamic> json) => TradeModel(
    symbol: json["symbol"],
    orderId: json["orderId"],
    orderListId: json["orderListId"],
    clientOrderId: json["clientOrderId"],
    price: json["price"],
    origQty: json["origQty"],
    executedQty: json["executedQty"],
    cummulativeQuoteQty: json["cummulativeQuoteQty"],
    status: json["status"],
    timeInForce: json["timeInForce"],
    type: json["type"],
    side: json["side"],
    stopPrice: json["stopPrice"],
    icebergQty: json["icebergQty"],
    time: json["time"],
    updateTime: json["updateTime"],
    isWorking: json["isWorking"],
  );

  Map<String, dynamic> toJson() => {
    "symbol": symbol,
    "orderId": orderId,
    "orderListId": orderListId,
    "clientOrderId": clientOrderId,
    "price": price,
    "origQty": origQty,
    "executedQty": executedQty,
    "cummulativeQuoteQty": cummulativeQuoteQty,
    "status": status,
    "timeInForce": timeInForce,
    "type": type,
    "side": side,
    "stopPrice": stopPrice,
    "icebergQty": icebergQty,
    "time": time,
    "updateTime": updateTime,
    "isWorking": isWorking,
  };
}
