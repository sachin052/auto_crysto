class States {
  int id;
  String title;

  States.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];

  }
}
class StateResponse {
  List<States> results;
  StateResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      results = new List<States>();
      json['data'].forEach((v) {
        results.add(States.fromJson(v));
      });
    }
  }
}