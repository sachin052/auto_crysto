import 'dart:async';
import 'package:auto_crysto/routes/Router.dart';
import 'package:auto_crysto/ui/apiresponseui/Loading.dart';
import 'package:auto_crysto/ui/data/ApiHelper/ApiBaseHelper.dart';
import 'package:auto_crysto/ui/data/ApiHelper/ApiResponse.dart';
import 'package:auto_crysto/ui/data/ApiHelper/Repo/AppRepo.dart';
import 'package:auto_crysto/ui/data/models/CommonApiResponse.dart';
import 'package:auto_crysto/ui/data/models/UserDetails.dart';
import 'package:auto_crysto/ui/data/models/UserPlans.dart';
import 'package:auto_crysto/ui/data/sharedPrefHelper/SharedPrefHelper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rxdart/rxdart.dart';
class HomBloc{
  AppRepo appRepo;
  StreamController homeController;
  StreamSink<ApiResponse<BaseResponse>> get sink =>homeController.sink;
  StreamSink<String> get refreshSink=>refreshData.sink;
  Stream<ApiResponse<BaseResponse>> get stream =>homeController.stream;
  final BehaviorSubject<CommonApiResponse> _logout = BehaviorSubject<CommonApiResponse>();
  BehaviorSubject<CommonApiResponse> get logoutRequest => _logout;

  StreamController userPlansController;
  StreamSink<ApiResponse<UserPlans>> get userPlansSink =>userPlansController.sink;
  Stream<ApiResponse<UserPlans>> get userPlansStream =>userPlansController.stream;
  Stream<String> get refreshStream=>refreshData.stream;
  StreamController refreshData;

  HomBloc(){
    appRepo=AppRepo();
    refreshData=StreamController<String>.broadcast();
    homeController=StreamController<ApiResponse<BaseResponse>>();
    userPlansController=StreamController<ApiResponse<UserPlans>>.broadcast();

  }

  Future<dynamic> getUserProfile()async{
    sink.add(ApiResponse.loading(("Fetching User Profile")));
    try{
       var response= await appRepo.getUserProfile();
       sink.add(ApiResponse.completed((response)));
      Future.delayed(Duration(seconds: 1),()async{
        await getUserPlans();
      });

    }catch(e){
      if(e is UnauthorizedException){
        await SharedPrefHelper.clearData();
      }
      sink.add(ApiResponse.error(e.toString()));
    }
  }

  Future<dynamic> getUserPlans()async{
    userPlansSink.add(ApiResponse.loading(("Fetching User Profile")));
    try{
      var response= await appRepo.getUserTradePlans();
      userPlansSink.add(ApiResponse.completed((response)));

    }catch(e){
      userPlansSink.add(ApiResponse.error(e.toString()));
    }
  }
  logoutUser(BuildContext context)async{
    showDialog(context: context,builder: (_)=>onLoading(colors: Colors.black12));
    try{
      var response=await appRepo.logOutUser();
      if(response.code==200){
        Navigator.of(context,rootNavigator: true).pop();
        Navigator.of(context,rootNavigator: true).pop();
        await SharedPrefHelper.clearData();
        Navigator.of(context).pushNamedAndRemoveUntil(Router.loginSignUp, (Route<dynamic> route) => false);
      }
    }catch(e){
      Navigator.of(context,rootNavigator: true).pop();
    Fluttertoast.showToast(msg: e.toString());
    }
  }
  void close(){
    homeController.close();
    userPlansController.close();
    refreshData.close();
    _logout.close();
  }
}