import 'dart:async';
import 'dart:io';
import 'package:auto_crysto/ui/aboutus/AboutUs.dart';
import 'package:auto_crysto/ui/apiConfig/ApiConfigDialog.dart';
import 'package:auto_crysto/ui/apiresponseui/OnError.dart';
import 'package:auto_crysto/ui/changePassword/ChangePassword.dart';
import 'package:auto_crysto/ui/data/ApiHelper/ApiResponse.dart';
import 'package:auto_crysto/ui/data/bloc/ApiConfigBloc.dart';
import 'package:auto_crysto/ui/data/models/ApiConfigList.dart';
import 'package:auto_crysto/ui/data/models/LoginModel.dart';
import 'package:auto_crysto/ui/data/models/UserDetails.dart' as prefix0;
import 'package:auto_crysto/ui/data/models/UserPlans.dart';
import 'package:auto_crysto/ui/data/sharedPrefHelper/SharedPrefHelper.dart';
import 'package:auto_crysto/ui/forms/FormBloc.dart';
import 'package:auto_crysto/ui/forms/HighRiskForm.dart';
import 'package:auto_crysto/ui/home/HomeBloc.dart';
import 'package:auto_crysto/ui/login_signup/LoginSignUp.dart';
import 'package:auto_crysto/ui/profile/Profile.dart';
import 'package:auto_crysto/ui/tradeHistory/TradeHistory.dart';
import 'package:auto_crysto/ui/utils/AppColors.dart';
import 'package:auto_crysto/ui/utils/Images.dart';
import 'package:auto_crysto/ui/utils/Styles.dart';
import 'package:auto_crysto/ui/utils/Utils.dart';
import 'package:auto_crysto/ui/walk_through/WalkThrough.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';

List<String> menuList = [
  "Trading",
  "Trading History",
  "Trading Instructions",
  "Api Keys Settings",
  "Change Password",
  "Support",
  "About Us",
  "Share Auto Crysto",
  "Rate US"
];
List<Plan> userPlans;

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home>
    with TickerProviderStateMixin{
  bool isCollapsed = true;
  var _screenWidth;
  var _screenHeight;
  var _scaleAnimation;
  Curve curve = Curves.easeInBack;
  AnimationController _animationController;
  AnimationController _slideController;
  UserDetail userDetails;
  AnimationController _iconAnimationController;
  ApiConfigBloc _apiConfigBloc;
  int durationTime = 500;
  bool animateContainer1 = false;
  HomBloc homeBloc;
  GlobalKey<ScaffoldState> key;
  bool visibleMainContent = false;
  String selectedTitle = '';
  Plan selectedPlan;
  Animation<Offset> _animation;
  StreamBuilder streamBuilder;
  Widget currentWidget;
  prefix0.UserDetails currentUser;
  String firstName;
  String lastName;
  bool buttonTapped = false;
  ApiConfigModel currentExchange;
  bool isHigh = false;
  bool isLow = false;
  bool isMed = false;
  bool isDialogShowing = false;
  String currentPlanTitle;
  bool fromDrawer = false;
  bool loadInitial = false;


  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    currentWidget = creatHomeScreenPage();
  }

  @override
  void initState() {
    super.initState();
    homeBloc = HomBloc();
    getUserData();
    key = GlobalKey<ScaffoldState>();
    _animationController = AnimationController(
        vsync: this, duration: Duration(milliseconds: durationTime));
    _iconAnimationController = AnimationController(
        vsync: this, duration: Duration(milliseconds: durationTime));
    _scaleAnimation = Tween<double>(begin: 1.0, end: 0.5).animate(
      _iconAnimationController,
    );
    Future.delayed(Duration(milliseconds: 500), () {
      setState(() {
        animateContainer1 = true;
      });
    });
//    _apiConfigBloc = ApiConfigBloc();
    print("at line 72");

    _apiConfigBloc = ApiConfigBloc();
//    getOtherUserData();

    _listenApiConfigStream();
    _listenRefreshStream();
//    streamBuilder = createApiStreamBuilder();
    _slideController = new AnimationController(
        vsync: this, duration: new Duration(milliseconds: 1000));
    _animation = Tween<Offset>(
      begin: Offset(0.0, .5),
      end: Offset(0, 0),
    ).animate(
        CurvedAnimation(parent: _slideController, curve: Curves.elasticOut));
    _slideController.forward();
//    _listerUserStream();
  }

  getUserData() async {
    if (currentUser == null) {
      await homeBloc.getUserProfile();
//      userDetails = await SharedPrefHelper.getUserData();
    } else {
      Future.delayed(Duration(milliseconds: 10), () {
        this.setState(() {
          firstName = currentUser.firstName;
          lastName = currentUser.lastName;
        });
      });
    }
  }

//  createApiStreamBuilder() =>


  @override
  void dispose() {
    super.dispose();
    _animationController.dispose();
    _iconAnimationController.dispose();
    homeBloc.close();
  }

  @override
  Widget build(BuildContext context) {
    _screenWidth = Utils().getScreenWidth(context);
    _screenHeight = Utils().getScreenHeight(context);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: Styles.getTheme(),
      home: MediaQuery(
        data: MediaQuery.of(context).size.width>600?MediaQuery.of(context).copyWith(textScaleFactor: 1.2):MediaQuery.of(context),
        child: Scaffold(
          key: key,
          body: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              Container(
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          colors: [
                            AppColors.colorPrimary,
                            AppColors.colorPrimaryGradient
                          ],
                          begin: Alignment.centerLeft,
                          stops: [0.1, 0.99],
                          end: Alignment.centerRight))),
              sideMenu(context),
              homeScreen(context),
        StreamBuilder<ApiResponse<prefix0.BaseResponse>>(
            stream: homeBloc.stream,
            builder: (context, snapshot) {
          if (snapshot.hasData) {
            var data = snapshot.data;
            switch (data.status) {
              case Status.LOADING:
                return Container(
                  color: AppColors.colorPrimary.withOpacity(.2),
                  margin: EdgeInsets.only(bottom: 10.0),
                  child: Align(
                      alignment: Alignment.center,
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(
                            Colors.white),
                      )),
                );
                break;
              case Status.COMPLETED:
                currentUser = snapshot.data.data.data[0];
                getUserData();
                  Future.delayed(Duration(seconds: 1),(){
                    setState(() {
                      currentWidget=creatHomeScreenPage();
                    });
                  });
                return homeScreen(context);
                break;
              case Status.ERROR:
                print("ERROR CASE");
//                  if (!isDialogShowing) {
////                    isDialogShowing = true;
////
////                  } else {
////                    return Container();
////                  }
                Future.delayed(Duration(seconds: 4),(){
                 setState(() {
                   currentWidget=Column(
                     mainAxisSize: MainAxisSize.max,
                     children: <Widget>[
                       Text(
                         "What would you like to do?",
                         style: Styles.regularBodyOne(color: Colors.white),
                       ),
                       Container(
                         child: OnError(
                           color: Colors.transparent,
                           errorMessage: data.message,
                           btnText:
                           data.message.contains("Unauthorized") ? "Login" : "Retry",
                           onRetryPressed: () async {
                             isDialogShowing = false;
                             if (data.message.contains("Unauthorized")) {
                               Navigator.pushReplacement(
                                   context,
                                   CupertinoPageRoute(
                                       builder: (context) => LoginSignUP()));
                             } else {
                               print("at line 143");
                               isDialogShowing = false;
                               await homeBloc.getUserProfile();
                             }
                           },
                         ),
                         height: MediaQuery.of(context).size.height*.80,
                       ),
                     ],
                   );
                 });
                });
                return Container(
                );
                break;
              default:
                return Container();
            }
          } else {
            return Container();
          }
            }),
              StreamBuilder<ApiResponse<UserPlans>>(
                  stream: homeBloc.userPlansStream,
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      var data = snapshot.data;
                      switch (data.status) {
                        case Status.LOADING:
                          return Container(
                            color: AppColors.colorPrimary.withOpacity(.2),
                            margin: EdgeInsets.only(bottom: 10.0),
                            child: Align(
                                alignment: Alignment.center,
                                child: CircularProgressIndicator(
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                      Colors.white),
                                )),
                          );
                          break;
                        case Status.COMPLETED:
                          userPlans = snapshot.data.data.plansList;
                          getUserData();
                          return Container();
                          break;
                        case Status.ERROR:
                          if (!isDialogShowing) {
                            isDialogShowing = true;
                            return OnError(
                              errorMessage: data.message,
                              btnText: data.message.contains("Unauthorized")
                                  ? "Login"
                                  : "Retry",
                              onRetryPressed: () async {
                                if (data.message.contains("Unauthorized")) {
                                  Navigator.pushReplacement(
                                      context,
                                      CupertinoPageRoute(
                                          builder: (context) =>
                                              LoginSignUP()));
                                } else {
                                  await homeBloc.getUserProfile();
                                }
                              },
                            );
                          } else {
                            return Container();
                          }
                          break;
                        default:
                          return Container();
                      }
                    } else {
                      return Container();
                    }
                  })
            ],
          ),
        ),
      ),
    );
  }

  Widget homeScreen(BuildContext context) {
    return AnimatedPositioned(
      curve: curve,
      top: 0,
      bottom: 0,
      left: isCollapsed ? 0.0 : 0.5 * _screenWidth,
      right: isCollapsed ? 0.0 : -0.4 * _screenWidth,
      duration: Duration(milliseconds: durationTime),
      child: ScaleTransition(
        scale: _scaleAnimation,
        child: Material(
            elevation: 8.0,
            borderRadius: !isCollapsed ? BorderRadius.circular(20.0) : null,
            child: Stack(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    borderRadius: !isCollapsed
                        ? BorderRadius.circular(20.0)
                        : BorderRadius.zero,
                    gradient: LinearGradient(
                        colors: [
                          AppColors.colorPrimary,
                          AppColors.colorPrimaryGradient
                        ],
                        begin: Alignment.bottomCenter,
                        stops: [0.1, 0.6],
                        end: Alignment.topCenter),
                  ),
                ),
                CustomScrollView(
                  slivers: <Widget>[
                    SliverAppBar(
//                      title: Text(
//                        firstName != null
//                            ? "Hi " + firstName + " " + lastName
//                            : "",
//                        style: Styles.boldHeadingTwoWhite(),
//                      ),
                      title: firstName != null
                          ? RichText(
                        text: TextSpan(children: [
                          TextSpan(
                              text: 'Hi ',
                              style: Styles.regularBodyOne(
                                  color: Colors.white)),
                          TextSpan(
                              text: firstName + " " + lastName,
                              style: Styles.boldHeadingTwoWhite()),
                        ]),
                      )
                          : Text(
                        'Hi',
                        style: Styles.regularBodyOne(),
                      ),
                      centerTitle: true,
                      leading: IconButton(
                          splashColor:
                          AppColors.colorPrimaryGradient.withOpacity(.5),
                          onPressed: () {
                            animateDrawer();
                          },
                          icon: AnimatedIcon(
                            icon: AnimatedIcons.menu_arrow,
                            progress: _iconAnimationController,
                            color: Colors.white,
                          )),
                      backgroundColor: Colors.transparent,
                      expandedHeight: 10.0,
                    ),
                    SliverList(
                      delegate: SliverChildListDelegate(
                        [currentWidget],
                      ),
                    ),
                  ],
                ),
                Visibility(
                  child: InkWell(
                    onTap: () {
                      animateDrawer();
                    },
                    child: Container(),
                  ),
                  visible: !isCollapsed ? true : false,
                )
              ],
            )),
      ),
    );
  }

  Widget buildRiskTypeCard(String title, String img, bool fakeCheck) {
    return Padding(
      padding:
      const EdgeInsets.only(left: 16.0, right: 16.0, top: 8.0, bottom: 8.0),
      child: Material(
        elevation: 8.0,
        borderRadius: BorderRadius.circular(10.0),
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30.0), color: Colors.white),
          child: Material(
            elevation: 8.0,
            color: Colors.transparent,
            type: MaterialType.transparency,
            borderRadius: BorderRadius.circular(30.0),
            child: Align(
              alignment: Alignment.center,
              child: InkWell(
                onTap: () {
                  buttonTapped = true;
                  print("Fetching");
                  selectedTitle = title;
                  assignSelectedPlan(title);
                  checkApiConfigStatus();
                  var formBloc = FormBloc();
                  formBloc?.planSink?.add(selectedPlan?.planType);
                },
                splashColor: AppColors.colorPrimaryGradient.withOpacity(.5),
                child: Container(
                  height: MediaQuery.of(context).size.width*.38,
                  width: Utils().getScreenWidth(context),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30.0),
                      color: Colors.transparent),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Stack(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  Visibility(
                                    visible: fakeCheck,
                                    child: Material(
                                      elevation: 10.0,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                          BorderRadius.circular(20.0)),
                                      child: CircleAvatar(
                                          radius: 15.0,
                                          backgroundColor:
                                          AppColors.colorPrimary,
                                          child: Icon(
                                            Icons.done,
                                            color: Colors.white,
                                            size: 15.0,
                                          )),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Image.asset(img, height: MediaQuery.of(context).size.width*.15, width: MediaQuery.of(context).size.width*.15),
                              ],
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Material(
                          type: MaterialType.transparency,
                          child: Text(
                            title,
                            textAlign: TextAlign.center,
                            style: Styles.boldHeadingOneBlack().copyWith(
                                fontSize: MediaQuery.of(context).textScaleFactor==1.0?19.0:28.0),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget sideMenu(BuildContext context) {
    var _image;
    return AnimatedPositioned(
      curve: curve,
      duration: Duration(milliseconds: durationTime),
      top: 0,
      bottom: 0,
      left: !isCollapsed ? 0 : -1.0 * _screenWidth,
      right: !isCollapsed ? 0 : 0.6 * _screenWidth,
      child: SingleChildScrollView(
        child: SafeArea(
          child: Container(
            padding: EdgeInsets.all(8.0),
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: [
                      AppColors.colorPrimary,
                      AppColors.colorPrimaryGradient
                    ],
                    begin: Alignment.centerLeft,
                    stops: [0.1, 0.99],
                    end: Alignment.centerRight)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 16.0, top: 16.0),
                  child: Material(
                    type: MaterialType.transparency,
                    borderRadius: BorderRadius.circular(40.0),
                    elevation: 10.0,
                    child: InkWell(
                      onTap: () {
                        setState(() {
//                        currentWidget=Profile();
//                        animateDrawer();
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (c) =>
                                      Profile(
                                        profile: currentUser,
                                        notifyPreviousWidget: homeBloc,
                                      )));
                        });
                      },
                      child: Hero(
                        tag: "image",
                        child: Material(
                          elevation: 10.0,
                          color: Colors.transparent,
                          borderRadius: BorderRadius.circular(40.0),
                          child: Container(
                            height: 90,
                            width: 90,
                            decoration: BoxDecoration(
                                border:
                                Border.all(color: Colors.white, width: 1.0),
                                borderRadius: BorderRadius.circular(45.0),
                                image: DecorationImage(
                                    image: currentUser != null
                                        ? CachedNetworkImageProvider(
                                        currentUser.profilePicUrl)
                                        : AssetImage(Images.profile),
                                    fit: BoxFit.cover)),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 16.0),
                  child: Hero(
                    tag: "name",
                    child: Material(
                      type: MaterialType.transparency,
                      child: Text(
                        currentUser != null
                            ? currentUser.firstName + " " + currentUser.lastName
                            : "",
                        style: Styles.semiBoldHeadingTwo(color: Colors.white),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 16.0),
                  child: Hero(
                    tag: "email",
                    child: Material(
                      type: MaterialType.transparency,
                      child: Text(
                        currentUser != null ? currentUser.email : "",
                        style: Styles.regularBodyTwo(color: Colors.white),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10.0),
                Column(
                  children: List<Widget>.generate(menuList.length, (index) {
                    if (currentUser != null &&
                        index == 4 &&
                        !currentUser.isDirect) {
                      return Container();
                    }
                    return Material(
                      type: MaterialType.transparency,
                      elevation: 8.0,
                      child: InkWell(
                        splashColor: Colors.white.withOpacity(.5),
                        onTap: () {
                          _onNavigationItemClicked(index);
                        },
                        child: Container(
                          height: 50.0,
                          decoration: BoxDecoration(shape: BoxShape.rectangle),
                          child: Row(
                            children: <Widget>[
                              SizedBox(
                                width: 10.0,
                              ),
                              Image.asset(
                                Images.menuImages[index],
                                height: 25.0,
                                width: 25.0,
                                color: Colors.white,
                              ),
                              SizedBox(
                                width: 10.0,
                              ),
                              Material(
                                type: MaterialType.transparency,
                                child: Text(
                                  menuList[index],
                                  style: Styles.semiBoldHeadingFour(
                                      color: Colors.white),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  }),
                  mainAxisSize: MainAxisSize.max,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 2, left: 10.0),
                  child: RaisedButton.icon(
                    onPressed: () {
                      Utils.showAnimatedDialog(
                          context,
                          AlertDialog(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0)),
                            title: Text(
                              "Logout ?",
                              style: Styles.semiBoldHeadingThree(),
                            ),
                            content: const Text('Are you sure want to logout?'),
                            actions: <Widget>[
                              FlatButton(
                                onPressed: () {
                                  Navigator.of(context,rootNavigator: true).pop();
                                },
                                child: Container(
                                  child: Center(
                                      child: Text(
                                        "No",
                                        style: Styles.semiBoldHeadingFour(
                                            color: Colors.white),
                                      )),
                                  width: 60,
                                ),
                                color: AppColors.colorPrimary,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20.0)),
                              ),
                              OutlineButton(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20.0),
                                ),
                                onPressed: () {
                                  homeBloc.logoutUser(context);
                                },
                                child: Container(
                                  child: Center(
                                      child: Text(
                                        "Yes",
                                        style: Styles.semiBoldHeadingFour(
                                            color: AppColors.colorPrimary),
                                      )),
                                  width: 60,
                                ),
                              )
                            ],
                          ));
                    },
                    icon: Icon(
                      Icons.exit_to_app,
                      color: Colors.white,
                      size: 16.0,
                    ),
                    label: Text(
                      "Sign out",
                      style: Styles.semiBoldHeadingFour(color: Colors.white),
                    ),
                    color: AppColors.colorPrimary,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                  ),
                ),
              ],
              mainAxisSize: MainAxisSize.max,
            ),
          ),
        ),
      ),
    );
  }

  void checkApiConfigStatus() {
    _apiConfigBloc.getApiConfigs();
  }

  assignSelectedPlan(String title) {
    selectedPlan = null;
    userPlans.forEach((plans) {
      if (title.contains("High")) {
        if (plans.planType == "2") {
          selectedPlan = plans;
        }
      } else if (title.contains("Medium")) {
        if (plans.planType == "1") {
          selectedPlan = plans;
        }
      } else {
        if (plans.planType == "0") {
          selectedPlan = plans;
        }
      }
    });
  }

  markSelected() {
    var list = userPlans.where((plan) => plan.isDefault).toList();
    if (list.isEmpty) {
      isLow = false;
      isHigh = false;
      isMed = false;
      currentPlanTitle = null;
    }
    else {
      var plan = list[0];
      if (plan.planType == "0") {
        isLow = true;
        isHigh = false;
        isMed = false;
        currentPlanTitle = "Low Risk, Low Profit";
      } else if (plan.planType == "1") {
        isMed = true;
        isLow = false;
        isHigh = false;
        currentPlanTitle = "Medium Risk, Medium Profit";
      } else if (plan.planType == "2") {
        isLow = false;
        isMed = false;
        isHigh = true;
        currentPlanTitle = "High Risk, High Profit";
      }
    }
  }

_listenApiConfigStream() {
  _apiConfigBloc.apiConfigStream.listen((snapshot) async {
    switch (snapshot.status) {
      case Status.LOADING:
        print("Loading");
        return showDialog(
            context: context,
            builder: (context) =>
                Container(
                  color: Colors.transparent,
                  child: Center(
                    child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(
                          AppColors.colorPrimary),
                      backgroundColor: Colors.white,
                    ),
                  ),
                ));
        break;
      case Status.COMPLETED:
//          Navigator.pop(context);
        Navigator.of(context, rootNavigator: true).pop();
        if (!loadInitial) {
          loadInitial = true;
        } else {

        }
        if (snapshot.data.exchangeList.isEmpty) {
          if (fromDrawer) {
            fromDrawer = false;
          var result=await Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (c) =>
                        TradeHistory(
                          model: currentExchange,
                        )));
          if(result!=null){
            Utils.showAnimatedDialog(
                context,
                ApiConfigDialog(
                  type: "Save",
                ));
          }
          } else {
            Utils.showAnimatedDialog(
                context,
                ApiConfigDialog(
                  type: "Save",
                ));
          }
        } else {
          currentExchange = snapshot.data.exchangeList[0];
          if (isCollapsed) {
            if (buttonTapped) {
              buttonTapped = false;

              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (c) =>
                          CommonForm(
                            widget.key,
                            selectedTitle,
                            snapshot.data.exchangeList[0].id.toString(),
                            currentUserPlan:
                            selectedPlan != null ? selectedPlan : null,
                            homBloc: homeBloc,
                            selectedPlanTitle: currentPlanTitle,
                          )));
            } else {}
          } else {
            if (fromDrawer) {
              fromDrawer = false;
              var result=await Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (c) =>
                          TradeHistory(
                            model: currentExchange,
                          )));
              print("resutl is"+result.toString());
            } else {
              Utils.showAnimatedDialog(
                  context,
                  ApiConfigDialog(
                    type: "Update",
                    userExchange: snapshot.data.exchangeList[0],
                  ));
            }
          }
        }
        return Container();
        break;
      case Status.ERROR:
        Fluttertoast.showToast(msg: snapshot.message);
        Navigator.pop(context);
        print("error is ${snapshot.message}");
        if (!isDialogShowing) {
          isDialogShowing = true;
          return OnError(
            btnText: snapshot.message.contains("Unauthorized")
                ? "Login"
                : "Retry",
            errorMessage: snapshot.message,
            onRetryPressed: () async {
              if (snapshot.message.contains("Unauthorized")) {
                Navigator.pushReplacement(
                    context,
                    CupertinoPageRoute(
                        builder: (context) => LoginSignUP()));
              } else {
                await _apiConfigBloc.getApiConfigs();
                Navigator.pop(context);
              }
            },
          );
        } else {
          print("Else");
          return Container();
        }
        break;
      default:
        return Container();
    }
  });
}

_listenRefreshStream() {
  homeBloc.refreshStream.listen((value) async {
    if (value == "Profile") {
      await homeBloc.getUserProfile();
    } else if (value == "Plans") {
      await homeBloc.getUserPlans();
    }
  });
}

animateDrawer() async{
    var connectivity=await Connectivity().checkConnectivity();
  setState(()  {
    if (!isCollapsed) {
      if(connectivity!=ConnectivityResult.none){
        checkApiConfigStatus();
      }
      _animationController.forward();
      _iconAnimationController.reverse();
    } else {
      _animationController.reverse();
      _iconAnimationController.forward();
    }
    isCollapsed = !isCollapsed;
  });
}

_onNavigationItemClicked(int index) async {
  switch (index) {
    case 0:
      setState(() {
        currentWidget = creatHomeScreenPage();
      });
      animateDrawer();
      break;
    case 1:
      if (currentExchange != null) {
         Navigator.push(
            context,
            CupertinoPageRoute(
                builder: (c) =>
                    TradeHistory(
                      model: currentExchange,
                    )));

      } else {
        fromDrawer = true;
        var connectivity=await Connectivity().checkConnectivity();
        if(connectivity!=ConnectivityResult.none){
          _apiConfigBloc.getApiConfigs();
        }
      }
      break;
    case 2:
//        setState(() {
//          currentWidget=WalkThrough();
//        });
      Navigator.push(
          context,
          CupertinoPageRoute(
              builder: (c) =>
                  WalkThrough(
                    fromMenu: true,
                  )));
      break;
    case 3:
//        animateDrawer();
      checkApiConfigStatus();
      break;
    case 4:
      Navigator.push(
          context,
          CupertinoPageRoute(
              builder: (c) =>
                  ChangePassword(
                    title: menuList[4],
                  )));

      break;
    case 5:
      var platform = Platform.isAndroid
          ? " Android Mobile App"
          : "IOS Mobile App";
      _launchURL("support@autrocryso.trade", "Support - $platform",
          "Please type your question here:");
      break;
    case 6:
      Navigator.push(
          context,
          CupertinoPageRoute(
              builder: (c) =>
                  AboutUs(
                    title: menuList[6],
                  )));
      break;
    case 7:
      var url = Platform.isAndroid
          ? 'http://play.google.com/store/apps/details?id=com.skycap.autocrysto'
          : "itms-apps://itunes.apple.com/app/id1455940098?ls";
      Share.share(
          "I'd like to share with you the 'AutoCrysto' app which trades Cryptos automatically. It is very easy to use. Check it out.\n\n App links: $url",
          subject: 'Download AutoCrysto App!');
      break;
    case 8:
      openRateUsPop();
      break;
  }
}

openRateUsPop() async {
  var url = Platform.isAndroid
      ? 'http://play.google.com/store/apps/details?id=com.skycap.autocrysto'
      : "itms-apps://itunes.apple.com/app/id1455940098?ls";
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    Fluttertoast.showToast(msg: "Somewthing went wrong");
  }
}

Container creatHomeScreenPage() {
  return Container(
    child: Column(
      children: <Widget>[
        Align(
          alignment: Alignment.center,
          child: Text(
            "What would you like to do?",
            style: Styles.regularBodyOne(color: Colors.white),
          ),
        ),
        SizedBox(
          height: 30.0,
        ),
        StreamBuilder<ApiResponse<UserPlans>>(
            stream: homeBloc.userPlansStream,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                var data = snapshot.data;
                switch (data.status) {
                  case Status.LOADING:
                    return Container(
//                        color: AppColors.colorPrimary.withOpacity(.2),
//                        margin: EdgeInsets.only(bottom: 10.0),
//                        child: Align(
//                            alignment: Alignment.bottomCenter,
//                            child: CircularProgressIndicator(
//                              valueColor:
//                              AlwaysStoppedAnimation<Color>(Colors.white),
//                            )),
                    );
                    break;
                  case Status.COMPLETED:
                    userPlans = data.data.plansList;
                    markSelected();
                    return SlideTransition(
                      position: _animation,
                      child: Column(
                        children: <Widget>[
                          buildRiskTypeCard("High Risk, High Profit",
                              Images.highRisk, isHigh),
                          buildRiskTypeCard("Medium Risk, Medium Profit",
                              Images.mediumRisk, isMed),
                          buildRiskTypeCard(
                              "Low Risk, Low Profit", Images.lowRisk, isLow),
                        ],
                      ),
                    );
                    break;
                  case Status.ERROR:
                    if (!isDialogShowing) {
                      isDialogShowing = true;
                      return OnError(
                        errorMessage: data.message,
                        btnText: data.message.contains("Unauthorized")
                            ? "Login"
                            : "Retry",
                        onRetryPressed: () async {
                          if (data.message.contains("Unauthorized")) {
                            Navigator.pushReplacement(
                                context,
                                CupertinoPageRoute(
                                    builder: (context) => LoginSignUP()));
                          } else {
                            await homeBloc.getUserProfile();
                          }
                        },
                      );
                    } else {
                      return Container();
                    }

                    break;
                  default:
                    return Container();
                }
              } else {
                return Container();
              }
            })
      ],
    ),
  );
}

_launchURL(String toMailId, String subject, String body) async {
  String uri = 'mailto:support@autrocryso.trade?subject=${Uri.encodeComponent(subject)}&body=${Uri.encodeComponent(body)}';
  try {
    if (await canLaunch(uri)) {
      await launch(uri);
    } else {
      Fluttertoast.showToast(msg: "Kindly check your email setup");
    }
  } catch (e) {
    showDialog(
        context: context,
        builder: (c) =>
            OnError(
              errorMessage: e.toString(),
            ));
  }
}
}
