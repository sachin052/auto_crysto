import 'dart:async';
import 'dart:io';

import 'package:auto_crysto/ui/data/ApiHelper/ApiResponse.dart';
import 'package:auto_crysto/ui/data/ApiHelper/Repo/AppRepo.dart';
import 'package:auto_crysto/ui/data/models/CommonApiResponse.dart';
import 'package:auto_crysto/ui/data/models/States.dart';
import 'package:auto_crysto/ui/utils/Utils.dart';
import 'package:path_provider/path_provider.dart';
import 'package:rxdart/rxdart.dart';

import '../../fieldvalidators.dart';

class SignUpBlock {
  AppRepo appRepo;
  StreamController countryController;
  StreamController stateController;
  StreamController cityController;
  StreamController signUpController;

  StreamSink<ApiResponse<List<States>>> get countryListSink =>
      countryController.sink;

  StreamSink<ApiResponse<List<States>>> get stateListSink =>
      stateController.sink;

  StreamSink<ApiResponse<List<States>>> get cityListSink => cityController.sink;

  StreamSink<ApiResponse<CommonApiResponse>> get signUpSink =>
      signUpController.sink;

  Stream<ApiResponse<List<States>>> get countryListStream =>
      countryController.stream;

  Stream<ApiResponse<List<States>>> get stateListStream =>
      stateController.stream;

  Stream<ApiResponse<List<States>>> get cityListStream => cityController.stream;

  Stream<ApiResponse<CommonApiResponse>> get signUpStream =>
      signUpController.stream;
  PublishSubject<bool> _userCreated;

  PublishSubject<bool> get userCreated => _userCreated;
  bool isPickerOpen = false;
  bool requestSent = false;

  //first name
  final _fNameController=BehaviorSubject<String>();
  Function(String) get changeFName=>_fNameController.sink.add;
  Stream<String> get firstName=>_fNameController.stream.transform(notEmptyValidator);

  //last name
  final _lNameController=BehaviorSubject<String>();
  Function(String) get changeLName=> _lNameController.sink.add;
  Stream<String> get lastName=>_lNameController.stream.transform(notEmptyValidator);

  //email
  final _emailController = BehaviorSubject<String>();
  Function(String) get changeEmail=>_emailController.sink.add;
  Stream<String> get email=>_emailController.stream.transform(validateEmail);

  //password
  final _pController=BehaviorSubject<String>();
  Function(String) get changePass=>_pController.sink.add;
  Stream<String> get password=>_pController.stream.transform(validatePassword);

  //confirm password
  final _cPController=BehaviorSubject<String>();
  Function(String) get changecCPassword=>_cPController.sink.add;

  // confirm password validation
  Stream<String> get cPassword=>confirmPassValidator(_pController, _cPController);

  //country
  final _countryController=BehaviorSubject<String>();
  Function(String) get changeCountry=>_countryController.sink.add;
  Stream<String> get country=>_countryController.stream;

  // state
  final _stateController=BehaviorSubject<String>();
  Function(String) get changeState=>_stateController.sink.add;
  Stream<String> get state=>_stateController.stream;

  // city
  final _cityController=BehaviorSubject<String>();
  Function(String) get changeCity=>_cityController.sink.add;
  Stream<String> get city=>_cityController.stream;

  Stream<bool> get isValid=>CombineLatestStream.combine8(firstName, lastName, email, password, cPassword, country, state, city, (a,b,c,d,e,f,g,h)=>true);

  SignUpBlock() {
    countryController = StreamController<ApiResponse<List<States>>>.broadcast();
    stateController = StreamController<ApiResponse<List<States>>>.broadcast();
    cityController = StreamController<ApiResponse<List<States>>>.broadcast();
    signUpController =
        StreamController<ApiResponse<CommonApiResponse>>.broadcast();
    _userCreated = PublishSubject<bool>();
    appRepo = AppRepo();
  }

  fetchCountryList() async {
    if (!requestSent) {
      requestSent = true;
      countryListSink.add(ApiResponse.loading("Loading Countries"));
      try {
        var response = await appRepo.fetchCountryList();
        var countries=ApiResponse.completed(response);
        changeCountry(countries.data[0].title);
        countryListSink.add(countries);
        requestSent = false;
      } catch (ex) {
        countryListSink.add(ApiResponse.error(ex.toString()));
        requestSent = false;
      }
    }
  }

  fetchStateList(String countryID) async {
    if (!requestSent) {
      requestSent = true;
      stateListSink.add(ApiResponse.loading("Loading States"));
      try {
        var response = await appRepo.fetchStateList(countryID);
        stateListSink.add(ApiResponse.completed(response));
        requestSent = false;
      } catch (ex) {
        stateListSink.add(ApiResponse.error(ex.toString()));
        requestSent = false;
      }
    }
  }

  fetchCityList(String stateID) async {
    if (!requestSent) {
      requestSent = true;
      cityListSink.add(ApiResponse.loading("Loading Cities"));
      try {
        var response = await appRepo.fetchCityList(stateID);
        print(response);
        cityListSink.add(ApiResponse.completed(response));
        requestSent = false;
      } catch (ex) {
        cityListSink.add(ApiResponse.error(ex.toString()));
        requestSent = false;
      }
    }
  }

  signUp(Map<String, String> body) async {
    signUpSink.add(ApiResponse.loading("Creating your account"));
    try {
      var response = await appRepo.signUp(body);
      signUpSink
          .add(ApiResponse.completed(CommonApiResponse.fromJson(response)));
    } catch (ex) {
      signUpSink.add(ApiResponse.error(ex.toString()));
    }
  }

  Future<dynamic> uploadImage(File localImage) async {
    Directory tempDir = await getApplicationDocumentsDirectory();
    File compressedImage = await Utils.compressImage(
      localImage,
      tempDir.path,
    );
    print("Size of image is${compressedImage.lengthSync()}");
    return await appRepo.uploadImage(compressedImage);
  }

  dispose() {
    countryController.close();
    stateController.close();
    cityController.close();
    signUpController.close();
    _userCreated.close();
    _cPController.close();
    _pController.close();
    _emailController.close();
    _lNameController.close();
    _fNameController.close();
    _countryController.close();
    _stateController.close();
    _cityController.close();
  }

  bool validateInput() {
    var result=true;
    if(_fNameController.value==null){
      _fNameController.sink.addError("First name must not be empty");
      result= false;
    }
    if(_lNameController.value==null){
      _lNameController.sink.addError("Last name must not be empty");
      result= false;
    }
    if(_emailController.value==null){
      _emailController.sink.addError("Email must not be empty");
      result= false;
    }
    if(_pController.value==null){
      _pController.sink.addError("Password must not be empty");
      result= false;
    }
    if(_cPController.value==null){
      _cPController.sink.addError("Confirm Password must not be empty");
      result=false;
    }
    if(_countryController.value==null){
      _countryController.sink.addError("Country must not be empty");
      result=false;
    }if(_stateController.value==null){
      _stateController.sink.addError("State must not be empty");
    result=false;
    }
    if(_cityController.value==null){
      _cityController.sink.addError("City must not be empty");
      result=false;
    }
    return result;
  }
}
