import 'dart:convert';
import 'dart:io';

import 'package:auto_crysto/ui/commonui/CustomMaterialButton.dart';
import 'package:auto_crysto/ui/commonui/ImagePicker.dart' as prefix0;
import 'package:auto_crysto/ui/commonui/ImagePicker.dart';
import 'package:auto_crysto/ui/data/ApiHelper/ApiResponse.dart';
import 'package:auto_crysto/ui/data/models/States.dart';
import 'package:auto_crysto/ui/login_signup/SignUpBloc.dart';
import 'package:auto_crysto/ui/utils/AppColors.dart';
import 'package:auto_crysto/ui/utils/Images.dart';
import 'package:auto_crysto/ui/utils/Styles.dart';
import 'package:auto_crysto/ui/utils/Utils.dart';
import 'package:auto_crysto/ui/utils/Validators.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import '../../main.dart';

TextEditingController _fnameController = TextEditingController();
TextEditingController _lNameController = TextEditingController();
TextEditingController _emailController = TextEditingController();
TextEditingController _passController = TextEditingController();
TextEditingController _cPassController = TextEditingController();
TextEditingController _stateController = TextEditingController();
TextEditingController _cityController = TextEditingController();
TextEditingController _countryController = TextEditingController();
var email = "";
var password = "";
var fName = "";
var lName = "";
var cityId = "";
var profilePic = "";

class SignUpUI extends StatefulWidget {
  final VoidCallback callback;

  const SignUpUI({Key key, this.callback}) : super(key: key);

  @override
  _SignUpUIState createState() => _SignUpUIState(this.callback);
  clearAllData(){
    _fnameController.clear();
    _lNameController.clear();
    _emailController.clear();
    _countryController.clear();
    _cityController.clear();
    _stateController.clear();
    _passController.clear();
    _cPassController.clear();
  }
}

class _SignUpUIState extends State<SignUpUI>
    with SingleTickerProviderStateMixin {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  AnimationController _animationController;
  Animation<double> _animation;
  String _fNameError;
  String _lNameError;
  String _emailError;
  String _passError;
  String _confirmPassError;
  String _countryError;
  String _stateError;
  String _cityError;
  List<States> _countriesList;
  List<States> _statesList;
  List<States> _citiesList;
  String _selectedCountryID;
  final VoidCallback callback;
  int _selectedStateID = 0;
  int _selectedCityID = 0;
  var json = JsonCodec();
  var _countryFocusNode = FocusNode();
  var _cityFocusNode = FocusNode();
  SignUpBlock signUpBlock;
  File _image;
  // All Focus nodes
  FocusNode fNameNode =FocusNode();
  FocusNode lNameNode =FocusNode();
  FocusNode emailNode =FocusNode();
  FocusNode passNode =FocusNode();
  FocusNode cPassNode =FocusNode();
  _SignUpUIState(this.callback);

  @override
  void initState() {
    super.initState();
    _animationController = new AnimationController(
        vsync: this, duration: new Duration(milliseconds: 500));
    _animation = Tween(begin: 0.0, end: 1.0).animate(_animationController);
    _animationController.forward();
    signUpBlock = locator<SignUpBlock>();
    _countryFocusNode.addListener(showPicker);
    _fnameController.addListener(setFNameError);
    _lNameController.addListener(setLNameError);
    _emailController.addListener(setEmailError);
    _passController.addListener(setPasswordError);
    _cPassController.addListener(setCPasswordError);

    bindListeners();
    retrieveLostData();
//    _loadStateList();
  }

  @override
  void dispose() {

    // TODO: implement dispose
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: FadeTransition(
        opacity: _animation,
        child: Material(
          child: Container(
            padding: EdgeInsets.only(left: 20.0, right: 20.0),
            child: Column(
              children: <Widget>[
                Align(
                  alignment: Alignment.center,
                  child: Stack(
                    children: <Widget>[
                      Align(
                        child: InkWell(
                          child: Material(
                            elevation: 10.0,
                            borderRadius: BorderRadius.circular(50.0),
                            child: Container(
                              height: 100,
                              width: 100,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: _image == null
                                        ? AssetImage(Images.profile)
                                        : FileImage(_image),
                                    fit: BoxFit.cover),
                                border: Border.all(
                                    color:
                                        AppColors.colorPrimary.withOpacity(.5),
                                    width: 2.0,
                                    style: BorderStyle.solid),
                                borderRadius: BorderRadius.circular(50.0),
                              ),
                            ),
                          ),
                        ),
                        alignment: Alignment.center,
                      ),
                      Align(
                        alignment: Alignment.topCenter,
                        child: Padding(
                          padding: EdgeInsets.only(
                              left: MediaQuery.of(context).size.width * .19),
                          child: MaterialButton(
                            onPressed: () {
                              if (Platform.isIOS) {
                                actionSheetMethodIOS(context,getImage);
                              } else {
                                prefix0.androidPicker(context, getImage);
                              }
                            },
                            child: Container(
                              height: 25.0,
                              width: 25.0,
                              child: Icon(
                                Icons.add,
                                color: Colors.white,
                                size: 15.0,
                              ),
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      color: Colors.white, width: 1.0),
                                  borderRadius: BorderRadius.circular(13.0),
                                  color: AppColors.colorPrimary),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        height: 15.0,
                      ),
                      FirstNameWidget(
                          widget.key, _fnameController, _fNameError,lNameNode,fNameNode,signUpBlock),
                      SizedBox(
                        height: 15.0,
                      ),
                      LastNameWidget(widget.key, _lNameController, _lNameError,lNameNode,emailNode,signUpBlock),
                      SizedBox(
                        height: 15.0,
                      ),
                      EmailWidget(widget.key, _emailController,
                          _emailError,emailNode,passNode,signUpBlock),
                      SizedBox(
                        height: 15.0,
                      ),
                      PasswordWidget(widget.key, _passController, _passError,passNode,cPassNode,signUpBlock),
                      SizedBox(
                        height: 15.0,
                      ),
                      ConfirmPassWidget(
                          widget.key, _cPassController, _confirmPassError,cPassNode,_countryFocusNode,signUpBlock),
                      SizedBox(
                        height: 15.0,
                      ),
                      Stack(
                        children: <Widget>[
                          GestureDetector(
                            child: AbsorbPointer(
                              child: StreamBuilder<String>(
                                stream: signUpBlock.country,
                                builder: (context, snapshot) {
                                  return TextField(
                                    onChanged: (value){
                                      signUpBlock.changeCountry(value);
                                          _countryController.text=value;
                                    },
                                    showCursor: false,
                                    readOnly: true,
                                    autofocus: false,
                                    controller: _countryController,
                                    decoration: InputDecoration(
                                      errorText: snapshot?.error,
                                      contentPadding: EdgeInsets.all(14.0),
                                      labelText: "Select Country",
                                      focusColor: AppColors.colorPrimary,
                                      fillColor: AppColors.colorPrimary,
                                      hoverColor: AppColors.colorPrimary,
                                      border: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: AppColors.colorPrimary,
                                              width: 1.0)),
                                    ),
                                    keyboardType: TextInputType.text,
                                    cursorColor: AppColors.colorPrimary,
                                  );
                                }
                              ),
                            ),
                            onTap: () {
                              FocusScope.of(context).requestFocus(FocusNode());
                              showPicker();
                            },
                          ),
                          Align(
                              alignment: Alignment.centerRight,
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    top: 10.0, right: 4.0),
                                child: Icon(Icons.keyboard_arrow_down,color: Colors.black26,),
                              ))
                        ],
                      ),
                      SizedBox(
                        height: 15.0,
                      ),
                      Stack(
                        children: <Widget>[
                          GestureDetector(
                            onTap: () {
                              FocusScope.of(context).requestFocus(FocusNode());
                              showStatePicker();
                            },
                            child: AbsorbPointer(
                              child: StreamBuilder<String>(
                                stream: signUpBlock.state,
                                builder: (context, snapshot) {
                                  return TextField(
                                    controller: _stateController,
                                    focusNode: _cityFocusNode,
                                    showCursor: false,
                                    readOnly: true,
                                    autofocus: false,
                                    decoration: InputDecoration(
                                      errorText: snapshot?.error,
                                      contentPadding: EdgeInsets.all(14.0),
                                      labelText: "Select State",
                                      focusColor: AppColors.colorPrimary,
                                      fillColor: AppColors.colorPrimary,
                                      hoverColor: AppColors.colorPrimary,
                                      border: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: AppColors.colorPrimary,
                                              width: 1.0)),
                                    ),
                                    keyboardType: TextInputType.text,
                                    cursorColor: AppColors.colorPrimary,
                                  );
                                }
                              ),
                            ),
                          ),
                          Align(
                              alignment: Alignment.centerRight,
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    top: 10.0, right: 4.0),
                                child: Icon(Icons.keyboard_arrow_down,color: Colors.black26,),
                              ))
                        ],
                      ),
                      SizedBox(
                        height: 15.0,
                      ),
                      Stack(
                        children: <Widget>[
                          GestureDetector(
                            onTap: () {
                              FocusScope.of(context).requestFocus(FocusNode());
                              showCityPicker();
                            },
                            child: AbsorbPointer(
                              child: StreamBuilder<String>(
                                stream: signUpBlock.city,
                                builder: (context, snapshot) {
                                  return TextField(
                                    controller: _cityController,
                                    showCursor: false,
                                    readOnly: true,
                                    onChanged: signUpBlock.changeCity,
                                    autofocus: false,
                                    decoration: InputDecoration(
                                      errorText: snapshot?.error,
                                      contentPadding: EdgeInsets.all(14.0),
                                      labelText: "Select City",
                                      focusColor: AppColors.colorPrimary,
                                      fillColor: AppColors.colorPrimary,
                                      hoverColor: AppColors.colorPrimary,
                                      border: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: AppColors.colorPrimary,
                                              width: 1.0)),
                                    ),
                                    keyboardType: TextInputType.text,
                                    cursorColor: AppColors.colorPrimary,
                                  );
                                }
                              ),
                            ),
                          ),
                          Align(
                              alignment: Alignment.centerRight,
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    top: 10.0, right: 4.0),
                                child: Icon(Icons.keyboard_arrow_down,color: Colors.black26,),
                              ))
                        ],
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      StreamBuilder<bool>(
                        stream: signUpBlock.isValid,
                        builder: (context, snapshot) {
                          return CustomMaterialButton(widget.key, () {
                            if(signUpBlock.validateInput()&&snapshot?.data==true)
                            hitLoginApi();
                          }, "Register");
                        }
                      ),
//                      buildLoginButton(),
                      SizedBox(
                        height: 15.0,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget buildPicker(BuildContext context) {
    final FixedExtentScrollController scrollController =
        FixedExtentScrollController(initialItem: 0);
    return Container(
      height: Utils().getScreenHeight(context) * .35,
      width: Utils().getScreenWidth(context),
      child: Column(

        children: <Widget>[
          Material(
            child: Container(
                width: Utils().getScreenWidth(context),
                padding: EdgeInsets.only(top:12.0),
                color: Colors.white,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                  GestureDetector(
                      onTap: (){
                        Navigator.pop(context);
                      },
                      child: Text("Cancel",style: TextStyle(color: Colors.red,fontSize: 16.0),)),
                  Text("Select Country",style: TextStyle(fontSize: 16.0),),
                  GestureDetector(
                      onTap: (){
                        Navigator.pop(context);
                      },
                      child: Text("Done",style: TextStyle(color: AppColors.colorPrimary,fontSize: 16.0),))
                ],)),
          ),
          Expanded(
            child: CupertinoPicker(
              itemExtent: 35.0,
              onSelectedItemChanged: (index) {
                setState(() {
                  print(_countriesList[index].title);
                });
              },
              children: _countriesList != null
                  ? List<Widget>.generate(_countriesList.length, (index) {

                      return Center(
                        child: Text(_countriesList[index].title),
                      );
                    })
                  : List<Widget>.generate(0, (context) {}),
              backgroundColor: CupertinoColors.white,
              useMagnifier: true,
            ),
          ),
        ],
      ),
    );
  }

  Container buildStatePicker(BuildContext context) {
//    final FixedExtentScrollController scrollController =
//    FixedExtentScrollController(initialItem: _selectedStateID);
    return Container(
      height: Utils().getScreenHeight(context) * .35,
      width: Utils().getScreenWidth(context),
      child: Column(
        children: <Widget>[
          Material(
            child: Container(
                width: Utils().getScreenWidth(context),
                padding: EdgeInsets.only(top:12.0),
                color: Colors.white,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    GestureDetector(
                        onTap: (){
                          Navigator.pop(context);
                        },
                        child: Text("Cancel",style: TextStyle(color: Colors.red,fontSize: 16.0),)),
                    Text("Select State",style: TextStyle(fontSize: 16.0),),
                    GestureDetector(
                        onTap: (){
                          Navigator.pop(context);
                        },
                        child: Text("Done",style: TextStyle(color: AppColors.colorPrimary,fontSize: 16.0),))
                  ],)),
          ),
          Expanded(
            child: CupertinoPicker(
              itemExtent: 35.0,
              onSelectedItemChanged: (index) {
                signUpBlock.changeState(_statesList[index].title);
                setState(() {
                  _selectedStateID = _statesList[index].id;
                  print(_selectedStateID);
                  _stateController.text = _statesList[index].title;
                  _selectedStateID = _statesList[index].id;
                });
              },
              children: _statesList == null
                  ? null
                  : List<Widget>.generate(_statesList.length, (index) {
                      return Center(
                        child: Text(_statesList[index].title),
                      );
                    }),
              backgroundColor: CupertinoColors.white,
              useMagnifier: true,
              magnification: 1.2,
            ),
          ),
        ],
      ),
    );
  }

  Container buildCityPicker(BuildContext context) {
//    final FixedExtentScrollController scrollController =
//    FixedExtentScrollController(initialItem: _selectedStateID);
    return Container(
      height: Utils().getScreenHeight(context) * .35,
      width: Utils().getScreenWidth(context),
      child: Column(
        children: <Widget>[
          Material(
            child: Container(
                width: Utils().getScreenWidth(context),
                padding: EdgeInsets.only(top:12.0),
                color: Colors.white,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    GestureDetector(
                        onTap: (){
                          Navigator.pop(context);
                        },
                        child: Text("Cancel",style: TextStyle(color: Colors.red,fontSize: 16.0),)),
                    Text("Select City",style: TextStyle(fontSize: 16.0),),
                    GestureDetector(
                        onTap: (){
                          Navigator.pop(context);
                        },
                        child: Text("Done",style: TextStyle(color: AppColors.colorPrimary,fontSize: 16.0),))
                  ],)),
          ),
          Expanded(
            child: CupertinoPicker(
              itemExtent: 35.0,
              onSelectedItemChanged: (index) {
                signUpBlock.changeCity(_citiesList[index].title);
                setState(() {
                  _selectedCityID = _citiesList[index].id;
                  _cityController.text = _citiesList[index].title;
                  cityId = _citiesList[index].id.toString();
                });
              },
              children: _citiesList == null
                  ? List<Widget>.generate(0, (context) {})
                  : List<Widget>.generate(_citiesList.length, (index) {
                      return Center(
                        child: Text(_citiesList[index].title),
                      );
                    }),
              backgroundColor: CupertinoColors.white,
              useMagnifier: true,
              magnification: 1.2,
            ),
          ),
        ],
      ),
    );
  }

  showPicker() async {

    if (_countriesList == null) await _loadCountriesList();
    if(!signUpBlock.isPickerOpen){
      signUpBlock.isPickerOpen=true;
      await showCupertinoModalPopup(

          context: context,

          builder: (context) {
            return buildPicker(context);
          }).whenComplete((){
            signUpBlock.isPickerOpen=false;
      });
    }

  }

  showCityPicker() async {
    var response = await _loadCityList();
   if(!signUpBlock.isPickerOpen){
     signUpBlock.isPickerOpen=true;
     await showCupertinoModalPopup(
         context: context,
         builder: (context) {
           return buildCityPicker(context);
         }).whenComplete((){
       signUpBlock.isPickerOpen=false;
     });
   }
  }

  showStatePicker() async {
    if(!signUpBlock.isPickerOpen) await _loadStateList();
    if(!signUpBlock.isPickerOpen){
      signUpBlock.isPickerOpen=true;
      await showCupertinoModalPopup(
          context: context,
          builder: (context) {
            return buildStatePicker(context);
          }).whenComplete((){
        signUpBlock.isPickerOpen=false;
      });
    }
  }

  File getImage(File file)  {
//    Navigator.pop(context);
//    var image = await ImagePicker.pickImage(source: ImageSource.camera);
    setState(() {
      _image = file;
    });
    return file;
  }
  Future<void> retrieveLostData() async {
    final LostDataResponse response =
    await ImagePicker.retrieveLostData();
    if (response == null) {
      return;
    }
    if (response.file != null) {
      setState(() {
          _image=response.file;
      });
    } else {
//      _handleError(response.exception);
    }
  }
  Future _loadCountriesList() async {
    await signUpBlock.fetchCountryList();
  }

  Future _loadStateList() async {
    await signUpBlock.fetchStateList(_selectedCountryID);
  }

  bindListeners() {
    _listenCountries();
    _listenStates();
    _listenCities();
    _listenSignUp();
  }

  Future<dynamic> _loadCityList() async {
    var response = await signUpBlock.fetchCityList(_selectedStateID.toString());
    return response;
  }

  setFNameError() {
    if (_fnameController.text.length > 0 && _fnameController.text.length < 3) {
      setState(() {
        _fNameError = "Req min 3 chars";
      });
    } else {
      setState(() {
        _fNameError = null;
      });
    }
  }

  setLNameError() {
    if (_lNameController.text.length > 0 && _lNameController.text.length < 3) {
      setState(() {
        _lNameError = "Req min 3 chars";
      });
    } else {
      setState(() {
        _lNameError = null;
      });
    }
  }

  setEmailError() {
    if (!EmailValidator.validate(_emailController.text) &&
        _emailController.text.isNotEmpty) {
      setState(() {
        _emailError = "Invalid Email id";
      });
    } else {
      setState(() {
        _emailError = null;
      });
    }
    print(_emailController.text);
  }

  setPasswordError() {
    if (_passController.text.length < 6 && _passController.text.isNotEmpty) {
      setState(() {
        _passError = "Req min 6 character";
      });
    } else {
      setState(() {
        _passError = null;
      });
    }
  }

  setCPasswordError() {
    if (_passController.text != _cPassController.text &&
        _cPassController.text.isNotEmpty) {
      setState(() {
        _confirmPassError = "Password mismatch";
      });
    } else {
      setState(() {
        _confirmPassError = null;
      });
    }
  }

  hitLoginApi() async {
      if (_image != null) {
        Utils.showProgressDialog(context, "Uploading Image");
        await uploadImage(_image);
      } else {
        uploadFormToServer();
      }

  }


  Future getImageFromGallery(BuildContext context) async {
    Navigator.pop(context);
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image = image;

    });
  }

  uploadImage(File localImage) async {
    profilePic = await signUpBlock.uploadImage(localImage);
    Navigator.of(context,rootNavigator: true).pop();
    print("uploaded image is $profilePic");
    Scaffold.of(context).hideCurrentSnackBar();
    uploadFormToServer();
  }

  void uploadFormToServer() async {
    Map<String, String> body = {
      "first_name": _fnameController.text,
      "last_name": _lNameController.text,
      "email": _emailController.text,
      "password": _passController.text,
      "city": _selectedCityID.toString(),
      "profile_pic": profilePic
    };
    await signUpBlock.signUp(body);
//    try {
//
//      _onLoading("Submitting form");
//      var response =
//          await http.post(ApiConstants.baseUrl + "/auth/signup", body: body);
//      if (response.statusCode == 200) {
//        print(response.body);
//        _showDialog(json.decode(response.body)["message"]);
//      } else {
//        print(json.decode(response.body));
//        Utils.showShowSnackBar(
//            context, json.decode(response.body)["error"], true);
//      }
//      Scaffold.of(context).hideCurrentSnackBar();
//    } catch (exception) {
//      if (exception is SocketException) {
//        Scaffold.of(context).hideCurrentSnackBar();
//        Utils.showShowSnackBar(
//            context, "Please check your inetrnet connecttion", true);
//      }
//    }
  }

  void _listenStates() {
    signUpBlock.stateListStream.listen((data) {
      switch (data.status) {
        case Status.LOADING:
          Utils.onLoading("Loading States", context);
          break;
        case Status.COMPLETED:
          Scaffold.of(context).hideCurrentSnackBar();
          _statesList = data.data;
          break;
        case Status.ERROR:
          print(data.message);
          break;
      }
    }, onDone: () {
      print("Task Done");
    }, onError: (error) {
      print("Some Error");
    });
  }

  void _listenCountries() {
    signUpBlock.countryListStream.listen(
        (data) {
          switch (data.status) {
            case Status.LOADING:
              Utils.onLoading("Loading Countries", context);
              break;
            case Status.COMPLETED:
              Scaffold.of(context).hideCurrentSnackBar();
              _countriesList = data.data;
              _countryController.text = _countriesList[0].title;
              _selectedCountryID = _countriesList[0].id.toString();
              break;
            case Status.ERROR:
              Scaffold.of(context).hideCurrentSnackBar();
              print(data.message);
              break;
          }
        },
        onDone: () {},
        onError: (error) {
          print("Some Error");
        });
  }

  void _listenCities() {
    signUpBlock.cityListStream.listen(
        (data) {
          switch (data.status) {
            case Status.LOADING:
              Utils.onLoading(data.message, context,longSnackBar:true);
              break;
            case Status.COMPLETED:
              Scaffold.of(context).hideCurrentSnackBar();
              _citiesList = data.data;
              print("LIST IS $_citiesList");
              break;
            case Status.ERROR:
              print(data.message);
              break;
          }
        },
        onDone: () {},
        onError: (error) {
          print("Some Error");
        });
  }

  void _listenSignUp() {
    signUpBlock.signUpStream.listen(
        (data) {
          switch (data.status) {
            case Status.LOADING:
              Utils.onLoading(data.message, context);
              break;
            case Status.COMPLETED:
              _fnameController.text="";
              _lNameController.text="";
              _emailController.text="";
              _passController.text="";
              _cPassController.text="";
              _countryController.text="";
              _stateController.text="";
              _cityController.text="";
              Scaffold.of(context).hideCurrentSnackBar();
              signUpBlock.userCreated.add(true);
              Utils.showShowSnackBar(context, data.data.message, false);
              break;
            case Status.ERROR:
              Utils.showShowSnackBar(context, data.message, true);
              break;
          }
        },
        onDone: () {},
        onError: (error) {
          print("Some Error");
        });
  }
}

class PasswordWidget extends StatelessWidget {
  final TextEditingController passwordController;
  final String passError;
  final FocusNode nextFocusNode;
  final FocusNode focusNode;
  final SignUpBlock _signUpBlock;
  const PasswordWidget(Key key, this.passwordController, this.passError, this.focusNode, this.nextFocusNode, this._signUpBlock)
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<String>(
        stream: _signUpBlock.password,
      builder: (context, snapshot) {
        return TextField(
          controller: passwordController,
          obscureText: true,
          autofocus: false,
          focusNode: focusNode,
          onEditingComplete: (){
            moveToNext(context, nextFocusNode);
          },
          textInputAction: TextInputAction.next,
          onChanged: _signUpBlock.changePass,
          keyboardType: TextInputType.text,
          decoration: Styles.getOutlineBorder("Password", snapshot?.error),
          cursorColor: AppColors.colorPrimary,
        );
      }
    );
  }
}

class EmailWidget extends StatelessWidget {
  final TextEditingController emailController;
  final String emailError;
  final FocusNode nextFocusNode;
  final FocusNode focusNode;
  final SignUpBlock _signUpBlock;
  const EmailWidget(
      Key key, this.emailController, this.emailError, this.focusNode, this.nextFocusNode, this._signUpBlock)
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<String>(
      stream: _signUpBlock.email,
      builder: (context, snapshot) {
        return TextField(
          autofocus: false,
          focusNode: focusNode,
          textInputAction: TextInputAction.next,
          onEditingComplete: (){
            moveToNext(context, nextFocusNode);
          },
          onChanged: _signUpBlock.changeEmail,
          controller: emailController,
          cursorColor: AppColors.colorPrimary,
          keyboardType: TextInputType.emailAddress,
          decoration: Styles.getOutlineBorder('Email', snapshot?.error),
        );
      }
    );
  }


}

class ConfirmPassWidget extends StatelessWidget {
  final TextEditingController passwordController;
  final String confirmPass;
  final FocusNode nextFocusNode;
  final FocusNode focusNode;
  final SignUpBlock _signUpBlock;
  const ConfirmPassWidget(Key key, this.passwordController, this.confirmPass, this.focusNode, this.nextFocusNode, this._signUpBlock)
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<String>(
      stream: _signUpBlock.cPassword,
      builder: (context, snapshot) {
        return TextField(
          controller: passwordController,
          obscureText: true,
          autofocus: false,
          focusNode: focusNode,
          onChanged: _signUpBlock.changecCPassword,
          keyboardType: TextInputType.text,
          decoration: Styles.getOutlineBorder("Confirm Password", snapshot?.error),
          cursorColor: AppColors.colorPrimary,
        );
      }
    );
  }
}

class FirstNameWidget extends StatelessWidget {
  final TextEditingController fNameController;
  final String fNameError;
  final FocusNode nextFocusNode;
  final FocusNode focusNode;
  final SignUpBlock _signUpBlock;
  const FirstNameWidget (Key key, this.fNameController, this.fNameError, this.nextFocusNode, this.focusNode, this._signUpBlock)
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<String>(
      stream: _signUpBlock.firstName,
      builder: (context, snapshot) {
        return TextField(
          controller: fNameController,
          autofocus: false,
          focusNode: focusNode,

//      onSaved: (value) => fName = value,
          textInputAction: TextInputAction.next,
          onChanged:_signUpBlock.changeFName,
          onEditingComplete: (){
            moveToNext(context, this.nextFocusNode);
          },
          keyboardType: TextInputType.text,
          decoration: Styles.getOutlineBorder("First name", snapshot?.error),
          cursorColor: AppColors.colorPrimary,
        );
      }
    );
  }
}

class LastNameWidget extends StatelessWidget {
  final TextEditingController lNameController;
  final String lNameError;
  final FocusNode nextFocusNode;
  final FocusNode focusNode;
  final SignUpBlock _signUpBlock;
  const LastNameWidget(Key key, this.lNameController, this.lNameError, this.focusNode, this.nextFocusNode, this._signUpBlock, )
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  StreamBuilder<String>(
      stream: _signUpBlock.lastName,
      builder: (context, snapshot) {
        return TextField(
          controller: lNameController,
          autofocus: false,
          focusNode: focusNode,
          textInputAction: TextInputAction.next,
          onEditingComplete: (){
            moveToNext(context, nextFocusNode);
          },
          onChanged: _signUpBlock.changeLName,
          keyboardType: TextInputType.text,
          decoration: Styles.getOutlineBorder("Last name", snapshot?.error),
          cursorColor: AppColors.colorPrimary,
        );
      }
    );
  }
}

moveToNext(BuildContext context,FocusNode focusNode){
  FocusScope.of(context).requestFocus(focusNode);
}