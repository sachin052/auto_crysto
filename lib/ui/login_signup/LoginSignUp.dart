import 'package:auto_crysto/routes/Router.dart';
import 'package:auto_crysto/ui/utils/AppColors.dart';
import 'package:auto_crysto/ui/utils/Images.dart';
import 'package:auto_crysto/ui/utils/Styles.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../../main.dart';
import 'LoginUI.dart';
import 'SignUpBloc.dart';
import 'SignUpUI.dart';

class LoginSignUP extends StatefulWidget {
  @override
  _LoginSignUPState createState() => _LoginSignUPState();
}

class _LoginSignUPState extends State<LoginSignUP>
    with SingleTickerProviderStateMixin {
  List<Object> object = List();
  var clipperIndex = 0;
  AnimationController _animationController;
  Animation<double> _animation;
  Animation<Offset> _slideAnimation;
  Animation<double> _fadeTransition;
  Widget _currentUI;
  Color _signInColor = Colors.black;
  Color _signUpColor = Colors.white.withOpacity(.4);
  SignUpBlock signUpBlock;

  @override
  void dispose() {
    super.dispose();
    _animationController.dispose();
    signUpBlock.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _currentUI = locator<LoginUI>();
    object.add(WaveClipperOne());
    object.add(BottomWaveClipper());
    _animationController = new AnimationController(
        vsync: this, duration: new Duration(milliseconds: 600));
    _animation = new CurvedAnimation(
      parent: _animationController,
      curve: Curves.fastLinearToSlowEaseIn,
    );
    _fadeTransition = Tween(begin: 0.0, end: 1.0).animate(_animationController);
    _slideAnimation = Tween<Offset>(begin: Offset(-1.0, 0.0), end: Offset(0, 0))
        .animate(_animationController);
    _animation.addListener(() => this.setState(() {}));
    _slideAnimation.addListener(() => this.setState(() {}));
    _animationController.forward();
    signUpBlock = locator<SignUpBlock>();
    signUpBlock.userCreated.listen((value) {
      if (value == true) {
        setState(() {
          _signInColor = Colors.black;
          _signUpColor = Colors.white.withOpacity(.4);
          clipperIndex = 0;
          _currentUI = locator<LoginUI>();
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        onGenerateRoute: Router.generateRoute,
        theme: Styles.getTheme(),
        home: MediaQuery(
           data: MediaQuery.of(context).size.width>600?MediaQuery.of(context).copyWith(textScaleFactor: 1.8):MediaQuery.of(context),
          child: DefaultTabController(
            length: 1,
            child: Scaffold(
//            backgroundColor: Colors.white,
              body: Stack(
                children: <Widget>[
                  Container(
                    color: Colors.transparent,
                    height: 200,
                    child: ClipPath(
                      clipper: object.elementAt(clipperIndex),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                                colors: [
                                  AppColors.colorPrimary,
                                  AppColors.colorPrimaryGradient
                                ],
                                begin: Alignment.centerRight,
                                stops: [0.0, 1.0],
                                end: Alignment.centerLeft)
//                      image: DecorationImage(image: AssetImage(Images.curveBg),fit: BoxFit.fill)
                            ),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Image.asset(
                              Images.app_logo_white,
                              width: 150,
                              height: 50,
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: GestureDetector(
                          onTap: () {
                            setState(() {
                              _signInColor = Colors.black;
                              _signUpColor = Colors.white.withOpacity(.4);
                              clipperIndex = 0;
                              _currentUI = locator<LoginUI>();
                            });
                          },
                          child: Container(
                            height: 200.0,
                            padding: EdgeInsets.only(top: 150.0),
                            child: Align(
                              alignment: Alignment.center,
                              child: Text(
                                "Sign In",
                                style: Styles.semiBoldHeadingThree(
                                    color: _signInColor),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: GestureDetector(
                          onTap: () {
                            setState(() {
                              _signInColor = Colors.white.withOpacity(.4);
                              _signUpColor = Colors.black;
                              clipperIndex = 1;
                              _currentUI = locator<SignUpUI>();
                            });
                          },
                          child: Container(
                            height: 200,
                            padding: EdgeInsets.only(top: 150.0),
                            child: Align(
                              alignment: Alignment.center,
                              child: Text(
                                "Sign Up",
                                style: Styles.semiBoldHeadingThree(
                                    color: _signUpColor),
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 200.0),
                    child: FadeTransition(
                      opacity: _fadeTransition,
                      child: _currentUI,
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class BottomWaveClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = new Path();
    path.lineTo(0.0, size.height - 25);

    var firstControlPoint = Offset(size.width / 4, size.height + 20);
    var firstEndPoint = Offset(size.width / 2, size.height*.88);
    path.quadraticBezierTo(firstControlPoint.dx, firstControlPoint.dy,
        firstEndPoint.dx, firstEndPoint.dy);

    var secondControlPoint =
        Offset(size.width - (size.width * .25), size.height - 72);
    var secondEndPoint = Offset(size.width, size.height - 20);
    path.quadraticBezierTo(secondControlPoint.dx, secondControlPoint.dy,
        secondEndPoint.dx, secondEndPoint.dy);

    path.lineTo(size.width, size.height - 20);
    path.lineTo(size.width, 0.0);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}

class WaveClipperOne extends CustomClipper<Path> {
  bool reverse;

  WaveClipperOne({this.reverse = false});

  @override
  Path getClip(Size size) {
    if (!reverse) {
      var firstControlPoint = Offset(size.width / 4, size.height *.64);
      var firstEndPoint = Offset(size.width / 2, size.height * .87);
      var secondControlPoint = Offset(size.width * .80, size.height*1.13);
      var secondEndPoint = Offset(size.width, size.height - 30);

      final path = Path()
        ..lineTo(0.0, size.height - 20)
        ..quadraticBezierTo(firstControlPoint.dx, firstControlPoint.dy,
            firstEndPoint.dx, firstEndPoint.dy)
        ..quadraticBezierTo(secondControlPoint.dx, secondControlPoint.dy,
            secondEndPoint.dx, secondEndPoint.dy)
        ..lineTo(size.width, 0.0)
        ..close();
      return path;
    } else {
      Offset firstEndPoint = Offset(size.width * .5, 20);
      Offset firstControlPoint = Offset(size.width * .25, 30);
      Offset secondEndPoint = Offset(size.width, 50);
      Offset secondControlPoint = Offset(size.width * .75, 10);

      final path = Path()
        ..quadraticBezierTo(firstControlPoint.dx, firstControlPoint.dy,
            firstEndPoint.dx, firstEndPoint.dy)
        ..quadraticBezierTo(secondControlPoint.dx, secondControlPoint.dy,
            secondEndPoint.dx, secondEndPoint.dy)
        ..lineTo(size.width, size.height)
        ..lineTo(0.0, size.height)
        ..close();
      return path;
    }
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}
