import 'package:auto_crysto/ui/apiresponseui/Loading.dart';
import 'package:auto_crysto/ui/apiresponseui/OnError.dart';
import 'package:auto_crysto/ui/data/ApiHelper/ApiResponse.dart';
import 'package:auto_crysto/ui/data/ApiHelper/Repo/AppRepo.dart';
import 'dart:async';

import 'package:auto_crysto/ui/data/models/LoginModel.dart';
import 'package:auto_crysto/ui/utils/Styles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';

class LoginBloc{
  AppRepo appRepo;
  StreamController loginController;
  StreamSink<ApiResponse<LoginModel>> get loginStreamSink=>loginController.sink;
  Stream<ApiResponse<LoginModel>> get loginStream=>loginController.stream;
  LoginBloc(){
    loginController=StreamController<ApiResponse<LoginModel>>();
    appRepo=AppRepo();
  }
  Future<dynamic> login(Map<String,dynamic> body)async{
    loginStreamSink.add(ApiResponse.loading("Signing In"));
    try{
      var loginModel = await appRepo.login(body);
      loginStreamSink.add(ApiResponse.completed(loginModel));
    }catch(e){
      loginStreamSink.add(ApiResponse.error(e.toString()));
    }
  }
  Future<dynamic> loginWithFB(Map<String,dynamic> body)async{
    loginStreamSink.add(ApiResponse.loading("Signing In"));
    try{
      var loginModel = await appRepo.loginWithFB(body);
      loginStreamSink.add(ApiResponse.completed(loginModel));
    }catch(e){
      loginStreamSink.add(ApiResponse.error(e.toString()));
    }
  }
  Future<dynamic> loginWithGoogle(Map<String,dynamic> body)async{
    loginStreamSink.add(ApiResponse.loading("Signing In"));
    try{
      var loginModel = await appRepo.loginWithGoogle(body);
      loginStreamSink.add(ApiResponse.completed(loginModel));
    }catch(e){
      loginStreamSink.add(ApiResponse.error(e.toString()));
    }
  }

  void forgetPassword(BuildContext context,String emailID) async{
    showDialog(context: context,builder: (_)=>onLoading(colors: Colors.black26));
    try{
    var response=await appRepo.forgetPassword(emailID);
    Navigator.of(context,rootNavigator: true).pop();
    showDialog(context: context,builder: (_)=>PlatformAlertDialog(
      title: Text("Success",style: Styles.semiBoldHeadingTwo(),),
      actions: <Widget>[
        FlatButton(onPressed: (){
          Navigator.of(context,rootNavigator: true).pop();
          Navigator.of(context,rootNavigator: true).pop();
        }, child: Text("Ok",style: Styles.semiBoldHeadingFour(),))
      ],
      content: Text(response.message,style: Styles.semiBoldHeadingThree(),),));
    } catch(e){
      Navigator.of(context,rootNavigator: true).pop();
      showDialog(context: context,builder: (_)=>OnError(errorMessage: e.toString(),onRetryPressed: (){
        Navigator.of(context,rootNavigator: true).pop();
        forgetPassword(context, emailID);
      },));
    }


  }
  void dispose(){
    loginController.close();
  }

}