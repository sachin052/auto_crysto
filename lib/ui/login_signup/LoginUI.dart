import 'package:auto_crysto/main.dart';
import 'package:auto_crysto/routes/Router.dart';
import 'package:auto_crysto/ui/commonui/CustomMaterialButton.dart';
import 'package:auto_crysto/ui/data/ApiHelper/ApiResponse.dart';
import 'package:auto_crysto/ui/data/ApiHelper/Repo/AppRepo.dart';
import 'package:auto_crysto/ui/data/sharedPrefHelper/SharedPrefHelper.dart';
import 'package:auto_crysto/ui/login_signup/LoginBloc.dart';
import 'package:auto_crysto/ui/login_signup/SignUpUI.dart';
import 'package:auto_crysto/ui/utils/AppColors.dart';
import 'package:auto_crysto/ui/utils/Styles.dart';
import 'package:auto_crysto/ui/utils/Utils.dart';
import 'package:auto_crysto/ui/utils/Validators.dart';
import 'package:auto_crysto/ui/walk_through/WalkThrough.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_sign_in/google_sign_in.dart';

LoginBloc loginBloc;
String email;
String pass;

class LoginUI extends StatefulWidget {
  @override
  _LoginUIState createState() => _LoginUIState();
}

class _LoginUIState extends State<LoginUI> with SingleTickerProviderStateMixin {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  AnimationController _animationController;
  Animation<Offset> _animation;
  String _emailError;
  String _passError;
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passController = TextEditingController();
  bool isLoggedIn = false;
  GoogleSignIn _googleSignIn;

  @override
  void dispose() {
    // TODO: implement dispose
    _animationController.dispose();
    loginBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SlideTransition(
      position: _animation,
      child: SingleChildScrollView(
        child: Form(
          key: _formKey,
          autovalidate: false,
          child: Container(
            color: Colors.transparent,
            child: Padding(
              padding:
                  const EdgeInsets.only(left: 30.0, right: 30.0, top: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  EmailWidget(widget.key, _emailError, _emailController),
                  SizedBox(
                    height: 10,
                  ),
                  PasswordWidget(widget.key, _passError, _passController),
                  GestureDetector(
                    onTap: () {
                      Utils.showAnimatedDialog(context, ForgetDialog());
                    },
                    child: Align(
                      alignment: Alignment.topRight,
                      child: Padding(
                          padding: EdgeInsets.only(top: 15.0, right: 10.0),
                          child: Text("Forgot Password?",
                              style: TextStyle(
                                  decoration: TextDecoration.underline,
                                  color: Colors.black,
                                  fontSize: 14.0))),
                    ),
                  ),
                  SizedBox(
                    height: 30.0,
                  ),
                  CustomMaterialButton(widget.key, () {
                    login();
                  }, "Sign in"),
//                  buildLoginButton(),
                  SizedBox(
                    height: 30.0,
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      "---------- Or Connect with ----------",
                      style: TextStyle(color: Colors.grey),
                    ),
                  ),
                  SizedBox(
                    height: 30.0,
                  ),
                  buildGoogleButton(),
                  SizedBox(
                    height: 15.0,
                  ),
                  buildFBButton(),
                  SizedBox(
                    height: 25.0,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _animationController = new AnimationController(
        vsync: this, duration: new Duration(milliseconds: 500));
    _animation = Tween<Offset>(begin: Offset(0.0, .1), end: Offset(0, 0))
        .animate(_animationController);
    _animationController.forward();
    _emailController.addListener(setEmailError);
    _passController.addListener(setPasswordError);
    initGoogleLogin();
    loginBloc = LoginBloc();
    _loginListener();
  }

  Material buildLoginButton() {
    return Material(
      elevation: 10.0,
      borderRadius: BorderRadius.circular(30.0),
      color: AppColors.colorPrimary,
      child: Container(
        height: 50,
        width: MediaQuery.of(context).size.width - 40,
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(30.0)),
        child: InkWell(
          onTap: () {
            login();
          },
          borderRadius: BorderRadius.circular(30.0),
          splashColor: Colors.white.withOpacity(.2),
          child: Container(
            color: Colors.transparent,
            child: Center(
              child: Text(
                "Sign In",
                style: Styles.semiBoldHeadingThree(color: Colors.white),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Material buildFBButton() {
    return Material(
      elevation: 5.0,
      shadowColor: Color(0xFF3b5998).withOpacity(.50),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
      child: InkWell(
        onTap: () {
          initiateFacebookLogin();
        },
        borderRadius: BorderRadius.circular(30.0),
        splashColor: Colors.blue.withOpacity(.2),
        child: Container(
          height: 50,
          width: MediaQuery.of(context).size.width - 80,
          child: Center(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(FontAwesomeIcons.facebookF,
                      size: 18.0, color: Color(0xFF3b5998)),
                  SizedBox(
                    width: 10,
                  ),
                  Align(
                    alignment: Alignment(0, 0),
                    child: Text(
                      "Continue with Facebook",
                      style:
                          Styles.semiBoldHeadingFour(color: Color(0xFF3b5998)),
                    ),
                  )
                ],
              ),
            ),
          ),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30.0),
              border: Border.all(
                color: Color(0xFF3b5998),
                width: 2.0,
                style: BorderStyle.solid,
              )),
        ),
      ),
    );
  }

  Material buildGoogleButton() {
    return Material(
      color: Colors.red,
      elevation: 5.0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
      child: InkWell(
        splashColor: Colors.white,
        child: Container(
          height: 50,
          width: MediaQuery.of(context).size.width - 80,
          child: Center(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    FontAwesomeIcons.google,
                    size: 17.0,
                    color: Colors.white,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Continue with Google",
                    style: Styles.semiBoldHeadingFour(color: Colors.white),
                  )
                ],
              ),
            ),
          ),
          decoration: BoxDecoration(borderRadius: BorderRadius.circular(30.0)),
        ),
        onTap: () {
          _handleSignIn();
        },
        borderRadius: BorderRadius.circular(30.0),
      ),
    );
  }

  void initGoogleLogin() {
    _googleSignIn = GoogleSignIn(
      scopes: [
        'profile',
        'email',
      ],
    );
  }

  void onLoginStatusChanged(bool isLoggedIn) {
    setState(() {
      this.isLoggedIn = isLoggedIn;
    });
  }

  void initiateFacebookLogin() async {
    var facebookLogin = FacebookLogin();
    var facebookLoginResult =
        await facebookLogin.logIn(['email']);
    switch (facebookLoginResult.status) {
      case FacebookLoginStatus.error:
        print("Error");
        onLoginStatusChanged(false);
        break;
      case FacebookLoginStatus.cancelledByUser:
        print("CancelledByUser");
        onLoginStatusChanged(false);
        break;
      case FacebookLoginStatus.loggedIn:
        print("LoggedIn");
        Map<String, dynamic> body = {
          "token": facebookLoginResult.accessToken.token
        };
        loginBloc.loginWithFB(body);
        onLoginStatusChanged(true);
        break;
    }
  }

  setEmailError() {
    if (!EmailValidator.validate(_emailController.text) &&
        _emailController.text.isNotEmpty) {
      setState(() {
        _emailError = "Invalid Email id";
      });
    } else {
      setState(() {
        _emailError = null;
      });
    }
    print(_emailController.text);
  }

  setPasswordError() {
    if (_passController.text.length < 6 && _passController.text.isNotEmpty) {
      setState(() {
        _passError = "Req min 6 character";
      });
    } else {
      setState(() {
        _passError = null;
      });
    }
  }

  void login() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      Map<String, String> body = {"email": email, "password": pass};
      loginBloc.login(body);
    }
  }

  _loginListener() {
    loginBloc.loginStream.listen(
        (data) async {
          switch (data.status) {
            case Status.LOADING:
              _onLoading(data.message);
              break;
            case Status.COMPLETED:
              Scaffold.of(context).hideCurrentSnackBar();
              var userData = await SharedPrefHelper.getUserData();
              print(userData.email);
              Scaffold.of(context)
                  .showSnackBar(SnackBar(content: Text(userData.email)));
              locator<SignUpUI>().clearAllData();
              if (data.data.purchaseDetails.planType == "None") {
                var appRepo = AppRepo();
                await appRepo.activeIAPTrial();
              }
              if (SharedPrefHelper.isAlreadyLoggedIn(
                  data.data.userDetails.email)) {
                Navigator.pushReplacementNamed(context, Router.home);
              } else {
                SharedPrefHelper.ifFirstTimeUser(data.data.userDetails.email);
                Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                        builder: (context) => WalkThrough(
                              fromMenu: false,
                            )));
              }
              break;
            case Status.ERROR:
              print(data.message);
              Utils.showShowSnackBar(context, data.message, true);
              break;
          }
        },
        onDone: () {},
        onError: (error) {
          print(error);
        });
  }

  Widget _onLoading(String text) {
    Scaffold.of(context).showSnackBar(SnackBar(
      content: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          CircularProgressIndicator(
              backgroundColor: Colors.white,
              valueColor:
                  AlwaysStoppedAnimation<Color>(AppColors.colorPrimary)),
          SizedBox(
            width: 20.0,
          ),
          Text(
            text,
            style: TextStyle(color: Colors.white, fontSize: 16.0),
          ),
        ],
      ),
      backgroundColor: AppColors.colorPrimary,
    ));
  }

  Future _handleSignIn() async {
    try {
      _onLoading("Logging in Using Google");
      await _googleSignIn.signIn().then((result) {
        result.authentication.then((googleKey) {
          print(googleKey.idToken);
          Map<String, dynamic> body = {"token": googleKey.idToken};
          loginBloc.loginWithGoogle(body);
          Scaffold.of(context).hideCurrentSnackBar();
        }).catchError((err) {
          print('inner error');
          Scaffold.of(context).hideCurrentSnackBar();
        });
      }).catchError((err) {
        print('error occured');
        Scaffold.of(context).hideCurrentSnackBar();
      });
    } catch (error) {
      print(error);
    }
  }
}

class EmailWidget extends StatelessWidget {
  final String errorText;
  final TextEditingController emailController;

  const EmailWidget(Key key, this.errorText, this.emailController)
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var defTextStyle =
        Styles.semiBoldHeadingFour(color: Colors.black.withOpacity(.5));
    var style = Styles.semiBoldHeadingFourGrey();
    var errorStyle = Styles.semiBoldHeadingFive(color: Colors.red);
    var border = OutlineInputBorder(
        borderSide: BorderSide(color: AppColors.colorPrimary, width: 1.0));
    return TextFormField(
      controller: emailController,
      cursorColor: AppColors.colorPrimary,
      onSaved: (value) => email = value,
      validator: (value) {
        if (value.isEmpty) {
          return "Email must not be empty";
        } else if (!EmailValidator.validate(value)) {
          return "Invalid email id";
        } else {
          return null;
        }
      },
      keyboardType: TextInputType.emailAddress,
      decoration: Styles.getOutlineBorder('Email', errorText),
    );
  }
}

class PasswordWidget extends StatelessWidget {
  final String errorText;
  final TextEditingController passController;

  const PasswordWidget(Key key, this.errorText, this.passController)
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Styles.semiBoldHeadingFour(color: Colors.black.withOpacity(.5));
    var style = Styles.semiBoldHeadingFourGrey();
    var errorStyle = Styles.semiBoldHeadingFive(color: Colors.red);
    var border = OutlineInputBorder(
        borderSide: BorderSide(color: AppColors.colorPrimary, width: 1.0));
    return TextFormField(
      controller: passController,
      obscureText: true,
      onSaved: (value) => pass = value,
      validator: (value) {
        if (value.isEmpty) {
          return "Password must not be empty";
        } else if (value.length < 6) {
          return "Min password length is 6";
        } else {
          return null;
        }
      },
      keyboardType: TextInputType.text,
      decoration: Styles.getOutlineBorder("Password", errorText),

//      style: Styles.semiBoldHeadingFour(color: Colors.black.withOpacity(.5)),
      cursorColor: AppColors.colorPrimary,
    );
  }
}

class ForgetDialog extends StatefulWidget {
  @override
  _ForgetDialogState createState() => _ForgetDialogState();
}

class _ForgetDialogState extends State<ForgetDialog> {
  var defTextStyle =
      Styles.semiBoldHeadingFour(color: Colors.black.withOpacity(.5));
  var style = Styles.semiBoldHeadingFourGrey();
  var errorStyle = Styles.semiBoldHeadingFive(color: Colors.red);
  var border = OutlineInputBorder(
      borderSide: BorderSide(color: AppColors.colorPrimary, width: 1.0));
  String emailID;
  String _emailError;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      child: Material(
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        child: Container(
          decoration: BoxDecoration(borderRadius: BorderRadius.circular(10.0)),
          padding: EdgeInsets.all(12.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.lock,
                color: AppColors.colorPrimary,
                size: 80.0,
              ),
              Text(
                'Please enter your registered email address to reset your password',
                style: Styles.semiBoldHeadingFour(),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 10.0),
              TextField(
                onChanged: (value) {
                  this.setState(() {
                    emailID = value;
                    if (value.length == 0) {
                      _emailError = "Email must not be empty";
                    } else if (!EmailValidator.validate(value)) {
                      _emailError = "Invalid Email id";
                    } else {
                      _emailError = null;
                    }
                  });
                },
                decoration:
                    Styles.getOutlineBorder("Enter your email", _emailError),
                cursorColor: AppColors.colorPrimary,
              ),
              SizedBox(height: 10.0),
              CustomMaterialButton(widget.key, () {
                if (emailID != null &&
                    _emailError == null &&
                    emailID.isNotEmpty &&
                    EmailValidator.validate(emailID)) {
                  loginBloc.forgetPassword(context, emailID);
                } else {
                  setEmaiError(emailID);
                }
              }, "Reset Password")
            ],
          ),
        ),
      ),
    );
  }

  setEmaiError(String value) {
    setState(() {
      if (value == null) {
        _emailError = "Email must not be empty";
      } else if (value.length == 0) {
        _emailError = "Email must not be empty";
      } else if (!EmailValidator.validate(value)) {
        _emailError = "Invalid Email id";
      } else {
        _emailError = null;
      }
    });
  }
}
