import 'package:auto_crysto/ui/data/models/States.dart';
import 'package:auto_crysto/ui/utils/AppColors.dart';
import 'package:auto_crysto/ui/utils/Utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
typedef dynamic MyCallBack(States data);

getIosPicker(BuildContext context,List<States> data,MyCallBack callBack,String title){
  if(data.isNotEmpty){
    callBack(data[0]);
    showCupertinoModalPopup(
        context: context,
        builder: (context) {
          return buildPicker(context,data,callBack,title);
        });
  }
}
Widget buildPicker(BuildContext context,List<States> data,MyCallBack callBack,String title) {
  final FixedExtentScrollController scrollController =
  FixedExtentScrollController(initialItem: 0);
  return Container(
    height: Utils().getScreenHeight(context) * .35,
    width: Utils().getScreenWidth(context),
    child: Column(
      children: <Widget>[
        Material(
          child: Container(
              width: Utils().getScreenWidth(context),
              padding: EdgeInsets.only(top:12.0),
              color: Colors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  GestureDetector(
                      onTap: (){
                        Navigator.pop(context);
                      },
                      child: Text("Cancel",style: TextStyle(color: Colors.red,fontSize: 16.0),)),
                  Text(title,style: TextStyle(fontSize: 16.0),),
                  GestureDetector(
                      onTap: (){
                        Navigator.pop(context);
                      },
                      child: Text("Done",style: TextStyle(color: AppColors.colorPrimary,fontSize: 16.0),))
                ],)),
        ),
        Expanded(
          child: CupertinoPicker(
            itemExtent: 35.0,
            onSelectedItemChanged: (index) {
            callBack(data[index]);
            },
            children: data != null
                ? List<Widget>.generate(data.length, (index) {
              return Center(
                child: Text(data[index].title),
              );
            })
                : List<Widget>.generate(1, (context) {
                  return CircularProgressIndicator();
            }),
            backgroundColor: CupertinoColors.white,
            useMagnifier: true,
          ),
        ),
      ],
    ),
  );
}