
import 'package:auto_crysto/ui/utils/AppColors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget onLoading({Color colors:Colors.white}) {
  return Container(
    color: colors,
    child: Align(
      alignment: Alignment.center,
      child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(Colors.white),backgroundColor: AppColors.colorPrimary,semanticsLabel: "Loading",semanticsValue: "value",),
    ),
  );
}
