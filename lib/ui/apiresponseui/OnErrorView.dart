import 'package:auto_crysto/ui/utils/AppColors.dart';
import 'package:auto_crysto/ui/utils/Styles.dart';
import 'package:flutter/material.dart';

class OnErrorView extends StatefulWidget {
  final String errorMsg;
  final VoidCallback onButtonPressed;

  const OnErrorView({Key key, @required this.errorMsg,@required this.onButtonPressed}) : super(key: key);

  @override
  _OnErrorViewState createState() => _OnErrorViewState();
}

class _OnErrorViewState extends State<OnErrorView> {
  @override
  Widget build(BuildContext context) {
    return Container(

      child: Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Oops",
                style: Styles.semiBoldHeadingTwo(color: AppColors.colorPrimary),
              ),
              SizedBox(
                height: 10.0,
              ),
              Text(
                widget.errorMsg,
                style:
                    Styles.semiBoldHeadingFour(color: AppColors.colorPrimary),
              ),
              SizedBox(
                height: 10.0,
              ),
              FlatButton(
                onPressed:widget.onButtonPressed
                ,
                child: Text(
                  "Retry",
                  style: Styles.semiBoldHeadingFive(color: Colors.white),
                ),
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
                color: AppColors.colorPrimary,
              )
            ],
          ),
        ),
      ),
    );
  }
}
