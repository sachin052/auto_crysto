
import 'package:auto_crysto/ui/utils/AppColors.dart';
import 'package:auto_crysto/ui/utils/Images.dart';
import 'package:auto_crysto/ui/utils/Styles.dart';
import 'package:flutter/material.dart';
class OnError extends StatelessWidget {
  final String errorMessage;
final Color color;
  final Function onRetryPressed;
  final String btnText;

  const OnError({Key key, this.errorMessage, this.onRetryPressed, this.btnText, this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Center(
        child: Container(
          width: MediaQuery.of(context).size.width,
          color: color!=null?color:AppColors.colorPrimary,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset(errorMessage.contains("internet")?Images.noInternet:Images.error,height: MediaQuery.of(context).size.width/2,),
              Text("Oops!",style: TextStyle(color: Colors.white,fontSize: 20.0,fontWeight: FontWeight.bold),),
              Padding(
                padding: const EdgeInsets.all(18.0),
                child: Text(
                  errorMessage,
                  textAlign: TextAlign.center,
                  style: Styles.semiBoldHeadingFour(color:Colors.white),
                ),
              ),
              SizedBox(height: 8),
              RaisedButton(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0),side: BorderSide(color: Colors.white,width: 2.0)),
                color: AppColors.colorPrimary,
                child: Text(btnText!=null?btnText:'Retry', style: Styles.semiBoldHeadingFive(color: Colors.white)),
                onPressed: onRetryPressed,
              )
            ],
          ),
        ),
      ),
    );
  }
}