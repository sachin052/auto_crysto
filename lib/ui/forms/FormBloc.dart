import 'dart:async';

import 'package:auto_crysto/ui/apiresponseui/OnError.dart';
import 'package:auto_crysto/ui/data/ApiHelper/ApiResponse.dart';
import 'package:auto_crysto/ui/data/ApiHelper/Repo/AppRepo.dart';
import 'package:auto_crysto/ui/data/models/Coin.dart';
import 'package:auto_crysto/ui/data/models/CommonApiResponse.dart';
import 'package:auto_crysto/ui/data/models/FormData.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FormBloc {
  AppRepo appRepo;
  // User for fetching list of coins over the server
  StreamController<ApiResponse<List<Coins>>> streamController;

  StreamSink<ApiResponse<List<Coins>>> get coinsSink => streamController.sink;

  Stream<ApiResponse<List<Coins>>> get coinStream => streamController.stream;

  // User for sending data to server

  StreamController<ApiResponse<CommonApiResponse>> formController;

  StreamSink<ApiResponse<CommonApiResponse>> get formSink => formController.sink;

  Stream<ApiResponse<CommonApiResponse>> get formStream => formController.stream;

  StreamController<String> currentPlanController;
  StreamSink<String> get planSink=>currentPlanController.sink;
 Stream<String> get planStream=>currentPlanController.stream;

  FormBloc() {
    appRepo = AppRepo();
    currentPlanController=StreamController<String>.broadcast();
    formController=StreamController<ApiResponse<CommonApiResponse>>();
    streamController = StreamController<ApiResponse<List<Coins>>>();
  }

  getCoinsList() async {
    coinsSink.add(ApiResponse.loading("Fetching Coins"));
    try {
      var response = await appRepo.getCoinsList();
      coinsSink.add(ApiResponse.completed(response));
    } catch (e) {
      coinsSink.add(ApiResponse.error(e.toString()));
    }
  }

  void dispose() {
    currentPlanController.close();
    streamController.close();
    formController.close();
  }

  postFormData(BuildContext context,String planID,FormData formData) async {
    formSink.add(ApiResponse.loading("Saving Form"));
    try {
     var response= planID==null?await appRepo.postFormData(formData.toMap()):await appRepo.updateFormData(planID,formData.toMap());
      formSink.add(ApiResponse.completed(response));
    } catch (ex) {
      Future.delayed(Duration(milliseconds: 500),(){
        Navigator.push(context, CupertinoPageRoute(builder: (c)=>Scaffold(body: OnError(
          errorMessage: ex.toString(),
          onRetryPressed: () {
            Navigator.pop(context);
          },
        )),));
      });
      formSink.add(ApiResponse.error(ex.toString()));
    }
  }
}
