import 'package:auto_crysto/ui/apiresponseui/Loading.dart';
import 'package:auto_crysto/ui/apiresponseui/OnError.dart';
import 'package:auto_crysto/ui/apiresponseui/OnErrorView.dart';
import 'package:auto_crysto/ui/commonui/CustomMaterialButton.dart';
import 'package:auto_crysto/ui/commonui/UpdateFormDialog.dart';
import 'package:auto_crysto/ui/data/ApiHelper/ApiResponse.dart';
import 'package:auto_crysto/ui/data/models/Coin.dart';
import 'package:auto_crysto/ui/data/models/CommonApiResponse.dart';
import 'package:auto_crysto/ui/data/models/FormData.dart';
import 'package:auto_crysto/ui/data/models/UserPlans.dart';
import 'package:auto_crysto/ui/forms/FormBloc.dart';
import 'package:auto_crysto/ui/home/HomeBloc.dart';
import 'package:auto_crysto/ui/utils/AppColors.dart';
import 'package:auto_crysto/ui/utils/DecimalTextInputFormatter.dart';
import 'package:auto_crysto/ui/utils/Styles.dart';
import 'package:auto_crysto/ui/utils/Utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';

class CommonForm extends StatefulWidget {
  final String title;
  final String userExchangeID;
  final Plan currentUserPlan;
  final HomBloc homBloc;
  final selectedPlanTitle;

  const CommonForm(Key key, this.title, this.userExchangeID,
      {this.currentUserPlan, this.homBloc, this.selectedPlanTitle})
      : super(key: key);

  @override
  _CommonFormState createState() => _CommonFormState(
      userExchangeID, title, this.homBloc, this.selectedPlanTitle);
}

class _CommonFormState extends State<CommonForm> {
  final String userExchangeID;
  final String title;
  final selectedPlanTitle;

  var currentOpacity=0.0;

  _CommonFormState(
      this.userExchangeID, this.title, this.homBloc, this.selectedPlanTitle);

  final HomBloc homBloc;
  FormBloc formBloc;
  List<Coins> coinsList;
  bool visibileBottom = false;
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  StreamBuilder streamBuilder;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    formBloc = FormBloc();
    coinsList = List<Coins>();
//    bindListeners();
    fetchList();
    streamBuilder = StreamBuilder<ApiResponse<CommonApiResponse>>(
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          switch (snapshot.data.status) {
            case Status.LOADING:
              return onLoading(colors: AppColors.colorPrimary.withOpacity(.2));
              break;
            case Status.COMPLETED:
              Fluttertoast.showToast(
                  msg: snapshot.data.data.message,
                  backgroundColor: AppColors.colorPrimaryGradient,
                  textColor: Colors.white);
              widget.homBloc.refreshSink.add("Plans");
              Navigator.pop(context);
//            Utils.showShowSnackBarWithKey(key, snapshot.data.data.message, false);
              return Container();
              break;
            case Status.ERROR:
          return Container();
              break;
            default:
              return Container();
          }
        } else {
          return Container();
        }
      },
      stream: formBloc.formStream,
    );
  }

  fetchList() async {
    await formBloc.getCoinsList();
  }

  @override
  Widget build(BuildContext context) {
    return MediaQuery(
      data: MediaQuery.of(context).size.width>600?MediaQuery.of(context).copyWith(textScaleFactor: 1.2):MediaQuery.of(context),
      child: Scaffold(
        key: key,
        appBar: buildAppBar(context),
        body: Stack(
          children: <Widget>[
            StreamBuilder<ApiResponse<List<Coins>>>(
              stream: formBloc.coinStream,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  switch (snapshot.data.status) {
                    case Status.LOADING:
                      return onLoading();
                      break;
                    case Status.COMPLETED:
                      coinsList = snapshot.data.data;
                      return InputForm(
                        widget.key,
                        coinsList,
                        formBloc,
                        userExchangeID,
                        title,
                        selectedPlan: widget.currentUserPlan,
                        selectedPlanTitle: selectedPlanTitle,
                      );
                      break;
                    case Status.ERROR:
                      return OnErrorView(
                        errorMsg: snapshot.data.message,
                        onButtonPressed: () async {
                          await formBloc.getCoinsList();
                        },
                      );
                      break;
                    default:
                      return OnErrorView(
                        errorMsg: "Something went wrong",
                        onButtonPressed: () {
                          return InputForm(
                            widget.key,
                            coinsList,
                            formBloc,
                            userExchangeID,
                            title,
                            selectedPlan: widget.currentUserPlan,
                            selectedPlanTitle: selectedPlanTitle,
                          );
                        },
                      );
                  }
                } else {
                  return Container();
                }
              },
            ),
            streamBuilder,
          ],
        ),
      ),
    );
  }

  PreferredSize buildAppBar(BuildContext context) {
    return PreferredSize(
        child: Material(
          elevation: 0.0,
          child: AppBar(
            flexibleSpace: Container(
              decoration: BoxDecoration(
               color: AppColors.colorPrimary
              ),
            ),
            title: Hero(
              tag: widget.title,
              child: Material(
                type: MaterialType.transparency,
                child: Text(
                  widget.title,
                  style: Styles.boldHeadingTwo(colors: Colors.white),
                ),
              ),
            ),
            centerTitle: true,
          ),
        ),
        preferredSize: Size(MediaQuery.of(context).size.width, 56.0));
  }
}

class InputForm extends StatefulWidget {
  final List<Coins> coinsList;
  final FormBloc formBloc;
  final String userExchangeID;
  final String title;
  final Plan selectedPlan;
  final String selectedPlanTitle;
  const InputForm(
      Key key, this.coinsList, this.formBloc, this.userExchangeID, this.title,
      {this.selectedPlan, this.selectedPlanTitle})
      : super(key: key);

  @override
  _InputFormState createState() => _InputFormState();
}

class _InputFormState extends State<InputForm>
    with SingleTickerProviderStateMixin {
  FocusNode paidFocusNode = FocusNode();
  FocusNode amountFocusNode = FocusNode();
  FocusNode perBuyFocusNode = FocusNode();
  FocusNode numPosFocusNode = FocusNode();
  FocusNode buyTriggerFocusNode = FocusNode();
  FocusNode sellTriggerFocusNode = FocusNode();
  FocusNode sellTimeFocusNode = FocusNode();
  String coinNameError;
  String pairNameError;
  String tradingAmtError;
  String tradingAmtPerBuyError;
  String numPosError;
  String buyTriggerError;
  String sellTriggerError;
  String sellTimeError;
  String coinID;
  String pairedCoinID;
  String tradingAmount;
  String amountPerBuy;
  String numPos;
  String buyThreshold;
  String sellThreshold;
  String sellTimeLimit;
  RegExp validPercentage =
      RegExp("^\$|^(0|([1-9][0-9]{0,3}))(\\.[0-9]{0,2})?\$");
  bool _value = false;
  var style = Styles.semiBoldHeadingFourGrey();
  var errorStyle = Styles.semiBoldHeadingFive(color: Colors.red);
  var defTextStyle =
      Styles.semiBoldHeadingFour(color: Colors.black.withOpacity(.5));
  var border = OutlineInputBorder(
      borderSide: BorderSide(color: AppColors.colorPrimary, width: 1.0));
  var keyboard = TextInputType.number;
  TextEditingController _coinNameController = TextEditingController();
  TextEditingController _pairedCoinController = TextEditingController();
  TextEditingController _tradingAmountController = TextEditingController();
  TextEditingController _tradingAmountPerBuyController =
      TextEditingController();
  TextEditingController _numPosController = TextEditingController();
  TextEditingController _buyTriggerController = TextEditingController();
  TextEditingController _sellTriggerController = TextEditingController();
  TextEditingController _sellTimeController = TextEditingController();
  String selectedPlanTitle;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    paidFocusNode.addListener(() {
      setState(() {});
    });
    _numPosController.text="1";
    numPos="1";
    if (widget.selectedPlan != null) {
      var currentPlan = widget.selectedPlan;
      _coinNameController.text = widget.selectedPlan.coin.name;
      coinID = currentPlan.coin.id.toString();
      pairedCoinID = currentPlan.coin.id.toString();
      _pairedCoinController.text = widget.selectedPlan.coin.pair;
      _tradingAmountController.text =
          widget.selectedPlan.tradingAmount.toString();
      tradingAmount = currentPlan.tradingAmount.toString();
      amountPerBuy = currentPlan.amountPerBuy;
//      _tradingAmountPerBuyController.text = (double.parse(calculateAmountPerBuy())*double.parse(widget.coinsList[0].price)).toStringAsFixed(3);
      _tradingAmountPerBuyController.text = widget.selectedPlan.amountPerBuy;
      amountPerBuy=_tradingAmountController.text;
      _numPosController.text = widget.selectedPlan.numberOfPosition.toString();
      numPos = currentPlan.numberOfPosition.toString();
      _buyTriggerController.text = widget.selectedPlan.buyThreshold;
      buyThreshold = currentPlan.buyThreshold;
      _sellTriggerController.text = widget.selectedPlan.sellThreshold;
      sellThreshold = currentPlan.sellThreshold;
      _sellTimeController.text = widget.selectedPlan.buyTimeScale.toString();
      sellTimeLimit = currentPlan.buyTimeScale.toString();
      _value = currentPlan.isDefault ?? false;
    }
    listenCurrentPlanStream();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        padding: EdgeInsets.only(top: 4.0, left: 16.0, right: 16.0),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 15,
              ),
              TextField(
                onChanged: (value){
                  if(value.isNotEmpty){
                    coinNameError=null;
                  }
                },
                onTap: () async {
                  if (widget.coinsList.isEmpty) {
                    return;
                  }
                  await showCupertinoModalPopup(
                      context: context,
                      builder: (context) {
                        return showCoinsList(context);
                      });
                },
                controller: _coinNameController,
                readOnly: true,
                style: defTextStyle,
                decoration: InputDecoration(
                    enabledBorder:
                        coinID != null && coinID.isNotEmpty ? border : null,
                    errorStyle: errorStyle,
                    errorText: coinNameError,
                    labelText: "Coin name",
                    labelStyle:style,
                    suffixIcon: Icon(Icons.arrow_drop_down),
                    border: border),
              ),
              SizedBox(
                height: 15.0,
              ),
              TextField(
                onChanged: (value){
                  if(value.isNotEmpty){
                    pairNameError=null;
                  }
                },
                readOnly: true,
                style: defTextStyle,
                onTap: () async {
                  if (widget.coinsList.isEmpty) {
                    return;
                  }
                  await showCupertinoModalPopup(
                      context: context,
                      builder: (context) {
                        return showPairList(context);
                      });
                },
                controller: _pairedCoinController,
                decoration: InputDecoration(
                    enabledBorder:
                        pairedCoinID != null && pairedCoinID.isNotEmpty
                            ? border
                            : null,
                    errorStyle: errorStyle,
                    errorText: pairNameError,
                    labelStyle:style,
                    labelText: "Paired Coin",
                    focusColor: Colors.black,
                    border: border),
                cursorColor: AppColors.colorPrimary,
              ),
              SizedBox(
                height: 15.0,
              ),
              TextField(
                style: defTextStyle,
                controller: _tradingAmountController,
                onTap: () {
                  requestFocus(amountFocusNode);
                },
                onChanged: (value) {
                  setState(() {
                    tradingAmount = value;
                    if (value.length == 0) {
                      tradingAmtError = "Field Can't be Empty";
                    } else {
                      tradingAmtError = null;
                    }
                  });
                },
                focusNode: amountFocusNode,
//                inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
                keyboardType: TextInputType.numberWithOptions(
                    decimal: true,
                    signed: false),
                decoration: InputDecoration(
                  enabledBorder:
                      tradingAmount != null && tradingAmount.isNotEmpty
                          ? border
                          : null,
                  errorText: tradingAmtError,
                  labelText: "Trading Amount in USD",
                  errorStyle: errorStyle,

                  labelStyle: amountFocusNode.hasFocus
                      ? Styles.semiBoldHeadingFour(
                          color: tradingAmtError != null
                              ? Colors.red
                              : AppColors.colorPrimary)
                      : style,
                  border: border,
                ),
                cursorColor: AppColors.colorPrimary,

              ),
              SizedBox(
                height: 15.0,
              ),
              TextField(
                  controller: _tradingAmountPerBuyController,
                  keyboardType: TextInputType.numberWithOptions(
                      decimal: true,
                      signed: false),
                  style: defTextStyle,
                  onTap: () {
                    requestFocus(perBuyFocusNode);
                  },
                  onChanged: (value) {
                    setState(() {
                      amountPerBuy = value;
                      if (value.length == 0) {
                        tradingAmtPerBuyError = "Field Can't be Empty";
                      } else if (double.parse(calculateAmountPerBuy()) <
                          0.002) {
                        tradingAmtPerBuyError = "Minimum 0.002 BTC must be traded";
                      } else {
                        tradingAmtPerBuyError = null;
                      }
                    });
                  },
                  focusNode: perBuyFocusNode,
                  decoration: InputDecoration(
                      helperStyle: Styles.semiBoldHeadingFive(
                          color: AppColors.colorPrimary),
                      enabledBorder:
                          amountPerBuy != null && amountPerBuy.isNotEmpty
                              ? border
                              : null,
                      errorStyle: errorStyle,
                      errorText: tradingAmtPerBuyError,
                      labelText: 'Trading Amount Per Buy in USD ("\$")',
                      labelStyle: perBuyFocusNode.hasFocus
                          ? Styles.semiBoldHeadingFour(
                              color: tradingAmtPerBuyError != null
                                  ? Colors.red
                                  : AppColors.colorPrimary)
                          : style,
                      border: border),
                  cursorColor: AppColors.colorPrimary,
                 ),
              Visibility(
                visible: amountPerBuy!=null,
                child: SizedBox(
                  height: 5.0,
                ),
              ),
              Visibility(
                  visible: amountPerBuy != null,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 4.0, right: 4.0),
                    child: Text(
                      amountPerBuy != null &&
                              amountPerBuy.isNotEmpty &&
                              double.parse(calculateAmountPerBuy()) > 0.002
                          ? "Currently you've set \$$amountPerBuy which is = ${calculateAmountPerBuy()} BTC. This will vary as price changes. We will consider equivalent bitcoin quantity for trading."
                          : "",
                      style: Styles.semiBoldHeadingFive(
                          color: AppColors.colorPrimary),
                    ),
                  )),
              SizedBox(
                height: 15.0,
              ),
              TextField(
                  readOnly: true,
                  controller: _numPosController,
                  style: defTextStyle,
                  onTap: () {
                    requestFocus(numPosFocusNode);
                  },
                  onChanged: (value) {
                    numPos = value;
                    setState(() {
                      if (value.length == 0) {
                        numPosError = "Field Can't be Empty";
                      } else {
                        numPosError = null;
                      }
                    });
                  },
                  focusNode: numPosFocusNode,
                  decoration: InputDecoration(
                      enabledBorder:
                          numPos != null && numPos.isNotEmpty ? border : null,
                      errorText: numPosError,
                      errorStyle: errorStyle,
                      labelText: 'Enter Number of Positions',
                      labelStyle: numPosFocusNode.hasFocus
                          ? Styles.semiBoldHeadingFour(
                              color: numPosError != null
                                  ? Colors.red
                                  : AppColors.colorPrimary)
                          : style,
                      border: border),
                  cursorColor: AppColors.colorPrimary,
                  keyboardType: keyboard),
              SizedBox(
                height: 15.0,
              ),
              TextField(
                  controller: _buyTriggerController,
                  style: defTextStyle,
                  onTap: () {
                    requestFocus(buyTriggerFocusNode);
                  },
                  onChanged: (value) {
                    buyThreshold = value;
                    setState(() {
                      try {
                        if (value.length == 0) {
                          buyTriggerError = "Field Can't be Empty";
                        } else {
                          var data = double.parse(value);
                          if (data > 100) {
                            buyTriggerError = "Must be smaller than 100.00%";
                          } else {
                            buyTriggerError = null;
                          }
                        }
                      } catch (e) {
                        buyTriggerError = "Invalid Data";
                      }
                    });
                  },
                  focusNode: buyTriggerFocusNode,
                  inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                  decoration: InputDecoration(
                      enabledBorder:
                          buyThreshold != null && buyThreshold.isNotEmpty
                              ? border
                              : null,
                      errorStyle: errorStyle,
                      errorText: buyTriggerError,
                      labelText: 'Enter buy Trigger Threshold in %',
                      labelStyle: buyTriggerFocusNode.hasFocus
                          ? Styles.semiBoldHeadingFour(
                              color: buyTriggerError != null
                                  ? Colors.red
                                  : AppColors.colorPrimary)
                          : style,
                      border: border),
                  cursorColor: AppColors.colorPrimary,
                  keyboardType: keyboard),
              SizedBox(
                height: 15.0,
              ),
              Visibility(
                visible: widget.title.contains("Low") ? false : true,
                child: TextField(
                    controller: _sellTriggerController,
                    style: defTextStyle,
                    onTap: () {
                      requestFocus(sellTriggerFocusNode);
                    },
                    inputFormatters: [
                      DecimalTextInputFormatter(decimalRange: 2)
                    ],
                    onChanged: (value) {
                      sellThreshold = value;
                      setState(() {
                        try {
                          if (value.length == 0) {
                            sellTriggerError = "Field Can't be Empty";
                          } else {
                            var data = double.parse(value);
                            if (data > 100) {
                              sellTriggerError = "Must be smaller than 100.00%";
                            } else {
                              sellTriggerError = null;
                            }
                          }
                        } catch (e) {
                          sellTriggerError = "Invalid Data";
                        }
                      });
                    },
                    focusNode: sellTriggerFocusNode,
                    decoration: InputDecoration(
                        enabledBorder:
                            sellThreshold != null && sellThreshold.isNotEmpty
                                ? border
                                : null,
                        errorStyle: errorStyle,
                        errorText: sellTriggerError,
                        labelText: 'Enter Sell Trigger Threshold in %',
                        labelStyle: sellTriggerFocusNode.hasFocus
                            ? Styles.semiBoldHeadingFour(
                                color: sellTriggerError != null
                                    ? Colors.red
                                    : AppColors.colorPrimary)
                            : style,
                        border: border),
                    cursorColor: AppColors.colorPrimary,
                    keyboardType: keyboard),
              ),
              SizedBox(
                height: 15.0,
              ),
              Visibility(
                visible: widget.title.contains("Low") ? false : true,
                child: TextField(
                    controller: _sellTimeController,
                    style: defTextStyle,
                    onTap: () {
                      requestFocus(sellTimeFocusNode);
                    },
                    inputFormatters: [
                      WhitelistingTextInputFormatter.digitsOnly
                    ],
                    onChanged: (value) {
                      sellTimeLimit = value;
                      setState(() {
                        if (value.length == 0) {
                          sellTimeError = "Field Can't be Empty";
                        } else {
                          sellTimeError = null;
                        }
                      });
                    },
                    focusNode: sellTimeFocusNode,
                    decoration: InputDecoration(
                        enabledBorder:
                            sellTimeLimit != null && sellTimeLimit.isNotEmpty
                                ? border
                                : null,
                        errorStyle: errorStyle,
                        errorText: sellTimeError,
                        labelText: 'Sell Time Limit in mins',
                        labelStyle: sellTimeFocusNode.hasFocus
                            ? Styles.semiBoldHeadingFour(
                                color: sellTimeError != null
                                    ? Colors.red
                                    : AppColors.colorPrimary)
                            : style,
                        border: border),
                    cursorColor: AppColors.colorPrimary,
                    keyboardType: keyboard),
              ),
              SizedBox(
                height: 15.0,
              ),
              Row(
                children: <Widget>[
                  Checkbox(
                    value: _value,
                    onChanged: (value) {
                      setState(() {
                        _value = !_value;
                      });
                    },
                    activeColor: AppColors.colorPrimary,
                  ),
                  SizedBox(
                    width: 10.0,
                  ),
                  Text(
                    "Activate Now",
                    style: Styles.semiBoldHeadingFour(),
                  ),
                ],
              ),
              SizedBox(
                height: 10.0,
              ),
              CustomMaterialButton(widget.key, () {
                saveForm(widget.title,
                    planID: widget.selectedPlan != null
                        ? widget.selectedPlan.id.toString()
                        : null,
                    selectedPlanTitle: widget.selectedPlanTitle);
              }, "Save"),
              SizedBox(
                height: 10.0,
              ),
            ],
          ),
        ),
      ),
    );
  }

  String calculateAmountPerBuy() {
    return (double.parse(amountPerBuy).round() /
            double.parse(widget.coinsList[0].price))
        .toStringAsFixed(3);
  }

  void requestFocus(FocusNode node) {
    setState(() {
      FocusScope.of(context).requestFocus(node);
    });
  }

  Widget showCoinsList(BuildContext context) {
    final FixedExtentScrollController scrollController =
        FixedExtentScrollController(initialItem: 0);
    setInitialValueCoin();
    return Container(
      height: Utils().getScreenHeight(context) * .35,
      width: Utils().getScreenWidth(context),
      child: CupertinoPicker(
        itemExtent: 35.0,
        onSelectedItemChanged: (index) {},
        children: widget.coinsList != null
            ? List<Widget>.generate(widget.coinsList.length, (index) {
                return Center(
                  child: Text(widget.coinsList[index].name),
                );
              })
            : List<Widget>.generate(0, (context) {}),
        backgroundColor: CupertinoColors.white,
        useMagnifier: true,
        scrollController: scrollController,
      ),
    );
  }

  Widget showPairList(BuildContext context) {
    final FixedExtentScrollController scrollController =
        FixedExtentScrollController(initialItem: 0);
    setInitialValuePairedCoin();
    return Container(
      height: Utils().getScreenHeight(context) * .35,
      width: Utils().getScreenWidth(context),
      child: CupertinoPicker(
        itemExtent: 35.0,
        onSelectedItemChanged: (index) {
          setState(() {});
        },
        children: widget.coinsList != null
            ? List<Widget>.generate(widget.coinsList.length, (index) {
                return Center(
                  child: Text(widget.coinsList[index].pair),
                );
              })
            : List<Widget>.generate(0, (context) {}),
        backgroundColor: CupertinoColors.white,
        useMagnifier: true,
        scrollController: scrollController,
      ),
    );
  }

  setInitialValuePairedCoin() {
    _pairedCoinController.text = widget.coinsList[0].pair;
    pairedCoinID = widget.coinsList[0].id;
    pairNameError=null;
  }

  setInitialValueCoin() {
    _coinNameController.text = widget.coinsList[0].name;
    coinID = widget.coinsList[0].id;
    coinNameError=null;
  }

  void saveForm(String title, {String planID, String selectedPlanTitle}) async {
    if (title.contains("Low")) {
      if (isValidField(coinNameError, coinID) &&
          isValidField(pairNameError, pairedCoinID) &&
          isValidField(tradingAmtError, tradingAmount) &&
          isValidField(tradingAmtPerBuyError, amountPerBuy) &&
          isValidField(buyTriggerError, buyThreshold)) {
        var data = FormData();
        data.coin = coinID;
        data.exchange = widget.userExchangeID;
        data.plan_type = "0";
        data.trading_amount = double.parse(tradingAmount).round().toString();
        data.amount_per_buy = calculateAmountPerBuy().toString();
        data.number_of_position = numPos;
        data.buy_threshold = buyThreshold;
        data.is_default = _value ? "1" : "0";
        data.buy_time_scale = "0";
        data.sell_threshold = "0";
        if (widget.selectedPlanTitle!=null&& widget.selectedPlanTitle!=title&&
            _value == true) {
          showModalBottomSheet(
              context: context,
              builder: (c) => UpdateFormDialog(
                    changePlanName: widget.title,
                    planName: selectedPlanTitle,
                    onTapUpdate: () async {
            Navigator.pop(context);
            planID!=null? await widget.formBloc.postFormData(context,planID,data):await widget.formBloc.postFormData(context,null, data);
                    },
                  ));
        } else {
          planID != null ? await widget.formBloc.postFormData(context,planID, data) : await widget.formBloc.postFormData(context,null, data);

        print("selected plan is ${widget.selectedPlan.toString()}");
        }
//        planID!=null? await widget.formBloc.postFormData(planID,data):await widget.formBloc.postFormData(null, data);

      } else {
        setError();
      }
    } else {
      if (isValidField(coinNameError, coinID) &&
          isValidField(pairNameError, pairedCoinID) &&
          isValidField(tradingAmtError, tradingAmount) &&
          isValidField(tradingAmtPerBuyError, amountPerBuy) &&
          isValidField(buyTriggerError, buyThreshold) &&
          isValidField(sellTriggerError, sellThreshold) &&
          isValidField(sellTimeError, sellTimeLimit)) {
        var data = FormData();
        data.coin = coinID;
        data.exchange = widget.userExchangeID;
        data.plan_type = title.contains("High") ? "2" : "1";
        data.trading_amount = double.parse(tradingAmount).round().toString();
        data.buy_time_scale = sellTimeLimit;
        data.amount_per_buy = calculateAmountPerBuy();
        data.number_of_position = numPos;
        data.buy_threshold = buyThreshold;
        data.sell_threshold = sellThreshold;
        data.is_default = _value ? "1" : "0";
        if (widget.selectedPlanTitle!=null&&
            widget.selectedPlanTitle!=title&&
            _value == true
        ) {

          showModalBottomSheet(
              context: context,
              builder: (c) => UpdateFormDialog(
                    changePlanName: widget.title,
                    planName: selectedPlanTitle,
                    onTapUpdate: () async {
            Navigator.pop(context);
            planID!=null? await widget.formBloc.postFormData(context,planID,data):await widget.formBloc.postFormData(context,null, data);
                    },
                  ));
        } else {
          planID != null
              ? await widget.formBloc.postFormData(context,planID, data)
              : await widget.formBloc.postFormData(context,null, data);
//          Fluttertoast.showToast(msg: "Else block");
        }
//        planID!=null? await widget.formBloc.postFormData(planID,data):await widget.formBloc.postFormData(null, data);

      } else {
        setError();
        print("Invalid form");
      }
    }
  }

  bool isValidField(String error, String data) {
    if (data != null && data.isNotEmpty && error == null) {
      return true;
    } else {
      return false;
    }
  }

  listenCurrentPlanStream() {
    widget.formBloc.planStream.listen((value) {
      Fluttertoast.showToast(msg: value);
    });
  }

  String getPlanTitle() {
    var plan = widget.selectedPlan;
    if (plan.planType == "0" && plan.isDefault == true) {
      selectedPlanTitle = "Low Risk, Low Profit";
    } else if (plan.planType == "1" && plan.isDefault == true) {
      selectedPlanTitle = "Medium Risk, Medium Profit";
    } else if (plan.planType == "2" && plan.isDefault == true) {
      selectedPlanTitle = "High Risk, High Profit";
    }
    print("plan name is ${plan.isDefault}");
    return selectedPlanTitle;
  }

  void setError() {
    setState(() {
      if(_coinNameController.text.isEmpty){
        coinNameError="Must not be empty";
      }
      if(_pairedCoinController.text.isEmpty){
        pairNameError="Must not be empty";
      }
      if (_tradingAmountPerBuyController.text.isEmpty) {
        tradingAmtPerBuyError = "Must not be empty";
      }
      if (_tradingAmountController.text.isEmpty) {
        tradingAmtError = "Must not be empty";
      }
      if (_numPosController.text.isEmpty) {
        numPosError = "Must not be empty";
      }
      if (_buyTriggerController.text.isEmpty) {
        buyTriggerError = "Must not be empty";
      }
      if (_sellTriggerController.text.isEmpty) {
        sellTriggerError = "Must not be empty";
      }
      if (_sellTimeController.text.isEmpty) {
        sellTimeError = "Must not be empty";
      }
    });
  }
}
