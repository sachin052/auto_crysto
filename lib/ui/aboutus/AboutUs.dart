
import 'package:auto_crysto/ui/utils/AppColors.dart';
import 'package:auto_crysto/ui/utils/Images.dart';
import 'package:auto_crysto/ui/utils/Styles.dart';
import 'package:flutter/material.dart';
class AboutUs extends StatelessWidget {
  final String title;

  const AboutUs({Key key, this.title}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Material(
      child: Scaffold(
        appBar: buildAppBar(context),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            child: Column(
              children: <Widget>[
                Image(image: AssetImage(Images.app_logo_white,),color: AppColors.colorPrimary,height: 50,width: 250,),
                Text("AutoCrysto is for traders on the go. We provide a simple interface where the traders can enter the buy and sell % threshold. No need to spend time understanding complex technical analysis and technical terms such as the Bollinger bands, Stochastic histograms etc. even though if you know them well it is a plus in trading.",style: Styles.regularBodyThree(fontSize: 14.0),),
              ],
            ),
          ),
        ),
      ),
    );
  }
  PreferredSize buildAppBar(BuildContext context) {
    return PreferredSize(
        child: Material(
          elevation: 10.0,
          child: AppBar(
            flexibleSpace: Container(
              decoration: BoxDecoration(
                gradient: RadialGradient(
                  colors: [
                    AppColors.colorPrimary,
                    AppColors.colorPrimaryGradient
                  ],
                  radius: 5.0,
//                    begin: Alignment.bottomCenter,
                  stops: [0.1, 0.66],
//                    end: Alignment.topCenter
                ),
              ),
            ),
            title: Material(
              type: MaterialType.transparency,
              child: Text(
                title,
                style: Styles.semiBoldHeadingFour(
    color: Colors.white),
              ),
            ),
            centerTitle: true,
          ),
        ),
        preferredSize: Size(MediaQuery
            .of(context)
            .size
            .width, 56.0));
  }
}
