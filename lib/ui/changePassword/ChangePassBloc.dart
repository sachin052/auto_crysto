import 'dart:async';

import 'package:auto_crysto/ui/data/ApiHelper/ApiResponse.dart';
import 'package:auto_crysto/ui/data/ApiHelper/Repo/AppRepo.dart';
import 'package:auto_crysto/ui/data/models/CommonApiResponse.dart';
import 'package:auto_crysto/ui/data/sharedPrefHelper/SharedPrefHelper.dart';

class ChangePassBloc{
  AppRepo appRepo;
  StreamController<ApiResponse<CommonApiResponse>> streamController;
  Stream<ApiResponse<CommonApiResponse>> get updateStream=>streamController.stream;
  StreamSink<ApiResponse<CommonApiResponse>> get updateSink=>streamController.sink;

  ChangePassBloc(){
   appRepo=AppRepo();
   streamController=StreamController<ApiResponse<CommonApiResponse>>();
  }
  updatePassword(Map<String,String> body)async{
    updateSink.add(ApiResponse.loading("Updating Profile"));
    try{
      var user=await SharedPrefHelper.getUserData();
      var response =await appRepo.updatePassword(user.id, body);
      updateSink.add(ApiResponse.completed(response));
    }catch(e){
      updateSink.add(ApiResponse.error(e.toString()));
    }

  }
  void dispose(){
    streamController.close();
}
}