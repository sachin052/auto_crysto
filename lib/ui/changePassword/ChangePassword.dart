
import 'package:auto_crysto/ui/apiresponseui/OnError.dart';
import 'package:auto_crysto/ui/changePassword/ChangePassBloc.dart';
import 'package:auto_crysto/ui/commonui/CustomMaterialButton.dart';
import 'package:auto_crysto/ui/data/ApiHelper/ApiResponse.dart';
import 'package:auto_crysto/ui/utils/AppColors.dart';
import 'package:auto_crysto/ui/utils/Images.dart';
import 'package:auto_crysto/ui/utils/Styles.dart';
import 'package:auto_crysto/ui/utils/Utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
class  ChangePassword extends StatefulWidget {
 final String title;

  const ChangePassword({Key key, this.title}) : super(key: key);

  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  TextEditingController _oldPassController;
  TextEditingController _newPassController;
  TextEditingController _confirmPassController;
  String _oldPassError;
  String _newPassError;
  String _confirmPassError;
  ChangePassBloc changePassBloc;

  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    changePassBloc=ChangePassBloc();
    _oldPassController=TextEditingController();
    _newPassController=TextEditingController();
    _confirmPassController=TextEditingController();
    _listenUpdatePassStream();
  }
  @override
  Widget build(BuildContext context) {
    return MediaQuery(
      data: MediaQuery.of(context).size.width>600?MediaQuery.of(context).copyWith(textScaleFactor: 1.2):MediaQuery.of(context),
      child: Scaffold(
        key: key,
        appBar: buildAppBar(context),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                SizedBox(height: 50.0,),
                Image(image: AssetImage(Images.app_logo_white,),color: AppColors.colorPrimary,height: 50,width: 250,),
                SizedBox(height: 10.0,),
                TextField(
                  obscureText: true,
                  controller: _oldPassController,
                  onChanged: (v){
                  setOldPassError(v);
                  },
                  cursorColor: AppColors.colorPrimary,
                  style: Styles.semiBoldHeadingFour(color: Colors.black.withOpacity(.5)),
                  decoration: Styles.getOutlineBorder("Old Password", _oldPassError),
                ),
                SizedBox(height: 10.0,),
                TextField(
                  obscureText: true,
                  controller: _newPassController,
                  onChanged: (v){
                  setNewPassError(v);
                  },
                  cursorColor: AppColors.colorPrimary,
                  style: Styles.semiBoldHeadingFour(color: Colors.black.withOpacity(.5)),
                  decoration: Styles.getOutlineBorder("New Password", _newPassError),
                ),
                SizedBox(height: 10.0,),
                TextField(
                  obscureText: true,
                  controller: _confirmPassController,
                  onChanged: (v){
                    setConfirmPassError(v);
                  },
                  cursorColor: AppColors.colorPrimary,
                  style: Styles.semiBoldHeadingFour(color: Colors.black.withOpacity(.5)),
                  decoration: Styles.getOutlineBorder("Confirm Password", _confirmPassError),
                ),
                SizedBox(height: 15.0,),
                CustomMaterialButton(UniqueKey(),(){
                  _updateUserPassword();
                },"Update"),
                SizedBox(height: 10.0,),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _updateUserPassword() {
  if(_oldPassController.text.isNotEmpty&&_oldPassError==null&&_newPassController.text.isNotEmpty&&_newPassError==null&&_confirmPassController.text.isNotEmpty&&_confirmPassError==null){
    Map<String ,String> body={
      "old_password":_oldPassController.text,
      "new_password":_newPassController.text,
      "confirm_password":_confirmPassController.text
    };
    changePassBloc.updatePassword(body);
  }
  else{
    setConfirmPassError(_confirmPassController.text);
    setNewPassError(_newPassController.text);
    setOldPassError(_oldPassController.text);
  }
  }
  _listenUpdatePassStream(){
    changePassBloc.updateStream.listen((data){
      switch (data.status) {
        case Status.LOADING:
          Utils.onLoadingWithKey(key, data.message);
          break;
        case Status.COMPLETED:
          key.currentState.hideCurrentSnackBar();
          Utils.showShowSnackBarWithKey(key, data.data.message, false);
//          getIosPicker(context, data.data, getSelectedCountry);
//          _countryList=data.data;
//              _countriesList = data.data;
//              _countryController.text = _countriesList[0].title;
//              _selectedCountryID=_countriesList[0].id.toString();
          break;
        case Status.ERROR:
          key.currentState.hideCurrentSnackBar();
          Navigator.push(context, CupertinoPageRoute(builder: (_)=>Scaffold(body: OnError(errorMessage: data.message,onRetryPressed: ()async{
            Navigator.pop(context);
//            signUpBlock.fetchStateList(_selectedCountryID);
          },btnText: "Ok",))));
//          showDialog(context: context,builder: (c)=> OnError(errorMessage: data.message,onRetryPressed: ()async{
//            Navigator.pop(context);
////            signUpBlock.fetchStateList(_selectedCountryID);
//          },btnText: "Ok",));
          print(data.message);
          break;
      }
    });
  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    changePassBloc.dispose();
  }
  PreferredSize buildAppBar(BuildContext context) {
    return PreferredSize(
        child: Material(
          elevation: 10.0,
          child: AppBar(
            flexibleSpace: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [
                    AppColors.colorPrimary,
                    AppColors.colorPrimaryGradient
                  ],
//                  radius: 5.0,
                    begin: Alignment.topCenter,
                  stops: [0.1, 0.66],
                    end: Alignment.bottomCenter
                ),
              ),
            ),
            title: Material(
              type: MaterialType.transparency,
              child: Text(
                widget.title,
                style: Styles.semiBoldHeadingThree(
                    color: Colors.white),
              ),
            ),
            centerTitle: true,
          ),
        ),
        preferredSize: Size(MediaQuery
            .of(context)
            .size
            .width, 56.0));
  }

  void setConfirmPassError(String v) {
    setState(() {
      if(v.isEmpty){
        _confirmPassError="Confirm password is empty";
      }
      else if(_newPassController.text!=_confirmPassController.text){
        _confirmPassError="Confirm password mismatch";
      }
      else{
        _confirmPassError=null;
      }
    });
  }

  void setNewPassError(String v) {
    setState(() {
      if(v.isEmpty){
        _newPassError="New password is empty";
      }
      else if(v.length<6){
        _newPassError="Req min 6 char";
      }
      else{
        _newPassError=null;
      }
    });
  }

  void setOldPassError(String v) {
    setState(() {
      if(v.isEmpty){
        _oldPassError="Old password is empty";
      }
      else if(v.length<6){
        _oldPassError="Req min 6 char";
      }
      else{
        _oldPassError=null;
      }
    });
  }
}
