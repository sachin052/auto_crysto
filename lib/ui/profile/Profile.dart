import 'dart:io';

import 'package:auto_crysto/ui/apiresponseui/IosPicker.dart';
import 'package:auto_crysto/ui/apiresponseui/OnError.dart';
import 'package:auto_crysto/ui/commonui/CustomMaterialButton.dart';
import 'package:auto_crysto/ui/commonui/ImagePicker.dart';
import 'package:auto_crysto/ui/data/ApiHelper/ApiResponse.dart';
import 'package:auto_crysto/ui/data/models/States.dart';
import 'package:auto_crysto/ui/data/models/UserDetails.dart' as prefix0;
import 'package:auto_crysto/ui/home/HomeBloc.dart';
import 'package:auto_crysto/ui/login_signup/SignUpBloc.dart';
import 'package:auto_crysto/ui/profile/ProfileBloc.dart';
import 'package:auto_crysto/ui/utils/AppColors.dart';
import 'package:auto_crysto/ui/utils/Styles.dart';
import 'package:auto_crysto/ui/utils/Utils.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:showcaseview/showcase_widget.dart';
import 'package:showcaseview/showcaseview.dart';

import '../../main.dart';

class Profile extends StatefulWidget {
  final prefix0.UserDetails profile;
  final HomBloc notifyPreviousWidget;

  const Profile({Key key, this.profile, this.notifyPreviousWidget})
      : super(key: key);

  @override
  _ProfileState createState() =>
      _ProfileState(this.profile, this.notifyPreviousWidget);
}

class _ProfileState extends State<Profile> {
  var _image;
  SignUpBlock signUpBlock;
  final prefix0.UserDetails profile;
  final HomBloc notifyPreviousWidget;
  TextEditingController firstNameController;
  TextEditingController lastNameController;
  TextEditingController countyController;
  TextEditingController stateController;
  TextEditingController cityController;
  String _selectedCountryID;
  String _selectedStateID;
  String _selectedCityID;
  List<States> _countryList;
  List<States> _stateList;
  List<States> _cityList;
  String _firstNameError;
  String _lastNameError;
  GlobalKey _one = GlobalKey();
  ScrollController _controller;
  FocusNode _firstFocusNode;

  _ProfileState(this.profile, this.notifyPreviousWidget);

  File localImage;
  bool enableEditing = true;
  ProfileBloc profileBloc;
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  double opacity = 0.0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    profileBloc = ProfileBloc();
    signUpBlock = locator<SignUpBlock>();
    _listenCountries();
    _listenStates();
    _listenCities();
    _firstFocusNode = FocusNode();
    _listenSignUpResponse();
    firstNameController = TextEditingController();
    lastNameController = TextEditingController();
    countyController = TextEditingController();
    cityController = TextEditingController();
    stateController = TextEditingController();
    if (profile != null) {
      firstNameController.text = widget.profile.firstName;
      lastNameController.text = widget.profile.lastName;
      countyController.text = profile.city.state.country.title;
      _selectedCountryID = profile.city.state.country.id.toString();
      stateController.text = profile.city.state.title;
      _selectedStateID = profile.city.state.id.toString();
      cityController.text = profile.city.title;
      _selectedCityID = profile.city.id.toString();
    }
    _controller=ScrollController();
    _controller.addListener((){

    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: Styles.getTheme(),
      home: MediaQuery(
        data: MediaQuery.of(context).size.width>600?MediaQuery.of(context).copyWith(textScaleFactor: 1.2):MediaQuery.of(context),
        child: Scaffold(
          key: key,
          backgroundColor: Colors.white,
          body: ShowCaseWidget(

            builder: Builder(
              builder: (context) => SingleChildScrollView(
                controller: _controller,
                child: Stack(
                  children: <Widget>[
                    Container(
                      height: MediaQuery.of(context).size.height + 80,
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                              colors: [
                                AppColors.colorPrimaryGradient,
                                AppColors.colorPrimary,
                              ],
                              begin: Alignment.topCenter,
                              stops: [0.1, 0.2],
                              end: Alignment.bottomCenter)),
                      child: Container(
                        margin: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * .28),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10.0),
                                topRight: Radius.circular(10.0))),
                      ),
                    ),
                    Positioned(
                      width: MediaQuery.of(context).size.width,
                      top: 30,
                      child: SingleChildScrollView(
                        child: buildProfileView(context),
                      ),
                    ),
                    buildAppBar(),
                    Visibility(
                      visible: enableEditing,
                      child: GestureDetector(
                        onTap: () {
                          if(_controller.offset >= _controller.position.maxScrollExtent-30){
                              _controller..animateTo(
                                0.0,
                                curve: Curves.easeOut,
                                duration: const Duration(milliseconds: 200),
                              );
                              if (enableEditing) {
                               Future.delayed(Duration(milliseconds: 250),(){
                                 ShowCaseWidget.of(context).startShowCase([_one]);
                               });
                              }
                              return;
                          }
                          if (enableEditing) {
                            ShowCaseWidget.of(context).startShowCase([_one]);
                          }
                        },
                        child: Container(
                          height: MediaQuery.of(context).size.height * .87,
                          margin: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * .13,
                          ),
                          color: Colors.transparent,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),

          ),
        ),
      ),
    );
  }

  buildProfileView(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 20.0, right: 20.0),
      width: MediaQuery.of(context).size.width,
      child: Column(
        children: <Widget>[
          SizedBox(
            height: MediaQuery.of(context).size.height * .15,
          ),
          Card(
            elevation: 5.0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            child: Stack(
              alignment: Alignment.center,
              overflow: Overflow.visible,
              children: <Widget>[
                Positioned(
                  child: Container(
                    child: Material(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(40.0)),
                      color: Colors.transparent,
                      elevation: 15.0,
                      child: InkWell(
                        onTap: () {
                          Platform.isIOS
                              ? actionSheetMethodIOS(context,getImage)
                              : androidPicker(context, getImage);
                        },
                        child: Hero(
                          tag: "image",
                          child: Material(
                            elevation: 5.0,
                            shadowColor: AppColors.colorPrimary,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(45.0)),
                            child: CircleAvatar(
                              radius: 45,
                              backgroundColor: AppColors.colorPrimary,
                              backgroundImage: localImage == null
                                  ? CachedNetworkImageProvider(
                                      widget.profile.profilePicUrl,
                                      errorListener: () {
                                      return Icon(Icons.error);
                                    })
                                  : FileImage(localImage),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  top: -40,
                ),
                Container(
                  margin: EdgeInsets.only(top: 70.0),
                  padding: EdgeInsets.fromLTRB(18.0, 0.0, 18.0, 40.0),
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Hero(
                            tag: "name",
                            child: Material(
                                type: MaterialType.transparency,
                                child: Text(
                                  widget.profile.firstName +
                                      " " +
                                      widget.profile.lastName,
                                  style: Styles.semiBoldHeadingTwo(),
                                ))),
                        SizedBox(
                          height: 5.0,
                        ),
                        Hero(
                            tag: "email",
                            child: Material(
                                type: MaterialType.transparency,
                                child: Text(
                                  widget.profile.email,
                                  style: Styles.regularBodyTwo(),
                                ))),
                        SizedBox(
                          height: 20.0,
                        ),
                        TextField(
                            focusNode: _firstFocusNode,
                            controller: firstNameController,
                            onChanged: (v) {
                              setState(() {
                                if (v.isEmpty) {
                                  _firstNameError = "First name is empty";
                                } else {
                                  _firstNameError = null;
                                }
                              });
                            },
                            style: Styles.semiBoldHeadingFour(
                                color: Colors.black.withOpacity(.8)),
                            cursorColor: AppColors.colorPrimary,
                            decoration: Styles.getOutlineBorder(
                                'First Name', _firstNameError)),
                        SizedBox(height: 15.0),
                        TextField(
                          controller: lastNameController,
                          onChanged: (v) {
                            setState(() {
                              if (v.isEmpty) {
                                _lastNameError = "Last name is empty";
                              } else {
                                _lastNameError = null;
                              }
                            });
                          },
                          cursorColor: AppColors.colorPrimary,
                          style: Styles.semiBoldHeadingFour(
                              color: Colors.black.withOpacity(.8)),
                          decoration: Styles.getOutlineBorder(
                              'Last Name', _lastNameError),
                        ),
                        SizedBox(height: 15.0),
                        Stack(
                          children: <Widget>[
                            TextField(
                              onTap: () async {
                                await signUpBlock.fetchCountryList();
                              },
                              controller: countyController,
                              readOnly: true,
                              cursorColor: AppColors.colorPrimary,
                              style: Styles.semiBoldHeadingFour(
                                  color: Colors.black.withOpacity(.8)),
                              decoration:
                                  Styles.getOutlineBorder('Country', null),
                            ),
                            Align(
                                alignment: Alignment.centerRight,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Icon(
                                    Icons.arrow_drop_down,
                                    color: Colors.grey,
                                  ),
                                ))
                          ],
                        ),
                        SizedBox(height: 15.0),
                        Stack(
                          children: <Widget>[
                            TextField(
                              onTap: () async {
                                await signUpBlock
                                    .fetchStateList(_selectedCountryID);
                              },
                              controller: stateController,
                              readOnly: true,
                              cursorColor: AppColors.colorPrimary,
                              style: Styles.semiBoldHeadingFour(
                                  color: Colors.black.withOpacity(.8)),
                              decoration:
                                  Styles.getOutlineBorder('State', null),
                            ),
                            Align(
                                alignment: Alignment.centerRight,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Icon(
                                    Icons.arrow_drop_down,
                                    color: Colors.grey,
                                  ),
                                ))
                          ],
                        ),
                        SizedBox(height: 15.0),
                        Stack(
                          children: <Widget>[
                            TextField(
                              onTap: () async {
                                await signUpBlock
                                    .fetchCityList(_selectedStateID);
                              },
                              controller: cityController,
                              readOnly: true,
                              cursorColor: AppColors.colorPrimary,
                              style: Styles.semiBoldHeadingFour(
                                  color: Colors.black.withOpacity(.8)),
                              decoration: Styles.getOutlineBorder('City', null),
                            ),
                            Align(
                                alignment: Alignment.centerRight,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Icon(
                                    Icons.arrow_drop_down,
                                    color: Colors.grey,
                                  ),
                                ))
                          ],
                        ),
                        SizedBox(height: 15.0),
                        Visibility(
                          visible: !enableEditing,
                          child: CustomMaterialButton(widget.key, () async {
                            var image;
                            if (localImage != null) {
                              Utils.showProgressDialog(context, "Uploading Image");
                              image = await profileBloc.uploadImage(localImage);

                            }
                            Map<String, String> data = {
                              "first_name": firstNameController.text,
                              "last_name": lastNameController.text,
                              "city": _selectedCityID,
                              "profile_pic":
                                  image != null ? image.toString() : "",
                            };
                            Navigator.of(context,rootNavigator: true).pop();
                            var respones =
                                await profileBloc.updateProfile(data);
                            widget.notifyPreviousWidget.refreshSink
                                .add("Profile");
                            print(respones);
                          }, "Save"),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
          SizedBox(
            height: 10.0,
          ),
        ],
      ),
    );
  }

  SafeArea buildAppBar() {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.only(top: 10.0, right: 10.0, left: 10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            IconButton(
                icon: Icon(
                  Icons.arrow_back,
                  color: Colors.white,
                ),
                iconSize: 25.0,
                splashColor: Colors.white,
                onPressed: onBackPressed),
            Padding(
              padding: const EdgeInsets.only(left: 0.0),
              child: Text(
                "Profile",
                style: Styles.semiBoldHeadingTwo(color: Colors.white),
              ),
            ),
            Showcase(
              key: _one,
              shapeBorder: CircleBorder(),
              showArrow: true,
              animationDuration: Duration(seconds: 1),
              onTargetClick: () {
                setState(() {
                  enableEditing = !enableEditing;
                  if (!enableEditing) {
                    FocusScope.of(context).requestFocus(_firstFocusNode);
                  }
                });
              },
              disposeOnTap: true,
              description: " Enable edit mode here   ",
              descTextStyle: Styles.semiBoldHeadingFour(),
              child: IconButton(
                onPressed: () {
                  setState(() {
                    enableEditing = !enableEditing;
                    if (!enableEditing) {
                      FocusScope.of(context).requestFocus(_firstFocusNode);
                    }
                  });
                },
                icon: Icon(
                  FontAwesomeIcons.solidEdit,
                  color: enableEditing
                      ? Colors.white
                      : Colors.white.withOpacity(.5),
                  size: 18.0,
                ),
                splashColor: Colors.white,
              ),
            )
          ],
        ),
      ),
    );
  }

  void onBackPressed() {
    Navigator.pop(context);
  }

  File getImage(File file) {

    setState(() {
      localImage = file;
    });
    return file;
  }

  void _listenCountries() {
    signUpBlock.countryListStream.listen(
        (data) {
          switch (data.status) {
            case Status.LOADING:
              Utils.onLoadingWithKey(key, data.message);
              break;
            case Status.COMPLETED:
              key.currentState.hideCurrentSnackBar();
              getIosPicker(context, data.data, getSelectedCountry,"Select Country");
              _countryList = data.data;
//              _countriesList = data.data;
//              _countryController.text = _countriesList[0].title;
//              _selectedCountryID=_countriesList[0].id.toString();
              break;
            case Status.ERROR:
              key.currentState.hideCurrentSnackBar();
              showDialog(
                  context: context,
                  builder: (c) => OnError(
                        errorMessage: data.message,
                        onRetryPressed: () async {
                          Navigator.pop(context);
                          signUpBlock.fetchStateList(_selectedCountryID);
                        },
                      ));
              print(data.message);
              break;
          }
        },
        onDone: () {},
        onError: (error) {
          print("Some Error");
        });
  }

  getSelectedCountry(States country) {
    print("selected country is " + country.title);
    _selectedCountryID = country.id.toString();
    return country;
  }

  getSelectedState(States country) {
    print("selected State is " + country.title);
    _selectedStateID = country.id.toString();
    stateController.text = country.title;
    return country;
  }

  getSelectedCity(States country) {
    print("selected State is " + country.title);
    _selectedCityID = country.id.toString();
    cityController.text = country.title;
    return country;
  }

  void _listenStates() {
    signUpBlock.stateListStream.listen((data) {
      switch (data.status) {
        case Status.LOADING:
          Utils.onLoadingWithKey(key, data.message);
//          Scaffold.of(context).sh
//          showDialog(context: context,builder: (c)=>onLoading(colors: AppColors.colorPrimary.withOpacity(.2)));
          break;
        case Status.COMPLETED:
//          Navigator.pop(context);
          key.currentState.hideCurrentSnackBar();
          if (data.data.isEmpty) {
            print("Empty data");
            showDialog(
                context: context,
                builder: (c) => OnError(
                      errorMessage: "No data found",
                      onRetryPressed: () async {
                        Navigator.pop(context);
                        signUpBlock.fetchStateList(_selectedCountryID);
                      },
                    ));
            return;
          }
          getIosPicker(context, data.data, getSelectedState,"Select State");
          _stateList = data.data;
          break;
        case Status.ERROR:
          key.currentState.hideCurrentSnackBar();
          showDialog(
              context: context,
              builder: (c) => OnError(
                    errorMessage: data.message,
                    onRetryPressed: () async {
                      signUpBlock.fetchStateList(_selectedCountryID);
                    },
                  ));
          print(data.message);
          break;
      }
    }, onDone: () {
      print("Task Done");
    }, onError: (error) {
      print("Some Error");
    });
  }

  void _listenCities() {
    signUpBlock.cityListStream.listen(
        (data) {
          switch (data.status) {
            case Status.LOADING:
              Utils.onLoadingWithKey(key, data.message);
              break;
            case Status.COMPLETED:
              key.currentState.hideCurrentSnackBar();
              getIosPicker(context, data.data, getSelectedCity,"Select City");
              _cityList = data.data;
              break;
            case Status.ERROR:
              key.currentState.hideCurrentSnackBar();
              showDialog(
                  context: context,
                  builder: (c) => OnError(
                        errorMessage: data.message,
                        onRetryPressed: () async {
                          Navigator.pop(context);
                          signUpBlock.fetchCityList(_selectedStateID);
                        },
                      ));
              print(data.message);
              break;
          }
        },
        onDone: () {},
        onError: (error) {
          print("Some Error");
        });
  }

  void _listenSignUpResponse() {
    profileBloc.streamResponse.listen(
        (data) {
          switch (data.status) {
            case Status.LOADING:
              Utils.onLoadingWithKey(key, data.message);
              break;
            case Status.COMPLETED:
              key.currentState.hideCurrentSnackBar();
              Utils.showShowSnackBarWithKey(key, data.data.message, false);
              break;
            case Status.ERROR:
              key.currentState.hideCurrentSnackBar();
              showDialog(
                  context: context,
                  builder: (c) => OnError(
                        errorMessage: data.message,
                        onRetryPressed: () async {
                          Navigator.pop(context);
                          signUpBlock.fetchCityList(_selectedStateID);
                        },
                      ));
              print(data.message);
              break;
          }
        },
        onDone: () {},
        onError: (error) {
          print("Some Error");
        });
  }

  dynamic uploadImage(File videoFile) async {

  }
}
