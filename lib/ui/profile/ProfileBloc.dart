import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:auto_crysto/ui/data/ApiHelper/ApiResponse.dart';
import 'package:auto_crysto/ui/data/ApiHelper/Repo/AppRepo.dart';
import 'package:auto_crysto/ui/data/models/CommonApiResponse.dart';
import 'package:auto_crysto/ui/utils/Utils.dart';
import 'package:path_provider/path_provider.dart';

class ProfileBloc{
  AppRepo appRepo;
  StreamController streamController;
  StreamSink<ApiResponse<CommonApiResponse>> get sink=>streamController.sink;
  Stream<ApiResponse<CommonApiResponse>> get streamResponse=>streamController.stream;

  ProfileBloc(){
    appRepo=AppRepo();
    streamController=StreamController<ApiResponse<CommonApiResponse>>();
  }
  updateProfile(Map<String,String> body)async{
    sink.add(ApiResponse.loading("Updating Profile"));
    try{
      var response=await appRepo.updateProfile(body);
      sink.add(ApiResponse.completed(response));
    }catch(e){
      sink.add(ApiResponse.error(e.toString()));
    }
  }

  Future<dynamic>uploadImage(File localImage)async{
    Directory tempDir = await getApplicationDocumentsDirectory();
    File compressedImage=await Utils.compressImage(localImage, tempDir.path,);
    print("Size of image is${compressedImage.lengthSync()}");
    return await appRepo.uploadImage(compressedImage);
  }
  void dispose(){
    streamController.close();
  }
}