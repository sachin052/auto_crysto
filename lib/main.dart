import 'package:auto_crysto/routes/Router.dart';
import 'package:auto_crysto/ui/login_signup/LoginUI.dart';
import 'package:auto_crysto/ui/login_signup/SignUpBloc.dart';
import 'package:auto_crysto/ui/login_signup/SignUpUI.dart';
import 'package:auto_crysto/ui/utils/AppColors.dart';
import 'package:auto_crysto/ui/utils/Styles.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get_it/get_it.dart';

GetIt locator;

void main() {
  setUpLocator();
  Crashlytics.instance.enableInDevMode = true;
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: AppColors.colorPrimary, // status bar color
  ));
  runApp(MyApp());
}

//Used for dependency injection
setUpLocator() {
  locator = GetIt.instance;
  locator.registerLazySingleton(() => SignUpBlock());
  locator.registerLazySingleton(() => LoginUI());
  locator.registerLazySingleton(() => SignUpUI());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
//    return DevicePreview(
//      builder:(_)=> MaterialApp(
//        debugShowCheckedModeBanner: false,
//        initialRoute: "/",
//        onGenerateRoute: Router.generateRoute,
//        theme: Styles.getTheme(),
//      ),
//    );
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: "/",
      onGenerateRoute: Router.generateRoute,
      theme: Styles.getTheme(),
    );
  }
}
