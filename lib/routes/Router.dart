import 'package:auto_crysto/ui/home/Home.dart';
import 'package:auto_crysto/ui/login_signup/LoginSignUp.dart';
import 'package:auto_crysto/ui/splash/SplashScreen.dart';
import 'package:auto_crysto/ui/tutorials/Tutorials.dart';
import 'package:auto_crysto/ui/walk_through/WalkThrough.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Router {
  static const String splash="/";
  static const String home="/Home";
  static const String tutorials="/Tutorials";
  static const String loginSignUp="/LoginSignUp";
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case splash:
        return MaterialPageRoute(builder: (_) => SplashScreen());
      case home:
        return MaterialPageRoute(builder: (_) => Home());
      case tutorials:
        return MaterialPageRoute(builder: (_)=>Tutorials());
      case loginSignUp:
        return MaterialPageRoute(builder: (_)=>LoginSignUP());
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
              body: Center(
                  child: Text('No route defined for ${settings.name}')),
            ));
    }
  }
}