import 'dart:async';
import 'package:rxdart/rxdart.dart';


final validateEmail =
StreamTransformer<String, String>.fromHandlers(handleData: (email, sink) {
  if (email.isValidEmail(email)) {
    sink.add(email);
  } else {
    sink.addError('Enter a valid email');
  }
});

final validatePassword = StreamTransformer<String, String>.fromHandlers(
    handleData: (string, sink) {
      try{
        if (string.length > 7) {
          sink.add(string);
        } else {
          sink.addError("Req min 8 Chars");
        }
      }catch(e){
        print("Exceptio:"+e.toString());
      }
    });
final notEmptyValidator = StreamTransformer<String, String>.fromHandlers(
    handleData: (string, sink) {
      if (string.isNotEmpty) {
        sink.add(string);
      } else {
        sink.addError("Must not be empty");
      }
    });

final phoneValidator = StreamTransformer<String, String>.fromHandlers(
    handleData: (string, sink) {
      if (string.length>=10) {
        sink.add(string);
      } else {
        sink.addError("Enter a valid phone");
      }
    });

confirmPassValidator(BehaviorSubject<String> _pController,BehaviorSubject<String> _cPController){
  return CombineLatestStream.combine2<String,String,String>(_pController, _cPController, (pass,cpass){
    if(pass==cpass) return cpass;
    _cPController.sink.addError("Password Mismatched");
    return null;
  });
}

extension StringExtention on String {
  bool isValidEmail(String email) {
    return RegExp(
        r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(email);
  }}